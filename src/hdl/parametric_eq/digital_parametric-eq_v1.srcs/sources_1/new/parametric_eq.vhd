----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/11/2020 19:02:23 PM
-- Design Name: 
-- Module Name: parametric_eq - beh
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity parametric_eq is
    generic(    
        data_width  : natural := 24
    );
                
    Port ( 
        clk_i           : in    std_logic;
        rst_i           : in    std_logic;
        we_i            : in    std_logic;
        ready_o         : out   std_logic;    
        q_i             : in    std_logic_vector (data_width-1 downto 0);
        f_i             : in    std_logic_vector (data_width-1 downto 0);
        data_i          : in    std_logic_vector (data_width-1 downto 0);
        data_bp_o       : out   std_logic_vector (data_width-1 downto 0);
        data_lp_o       : out   std_logic_vector (data_width-1 downto 0);
        data_hp_o       : out   std_logic_vector (data_width-1 downto 0);
        data_br_o       : out   std_logic_vector (data_width-1 downto 0)
    );
    
end parametric_eq;

architecture beh of parametric_eq is
 
    signal adder_1_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_2_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_3_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_4_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal multi_1_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal multi_2_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal multi_3_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal reg_1_s      : std_logic_vector (data_width-1 downto 0) := (others=>'0');   
    signal reg_2_s      : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    
    signal q_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
    signal f_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
    
    signal ready_s : std_logic := '0';
        
begin

    process (clk_i) is
    begin
        if (rising_edge(clk_i)) then
            if (we_i = '1') then
                q_s <= q_i;
                f_s <= f_i;
                ready_s <= '1';                      
            end if;
        end if;
    end process;
    
    adder_1_s <= std_logic_vector(signed(data_i) - signed(multi_3_s(43 downto 20)) - signed(adder_3_s));

    adder_2_s <= std_logic_vector(signed(multi_1_s(43 downto 20)) + signed(reg_1_s));
    
    adder_3_s <= std_logic_vector(signed(multi_2_s(43 downto 20)) + signed(reg_2_s));
    
    adder_4_s <= std_logic_vector(signed(adder_1_s) + signed(adder_3_s));
    
    multi_1_s <= std_logic_vector(signed(f_s) * signed(adder_1_s));
    
    multi_2_s <= std_logic_vector(signed(f_s) * signed(reg_1_s));
    
    multi_3_s <= std_logic_vector(signed(q_s) * signed(reg_1_s));
    
    process (clk_i) is
    begin
        if(rising_edge(clk_i))then
            data_bp_o <= adder_2_s;
            data_hp_o <= adder_1_s;
            data_lp_o <= adder_3_s;
            data_br_o <= adder_4_s;
        end if;
    end process;
    
    first_reg:
    entity work.reg(beh)
    generic map(
        N_bit_reg => data_width
    )
    port map(
        clk =>  clk_i,
        rst =>  rst_i,    
        d   =>  adder_2_s,
        q   =>  reg_1_s
    );
    
    second_reg:
    entity work.reg(beh)
    generic map(
        N_bit_reg => data_width
    )
    port map(
        clk =>  clk_i,
        rst =>  rst_i,    
        d   =>  adder_3_s,
        q   =>  reg_2_s
    );
    
    ready_o <= ready_s;

end beh;
