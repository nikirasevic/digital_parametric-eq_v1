----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Ra�evi�
-- 
-- Create Date: 06/11/2020 19:26:03 PM
-- Design Name: 
-- Module Name: parametric_eq_tb - beh
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use work.txt_util.all;

entity parametric_eq_tb is
    generic(data_width : natural := 24);
--  Port ( );
end parametric_eq_tb;

architecture beh of parametric_eq_tb is
    signal clk_s    : std_logic := '0';
    signal rst_s    : std_logic := '1';
    signal we_s     : std_logic := '0';
    signal ready_s  : std_logic := '0';
    
    signal q_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
    signal f_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
    
    signal tmp_q_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
    signal tmp_f_s  : std_logic_vector(data_width-1 downto 0) := (others=>'0');
         
    signal uut_input_s : std_logic_vector(data_width-1 downto 0);
    signal uut_bp_output_s : std_logic_vector(data_width-1 downto 0);
    signal uut_hp_output_s : std_logic_vector(data_width-1 downto 0);
    signal uut_lp_output_s : std_logic_vector(data_width-1 downto 0);
    signal uut_br_output_s : std_logic_vector(data_width-1 downto 0);

    constant period      : time := 20ns;
    constant reset_time 	: time := 200ns; 		-- reset time
    constant write_time 	: time := 120ns;
    constant wait_time   	: time := 80ns;
    constant wait_time_1    : time := 10000ns;
    constant write_time_1   : time := 10040ns;
    constant wait_time_2    : time := 20000ns;
    constant write_time_2   : time := 20040ns;
    
    --putanju do �eljenog fajla je potrebno prilagoditi strukturi direktorijuma na ra�unaru na kome se vr�i provera rada.
    file input_test_vector : text open read_mode is "D:\digital_parametric-eq_v1\filter_inputs\input.txt";

begin
    
    rst_s <= '0'after reset_time;
    
    we_en: process
    begin
        we_s <= '0', '1'after wait_time, '0'after write_time, '1'after wait_time_1, '0'after write_time_1, '1'after wait_time_2, '0'after write_time_2;
        
        q_s <= x"033333", x"01999a"after wait_time_1-1us, x"020000"after wait_time_2-1us;
        f_s <= x"0123bb", x"05ab41"after wait_time_1-1us, x"0091e3"after wait_time_2-1us;
               
        wait;
    end process;

    
    param_eq_under_test:
    entity work.parametric_eq(beh)
    port map(
        clk_i   =>  clk_s,
        rst_i   =>  rst_s,
        we_i    =>  we_s,
        ready_o =>  ready_s,
        q_i     =>  q_s,  
        f_i     =>  f_s, 
        data_i  =>  uut_input_s, 
        data_bp_o => uut_bp_output_s,
        data_hp_o => uut_hp_output_s,
        data_lp_o => uut_lp_output_s,
        data_br_o => uut_br_output_s
    );
             
    clk_process:
    process
    begin
        clk_s <= '0';
        wait for period/2;
        clk_s <= '1';
        wait for period/2;
    end process;
    
    stim_process:
    process
        variable tv : line;
    begin
        uut_input_s <= (others=>'0');
        wait until falling_edge(clk_s);
        --ulaz za filtriranje
        if(ready_s='1')then
        while not endfile(input_test_vector) loop
            readline(input_test_vector,tv);
            uut_input_s <= to_std_logic_vector(string(tv));
            wait until falling_edge(clk_s);
        end loop;
        report "verification done!" severity failure;
        end if;
    end process;

end beh;
