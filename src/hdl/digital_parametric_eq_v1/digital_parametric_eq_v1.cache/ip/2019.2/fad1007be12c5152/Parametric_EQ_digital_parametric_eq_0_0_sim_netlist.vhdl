-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Fri Jun 12 23:49:55 2020
-- Host        : kakaroto-PC running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Parametric_EQ_digital_parametric_eq_0_0_sim_netlist.vhdl
-- Design      : Parametric_EQ_digital_parametric_eq_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI is
  port (
    s00_axi_awready : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_wready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    \slv_reg3_reg[16]_0\ : out STD_LOGIC_VECTOR ( 16 downto 0 );
    \slv_reg2_reg[16]_0\ : out STD_LOGIC_VECTOR ( 16 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_awaddr_reg[4]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[4]_i_1_n_0\ : STD_LOGIC;
  signal \^axi_awaddr_reg[4]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg0[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal \^slv_reg2_reg[16]_0\ : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg3_reg[16]_0\ : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 24 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg5 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair1";
begin
  E(0) <= \^e\(0);
  Q(23 downto 0) <= \^q\(23 downto 0);
  SR(0) <= \^sr\(0);
  \axi_awaddr_reg[4]_0\(0) <= \^axi_awaddr_reg[4]_0\(0);
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
  \slv_reg2_reg[16]_0\(16 downto 0) <= \^slv_reg2_reg[16]_0\(16 downto 0);
  \slv_reg3_reg[16]_0\(16 downto 0) <= \^slv_reg3_reg[16]_0\(16 downto 0);
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s00_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => \^sr\(0)
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => sel0(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => sel0(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(2),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => sel0(2),
      O => \axi_araddr[4]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => sel0(0),
      S => \^sr\(0)
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => sel0(1),
      S => \^sr\(0)
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[4]_i_1_n_0\,
      Q => sel0(2),
      S => \^sr\(0)
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s00_axi_arready\,
      R => \^sr\(0)
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s00_axi_awready\,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s00_axi_awready\,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(2),
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^s00_axi_awready\,
      I5 => p_0_in(2),
      O => \axi_awaddr[4]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => \^sr\(0)
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => \^sr\(0)
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[4]_i_1_n_0\,
      Q => p_0_in(2),
      R => \^sr\(0)
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^sr\(0)
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s00_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => \^sr\(0)
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s00_axi_awready\,
      I2 => \^s00_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => \^sr\(0)
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(0),
      I4 => sel0(0),
      I5 => \^q\(0),
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(0),
      I1 => \^slv_reg2_reg[16]_0\(0),
      I2 => sel0(1),
      I3 => slv_reg1(0),
      I4 => sel0(0),
      I5 => slv_reg0(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(10),
      I4 => sel0(0),
      I5 => \^q\(10),
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(10),
      I1 => \^slv_reg2_reg[16]_0\(10),
      I2 => sel0(1),
      I3 => slv_reg1(10),
      I4 => sel0(0),
      I5 => slv_reg0(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(11),
      I4 => sel0(0),
      I5 => \^q\(11),
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(11),
      I1 => \^slv_reg2_reg[16]_0\(11),
      I2 => sel0(1),
      I3 => slv_reg1(11),
      I4 => sel0(0),
      I5 => slv_reg0(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(12),
      I4 => sel0(0),
      I5 => \^q\(12),
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(12),
      I1 => \^slv_reg2_reg[16]_0\(12),
      I2 => sel0(1),
      I3 => slv_reg1(12),
      I4 => sel0(0),
      I5 => slv_reg0(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(13),
      I4 => sel0(0),
      I5 => \^q\(13),
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(13),
      I1 => \^slv_reg2_reg[16]_0\(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => sel0(0),
      I5 => slv_reg0(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(14),
      I4 => sel0(0),
      I5 => \^q\(14),
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(14),
      I1 => \^slv_reg2_reg[16]_0\(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => sel0(0),
      I5 => slv_reg0(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(15),
      I4 => sel0(0),
      I5 => \^q\(15),
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(15),
      I1 => \^slv_reg2_reg[16]_0\(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => sel0(0),
      I5 => slv_reg0(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(16),
      I4 => sel0(0),
      I5 => \^q\(16),
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(16),
      I1 => \^slv_reg2_reg[16]_0\(16),
      I2 => sel0(1),
      I3 => slv_reg1(16),
      I4 => sel0(0),
      I5 => slv_reg0(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(17),
      I4 => sel0(0),
      I5 => \^q\(17),
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => slv_reg1(17),
      I4 => sel0(0),
      I5 => slv_reg0(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(18),
      I4 => sel0(0),
      I5 => \^q\(18),
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => sel0(0),
      I5 => slv_reg0(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(19),
      I4 => sel0(0),
      I5 => \^q\(19),
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => sel0(0),
      I5 => slv_reg0(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(1),
      I4 => sel0(0),
      I5 => \^q\(1),
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(1),
      I1 => \^slv_reg2_reg[16]_0\(1),
      I2 => sel0(1),
      I3 => slv_reg1(1),
      I4 => sel0(0),
      I5 => slv_reg0(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(20),
      I4 => sel0(0),
      I5 => \^q\(20),
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => sel0(0),
      I5 => slv_reg0(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(21),
      I4 => sel0(0),
      I5 => \^q\(21),
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => slv_reg1(21),
      I4 => sel0(0),
      I5 => slv_reg0(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(22),
      I4 => sel0(0),
      I5 => \^q\(22),
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => sel0(0),
      I5 => slv_reg0(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(23),
      I4 => sel0(0),
      I5 => \^q\(23),
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(23),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => sel0(0),
      I5 => slv_reg0(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(24),
      I4 => sel0(0),
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => slv_reg1(24),
      I4 => sel0(0),
      I5 => slv_reg0(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(25),
      I4 => sel0(0),
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => slv_reg1(25),
      I4 => sel0(0),
      I5 => slv_reg0(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(26),
      I4 => sel0(0),
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => sel0(0),
      I5 => slv_reg0(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(27),
      I4 => sel0(0),
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => sel0(0),
      I5 => slv_reg0(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(28),
      I4 => sel0(0),
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => slv_reg1(28),
      I4 => sel0(0),
      I5 => slv_reg0(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(29),
      I4 => sel0(0),
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg2(29),
      I2 => sel0(1),
      I3 => slv_reg1(29),
      I4 => sel0(0),
      I5 => slv_reg0(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(2),
      I4 => sel0(0),
      I5 => \^q\(2),
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(2),
      I1 => \^slv_reg2_reg[16]_0\(2),
      I2 => sel0(1),
      I3 => slv_reg1(2),
      I4 => sel0(0),
      I5 => slv_reg0(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(30),
      I4 => sel0(0),
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => slv_reg1(30),
      I4 => sel0(0),
      I5 => slv_reg0(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0A0A3A0A"
    )
        port map (
      I0 => \axi_rdata[31]_i_3_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg4(31),
      I4 => sel0(0),
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => sel0(0),
      I5 => slv_reg0(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(3),
      I4 => sel0(0),
      I5 => \^q\(3),
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(3),
      I1 => \^slv_reg2_reg[16]_0\(3),
      I2 => sel0(1),
      I3 => slv_reg1(3),
      I4 => sel0(0),
      I5 => slv_reg0(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(4),
      I4 => sel0(0),
      I5 => \^q\(4),
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(4),
      I1 => \^slv_reg2_reg[16]_0\(4),
      I2 => sel0(1),
      I3 => slv_reg1(4),
      I4 => sel0(0),
      I5 => slv_reg0(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(5),
      I4 => sel0(0),
      I5 => \^q\(5),
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(5),
      I1 => \^slv_reg2_reg[16]_0\(5),
      I2 => sel0(1),
      I3 => slv_reg1(5),
      I4 => sel0(0),
      I5 => slv_reg0(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(6),
      I4 => sel0(0),
      I5 => \^q\(6),
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(6),
      I1 => \^slv_reg2_reg[16]_0\(6),
      I2 => sel0(1),
      I3 => slv_reg1(6),
      I4 => sel0(0),
      I5 => slv_reg0(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(7),
      I4 => sel0(0),
      I5 => \^q\(7),
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(7),
      I1 => \^slv_reg2_reg[16]_0\(7),
      I2 => sel0(1),
      I3 => slv_reg1(7),
      I4 => sel0(0),
      I5 => slv_reg0(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(8),
      I4 => sel0(0),
      I5 => \^q\(8),
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(8),
      I1 => \^slv_reg2_reg[16]_0\(8),
      I2 => sel0(1),
      I3 => slv_reg1(8),
      I4 => sel0(0),
      I5 => slv_reg0(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A0A3A3A3A0A0A0A"
    )
        port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => slv_reg5(9),
      I4 => sel0(0),
      I5 => \^q\(9),
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[16]_0\(9),
      I1 => \^slv_reg2_reg[16]_0\(9),
      I2 => sel0(1),
      I3 => slv_reg1(9),
      I4 => sel0(0),
      I5 => slv_reg0(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => \^sr\(0)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => \^sr\(0)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => \^sr\(0)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => \^sr\(0)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => \^sr\(0)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => \^sr\(0)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => \^sr\(0)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => \^sr\(0)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => \^sr\(0)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => \^sr\(0)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => \^sr\(0)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => \^sr\(0)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => \^sr\(0)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => \^sr\(0)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => \^sr\(0)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => \^sr\(0)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => \^sr\(0)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => \^sr\(0)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => \^sr\(0)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => \^sr\(0)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => \^sr\(0)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => \^sr\(0)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => \^sr\(0)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => \^sr\(0)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => \^sr\(0)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => \^sr\(0)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => \^sr\(0)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => \^sr\(0)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => \^sr\(0)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => \^sr\(0)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => \^sr\(0)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => \^sr\(0)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => \^sr\(0)
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^s00_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => \^sr\(0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg0[15]_i_1_n_0\
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg0[23]_i_1_n_0\
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg0[31]_i_1_n_0\
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s00_axi_awready\,
      I2 => \^s00_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg0[7]_i_1_n_0\
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg0(0),
      R => \^sr\(0)
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg0(10),
      R => \^sr\(0)
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg0(11),
      R => \^sr\(0)
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg0(12),
      R => \^sr\(0)
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg0(13),
      R => \^sr\(0)
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg0(14),
      R => \^sr\(0)
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg0(15),
      R => \^sr\(0)
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg0(16),
      R => \^sr\(0)
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg0(17),
      R => \^sr\(0)
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg0(18),
      R => \^sr\(0)
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg0(19),
      R => \^sr\(0)
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg0(1),
      R => \^sr\(0)
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg0(20),
      R => \^sr\(0)
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg0(21),
      R => \^sr\(0)
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg0(22),
      R => \^sr\(0)
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg0(23),
      R => \^sr\(0)
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg0(24),
      R => \^sr\(0)
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg0(25),
      R => \^sr\(0)
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg0(26),
      R => \^sr\(0)
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg0(27),
      R => \^sr\(0)
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg0(28),
      R => \^sr\(0)
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg0(29),
      R => \^sr\(0)
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg0(2),
      R => \^sr\(0)
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg0(30),
      R => \^sr\(0)
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg0(31),
      R => \^sr\(0)
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg0(3),
      R => \^sr\(0)
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg0(4),
      R => \^sr\(0)
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg0(5),
      R => \^sr\(0)
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg0(6),
      R => \^sr\(0)
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg0(7),
      R => \^sr\(0)
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg0(8),
      R => \^sr\(0)
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg0(9),
      R => \^sr\(0)
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => \^sr\(0)
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => \^sr\(0)
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => \^sr\(0)
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => \^sr\(0)
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => \^sr\(0)
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => \^sr\(0)
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => \^sr\(0)
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => \^sr\(0)
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => \^sr\(0)
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => \^sr\(0)
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => \^sr\(0)
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => \^sr\(0)
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => \^sr\(0)
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => \^sr\(0)
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => \^sr\(0)
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => \^sr\(0)
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => \^sr\(0)
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => \^sr\(0)
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => \^sr\(0)
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => \^sr\(0)
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => \^sr\(0)
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => \^sr\(0)
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => \^sr\(0)
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => \^sr\(0)
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => \^sr\(0)
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => \^sr\(0)
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => \^sr\(0)
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => \^sr\(0)
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => \^sr\(0)
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => \^sr\(0)
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => \^sr\(0)
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => \^sr\(0)
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      O => p_1_in(15)
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      O => \^e\(0)
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      O => p_1_in(31)
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      O => p_1_in(7)
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => \^slv_reg2_reg[16]_0\(0),
      R => \^sr\(0)
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => \^slv_reg2_reg[16]_0\(10),
      R => \^sr\(0)
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => \^slv_reg2_reg[16]_0\(11),
      R => \^sr\(0)
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => \^slv_reg2_reg[16]_0\(12),
      R => \^sr\(0)
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => \^slv_reg2_reg[16]_0\(13),
      R => \^sr\(0)
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => \^slv_reg2_reg[16]_0\(14),
      R => \^sr\(0)
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => \^slv_reg2_reg[16]_0\(15),
      R => \^sr\(0)
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(16),
      Q => \^slv_reg2_reg[16]_0\(16),
      R => \^sr\(0)
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => \^sr\(0)
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => \^sr\(0)
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => \^sr\(0)
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => \^slv_reg2_reg[16]_0\(1),
      R => \^sr\(0)
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => \^sr\(0)
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => \^sr\(0)
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => \^sr\(0)
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^e\(0),
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => \^sr\(0)
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => \^sr\(0)
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => \^sr\(0)
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => \^sr\(0)
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => \^sr\(0)
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => \^sr\(0)
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => \^sr\(0)
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => \^slv_reg2_reg[16]_0\(2),
      R => \^sr\(0)
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => \^sr\(0)
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => \^sr\(0)
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => \^slv_reg2_reg[16]_0\(3),
      R => \^sr\(0)
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => \^slv_reg2_reg[16]_0\(4),
      R => \^sr\(0)
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => \^slv_reg2_reg[16]_0\(5),
      R => \^sr\(0)
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => \^slv_reg2_reg[16]_0\(6),
      R => \^sr\(0)
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => \^slv_reg2_reg[16]_0\(7),
      R => \^sr\(0)
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => \^slv_reg2_reg[16]_0\(8),
      R => \^sr\(0)
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => \^slv_reg2_reg[16]_0\(9),
      R => \^sr\(0)
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(2),
      O => \^axi_awaddr_reg[4]_0\(0)
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg3_reg[16]_0\(0),
      R => \^sr\(0)
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^slv_reg3_reg[16]_0\(10),
      R => \^sr\(0)
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^slv_reg3_reg[16]_0\(11),
      R => \^sr\(0)
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^slv_reg3_reg[16]_0\(12),
      R => \^sr\(0)
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^slv_reg3_reg[16]_0\(13),
      R => \^sr\(0)
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^slv_reg3_reg[16]_0\(14),
      R => \^sr\(0)
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^slv_reg3_reg[16]_0\(15),
      R => \^sr\(0)
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(16),
      Q => \^slv_reg3_reg[16]_0\(16),
      R => \^sr\(0)
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => \^sr\(0)
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => \^sr\(0)
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => \^sr\(0)
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg3_reg[16]_0\(1),
      R => \^sr\(0)
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => \^sr\(0)
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => \^sr\(0)
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => \^sr\(0)
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \^axi_awaddr_reg[4]_0\(0),
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => \^sr\(0)
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => \^sr\(0)
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => \^sr\(0)
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => \^sr\(0)
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => \^sr\(0)
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => \^sr\(0)
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => \^sr\(0)
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg3_reg[16]_0\(2),
      R => \^sr\(0)
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => \^sr\(0)
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => \^sr\(0)
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^slv_reg3_reg[16]_0\(3),
      R => \^sr\(0)
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^slv_reg3_reg[16]_0\(4),
      R => \^sr\(0)
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^slv_reg3_reg[16]_0\(5),
      R => \^sr\(0)
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^slv_reg3_reg[16]_0\(6),
      R => \^sr\(0)
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^slv_reg3_reg[16]_0\(7),
      R => \^sr\(0)
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^slv_reg3_reg[16]_0\(8),
      R => \^sr\(0)
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^slv_reg3_reg[16]_0\(9),
      R => \^sr\(0)
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^q\(0),
      R => \^sr\(0)
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^q\(10),
      R => \^sr\(0)
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^q\(11),
      R => \^sr\(0)
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^q\(12),
      R => \^sr\(0)
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^q\(13),
      R => \^sr\(0)
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^q\(14),
      R => \^sr\(0)
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^q\(15),
      R => \^sr\(0)
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^q\(16),
      R => \^sr\(0)
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^q\(17),
      R => \^sr\(0)
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^q\(18),
      R => \^sr\(0)
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^q\(19),
      R => \^sr\(0)
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^q\(1),
      R => \^sr\(0)
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^q\(20),
      R => \^sr\(0)
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^q\(21),
      R => \^sr\(0)
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^q\(22),
      R => \^sr\(0)
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^q\(23),
      R => \^sr\(0)
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => \^sr\(0)
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => \^sr\(0)
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => \^sr\(0)
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => \^sr\(0)
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => \^sr\(0)
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => \^sr\(0)
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^q\(2),
      R => \^sr\(0)
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => \^sr\(0)
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => \^sr\(0)
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^q\(3),
      R => \^sr\(0)
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^q\(4),
      R => \^sr\(0)
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^q\(5),
      R => \^sr\(0)
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^q\(6),
      R => \^sr\(0)
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^q\(7),
      R => \^sr\(0)
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^q\(8),
      R => \^sr\(0)
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^q\(9),
      R => \^sr\(0)
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(0),
      Q => slv_reg5(0),
      R => '0'
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(10),
      Q => slv_reg5(10),
      R => '0'
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(11),
      Q => slv_reg5(11),
      R => '0'
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(12),
      Q => slv_reg5(12),
      R => '0'
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(13),
      Q => slv_reg5(13),
      R => '0'
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(14),
      Q => slv_reg5(14),
      R => '0'
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(15),
      Q => slv_reg5(15),
      R => '0'
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(16),
      Q => slv_reg5(16),
      R => '0'
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(17),
      Q => slv_reg5(17),
      R => '0'
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(18),
      Q => slv_reg5(18),
      R => '0'
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(19),
      Q => slv_reg5(19),
      R => '0'
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(1),
      Q => slv_reg5(1),
      R => '0'
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(20),
      Q => slv_reg5(20),
      R => '0'
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(21),
      Q => slv_reg5(21),
      R => '0'
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(22),
      Q => slv_reg5(22),
      R => '0'
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(23),
      Q => slv_reg5(23),
      R => '0'
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(2),
      Q => slv_reg5(2),
      R => '0'
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(3),
      Q => slv_reg5(3),
      R => '0'
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(4),
      Q => slv_reg5(4),
      R => '0'
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(5),
      Q => slv_reg5(5),
      R => '0'
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(6),
      Q => slv_reg5(6),
      R => '0'
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(7),
      Q => slv_reg5(7),
      R => '0'
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(8),
      Q => slv_reg5(8),
      R => '0'
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => D(9),
      Q => slv_reg5(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 23 downto 0 );
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    P : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg is
  signal \^out\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \q[0]_i_1_n_0\ : STD_LOGIC;
  signal \q[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \q[12]_i_3__0_n_0\ : STD_LOGIC;
  signal \q[12]_i_4__0_n_0\ : STD_LOGIC;
  signal \q[12]_i_5__0_n_0\ : STD_LOGIC;
  signal \q[16]_i_2__0_n_0\ : STD_LOGIC;
  signal \q[16]_i_3__0_n_0\ : STD_LOGIC;
  signal \q[16]_i_4__0_n_0\ : STD_LOGIC;
  signal \q[16]_i_5__0_n_0\ : STD_LOGIC;
  signal \q[20]_i_2__0_n_0\ : STD_LOGIC;
  signal \q[20]_i_3__0_n_0\ : STD_LOGIC;
  signal \q[20]_i_4__0_n_0\ : STD_LOGIC;
  signal \q[20]_i_5__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_3__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_4__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \q[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \q[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \q[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \q[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \q_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_0\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_1\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_4\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \q_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_1\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_2\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_3\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_4\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_5\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_6\ : STD_LOGIC;
  signal \q_reg[20]_i_1__0_n_7\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \q_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \q_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \slv_reg5[0]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[0]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[0]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[0]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[11]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[11]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[11]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[11]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[15]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[15]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[15]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[15]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[19]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[19]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[19]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[19]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[3]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[3]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[3]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[3]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_2_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_3_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_4_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_5_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \slv_reg5_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \slv_reg5_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \slv_reg5_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \slv_reg5_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \slv_reg5_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \NLW_q_reg[20]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_slv_reg5_reg[23]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_slv_reg5_reg[3]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  \out\(23 downto 0) <= \^out\(23 downto 0);
\q[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(0),
      I1 => \^out\(0),
      O => \q[0]_i_1_n_0\
    );
\q[12]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(15),
      I1 => \^out\(15),
      O => \q[12]_i_2__0_n_0\
    );
\q[12]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(14),
      I1 => \^out\(14),
      O => \q[12]_i_3__0_n_0\
    );
\q[12]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(13),
      I1 => \^out\(13),
      O => \q[12]_i_4__0_n_0\
    );
\q[12]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(12),
      I1 => \^out\(12),
      O => \q[12]_i_5__0_n_0\
    );
\q[16]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(19),
      I1 => \^out\(19),
      O => \q[16]_i_2__0_n_0\
    );
\q[16]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(18),
      I1 => \^out\(18),
      O => \q[16]_i_3__0_n_0\
    );
\q[16]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(17),
      I1 => \^out\(17),
      O => \q[16]_i_4__0_n_0\
    );
\q[16]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(16),
      I1 => \^out\(16),
      O => \q[16]_i_5__0_n_0\
    );
\q[20]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(23),
      I1 => \^out\(23),
      O => \q[20]_i_2__0_n_0\
    );
\q[20]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(22),
      I1 => \^out\(22),
      O => \q[20]_i_3__0_n_0\
    );
\q[20]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(21),
      I1 => \^out\(21),
      O => \q[20]_i_4__0_n_0\
    );
\q[20]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(20),
      I1 => \^out\(20),
      O => \q[20]_i_5__0_n_0\
    );
\q[4]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(7),
      I1 => \^out\(7),
      O => \q[4]_i_2__0_n_0\
    );
\q[4]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(6),
      I1 => \^out\(6),
      O => \q[4]_i_3__0_n_0\
    );
\q[4]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(5),
      I1 => \^out\(5),
      O => \q[4]_i_4__0_n_0\
    );
\q[4]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(4),
      I1 => \^out\(4),
      O => \q[4]_i_5__0_n_0\
    );
\q[8]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(11),
      I1 => \^out\(11),
      O => \q[8]_i_2__0_n_0\
    );
\q[8]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(10),
      I1 => \^out\(10),
      O => \q[8]_i_3__0_n_0\
    );
\q[8]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(9),
      I1 => \^out\(9),
      O => \q[8]_i_4__0_n_0\
    );
\q[8]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(8),
      I1 => \^out\(8),
      O => \q[8]_i_5__0_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q[0]_i_1_n_0\,
      Q => \^out\(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1__0_n_5\,
      Q => \^out\(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1__0_n_4\,
      Q => \^out\(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1__0_n_7\,
      Q => \^out\(12)
    );
\q_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[8]_i_1__0_n_0\,
      CO(3) => \q_reg[12]_i_1__0_n_0\,
      CO(2) => \q_reg[12]_i_1__0_n_1\,
      CO(1) => \q_reg[12]_i_1__0_n_2\,
      CO(0) => \q_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(15 downto 12),
      O(3) => \q_reg[12]_i_1__0_n_4\,
      O(2) => \q_reg[12]_i_1__0_n_5\,
      O(1) => \q_reg[12]_i_1__0_n_6\,
      O(0) => \q_reg[12]_i_1__0_n_7\,
      S(3) => \q[12]_i_2__0_n_0\,
      S(2) => \q[12]_i_3__0_n_0\,
      S(1) => \q[12]_i_4__0_n_0\,
      S(0) => \q[12]_i_5__0_n_0\
    );
\q_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1__0_n_6\,
      Q => \^out\(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1__0_n_5\,
      Q => \^out\(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1__0_n_4\,
      Q => \^out\(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1__0_n_7\,
      Q => \^out\(16)
    );
\q_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[12]_i_1__0_n_0\,
      CO(3) => \q_reg[16]_i_1__0_n_0\,
      CO(2) => \q_reg[16]_i_1__0_n_1\,
      CO(1) => \q_reg[16]_i_1__0_n_2\,
      CO(0) => \q_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(19 downto 16),
      O(3) => \q_reg[16]_i_1__0_n_4\,
      O(2) => \q_reg[16]_i_1__0_n_5\,
      O(1) => \q_reg[16]_i_1__0_n_6\,
      O(0) => \q_reg[16]_i_1__0_n_7\,
      S(3) => \q[16]_i_2__0_n_0\,
      S(2) => \q[16]_i_3__0_n_0\,
      S(1) => \q[16]_i_4__0_n_0\,
      S(0) => \q[16]_i_5__0_n_0\
    );
\q_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1__0_n_6\,
      Q => \^out\(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1__0_n_5\,
      Q => \^out\(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1__0_n_4\,
      Q => \^out\(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \slv_reg5_reg[0]_i_1_n_6\,
      Q => \^out\(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1__0_n_7\,
      Q => \^out\(20)
    );
\q_reg[20]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[16]_i_1__0_n_0\,
      CO(3) => \NLW_q_reg[20]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \q_reg[20]_i_1__0_n_1\,
      CO(1) => \q_reg[20]_i_1__0_n_2\,
      CO(0) => \q_reg[20]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => P(22 downto 20),
      O(3) => \q_reg[20]_i_1__0_n_4\,
      O(2) => \q_reg[20]_i_1__0_n_5\,
      O(1) => \q_reg[20]_i_1__0_n_6\,
      O(0) => \q_reg[20]_i_1__0_n_7\,
      S(3) => \q[20]_i_2__0_n_0\,
      S(2) => \q[20]_i_3__0_n_0\,
      S(1) => \q[20]_i_4__0_n_0\,
      S(0) => \q[20]_i_5__0_n_0\
    );
\q_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1__0_n_6\,
      Q => \^out\(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1__0_n_5\,
      Q => \^out\(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1__0_n_4\,
      Q => \^out\(23)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \slv_reg5_reg[0]_i_1_n_5\,
      Q => \^out\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \slv_reg5_reg[0]_i_1_n_4\,
      Q => \^out\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1__0_n_7\,
      Q => \^out\(4)
    );
\q_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[0]_i_1_n_0\,
      CO(3) => \q_reg[4]_i_1__0_n_0\,
      CO(2) => \q_reg[4]_i_1__0_n_1\,
      CO(1) => \q_reg[4]_i_1__0_n_2\,
      CO(0) => \q_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(7 downto 4),
      O(3) => \q_reg[4]_i_1__0_n_4\,
      O(2) => \q_reg[4]_i_1__0_n_5\,
      O(1) => \q_reg[4]_i_1__0_n_6\,
      O(0) => \q_reg[4]_i_1__0_n_7\,
      S(3) => \q[4]_i_2__0_n_0\,
      S(2) => \q[4]_i_3__0_n_0\,
      S(1) => \q[4]_i_4__0_n_0\,
      S(0) => \q[4]_i_5__0_n_0\
    );
\q_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1__0_n_6\,
      Q => \^out\(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1__0_n_5\,
      Q => \^out\(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1__0_n_4\,
      Q => \^out\(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1__0_n_7\,
      Q => \^out\(8)
    );
\q_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[4]_i_1__0_n_0\,
      CO(3) => \q_reg[8]_i_1__0_n_0\,
      CO(2) => \q_reg[8]_i_1__0_n_1\,
      CO(1) => \q_reg[8]_i_1__0_n_2\,
      CO(0) => \q_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(11 downto 8),
      O(3) => \q_reg[8]_i_1__0_n_4\,
      O(2) => \q_reg[8]_i_1__0_n_5\,
      O(1) => \q_reg[8]_i_1__0_n_6\,
      O(0) => \q_reg[8]_i_1__0_n_7\,
      S(3) => \q[8]_i_2__0_n_0\,
      S(2) => \q[8]_i_3__0_n_0\,
      S(1) => \q[8]_i_4__0_n_0\,
      S(0) => \q[8]_i_5__0_n_0\
    );
\q_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1__0_n_6\,
      Q => \^out\(9)
    );
\slv_reg5[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(3),
      I1 => \^out\(3),
      O => \slv_reg5[0]_i_2_n_0\
    );
\slv_reg5[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(2),
      I1 => \^out\(2),
      O => \slv_reg5[0]_i_3_n_0\
    );
\slv_reg5[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(1),
      I1 => \^out\(1),
      O => \slv_reg5[0]_i_4_n_0\
    );
\slv_reg5[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(0),
      I1 => \^out\(0),
      O => \slv_reg5[0]_i_5_n_0\
    );
\slv_reg5[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(11),
      I1 => \^out\(11),
      O => \slv_reg5[11]_i_2_n_0\
    );
\slv_reg5[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(10),
      I1 => \^out\(10),
      O => \slv_reg5[11]_i_3_n_0\
    );
\slv_reg5[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(9),
      I1 => \^out\(9),
      O => \slv_reg5[11]_i_4_n_0\
    );
\slv_reg5[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(8),
      I1 => \^out\(8),
      O => \slv_reg5[11]_i_5_n_0\
    );
\slv_reg5[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(15),
      I1 => \^out\(15),
      O => \slv_reg5[15]_i_2_n_0\
    );
\slv_reg5[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(14),
      I1 => \^out\(14),
      O => \slv_reg5[15]_i_3_n_0\
    );
\slv_reg5[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(13),
      I1 => \^out\(13),
      O => \slv_reg5[15]_i_4_n_0\
    );
\slv_reg5[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(12),
      I1 => \^out\(12),
      O => \slv_reg5[15]_i_5_n_0\
    );
\slv_reg5[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(19),
      I1 => \^out\(19),
      O => \slv_reg5[19]_i_2_n_0\
    );
\slv_reg5[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(18),
      I1 => \^out\(18),
      O => \slv_reg5[19]_i_3_n_0\
    );
\slv_reg5[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(17),
      I1 => \^out\(17),
      O => \slv_reg5[19]_i_4_n_0\
    );
\slv_reg5[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(16),
      I1 => \^out\(16),
      O => \slv_reg5[19]_i_5_n_0\
    );
\slv_reg5[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(23),
      I1 => \^out\(23),
      O => \slv_reg5[23]_i_2_n_0\
    );
\slv_reg5[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(22),
      I1 => \^out\(22),
      O => \slv_reg5[23]_i_3_n_0\
    );
\slv_reg5[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(21),
      I1 => \^out\(21),
      O => \slv_reg5[23]_i_4_n_0\
    );
\slv_reg5[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(20),
      I1 => \^out\(20),
      O => \slv_reg5[23]_i_5_n_0\
    );
\slv_reg5[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(3),
      I1 => \^out\(3),
      O => \slv_reg5[3]_i_2_n_0\
    );
\slv_reg5[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(2),
      I1 => \^out\(2),
      O => \slv_reg5[3]_i_3_n_0\
    );
\slv_reg5[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(1),
      I1 => \^out\(1),
      O => \slv_reg5[3]_i_4_n_0\
    );
\slv_reg5[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(0),
      I1 => \^out\(0),
      O => \slv_reg5[3]_i_5_n_0\
    );
\slv_reg5[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(7),
      I1 => \^out\(7),
      O => \slv_reg5[7]_i_2_n_0\
    );
\slv_reg5[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(6),
      I1 => \^out\(6),
      O => \slv_reg5[7]_i_3_n_0\
    );
\slv_reg5[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(5),
      I1 => \^out\(5),
      O => \slv_reg5[7]_i_4_n_0\
    );
\slv_reg5[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(4),
      I1 => \^out\(4),
      O => \slv_reg5[7]_i_5_n_0\
    );
\slv_reg5_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \slv_reg5_reg[0]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[0]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[0]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(3 downto 0),
      O(3) => \slv_reg5_reg[0]_i_1_n_4\,
      O(2) => \slv_reg5_reg[0]_i_1_n_5\,
      O(1) => \slv_reg5_reg[0]_i_1_n_6\,
      O(0) => D(0),
      S(3) => \slv_reg5[0]_i_2_n_0\,
      S(2) => \slv_reg5[0]_i_3_n_0\,
      S(1) => \slv_reg5[0]_i_4_n_0\,
      S(0) => \slv_reg5[0]_i_5_n_0\
    );
\slv_reg5_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[7]_i_1_n_0\,
      CO(3) => \slv_reg5_reg[11]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[11]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[11]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(11 downto 8),
      O(3 downto 0) => D(11 downto 8),
      S(3) => \slv_reg5[11]_i_2_n_0\,
      S(2) => \slv_reg5[11]_i_3_n_0\,
      S(1) => \slv_reg5[11]_i_4_n_0\,
      S(0) => \slv_reg5[11]_i_5_n_0\
    );
\slv_reg5_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[11]_i_1_n_0\,
      CO(3) => \slv_reg5_reg[15]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[15]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[15]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(15 downto 12),
      O(3 downto 0) => D(15 downto 12),
      S(3) => \slv_reg5[15]_i_2_n_0\,
      S(2) => \slv_reg5[15]_i_3_n_0\,
      S(1) => \slv_reg5[15]_i_4_n_0\,
      S(0) => \slv_reg5[15]_i_5_n_0\
    );
\slv_reg5_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[15]_i_1_n_0\,
      CO(3) => \slv_reg5_reg[19]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[19]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[19]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(19 downto 16),
      O(3 downto 0) => D(19 downto 16),
      S(3) => \slv_reg5[19]_i_2_n_0\,
      S(2) => \slv_reg5[19]_i_3_n_0\,
      S(1) => \slv_reg5[19]_i_4_n_0\,
      S(0) => \slv_reg5[19]_i_5_n_0\
    );
\slv_reg5_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[19]_i_1_n_0\,
      CO(3) => \NLW_slv_reg5_reg[23]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \slv_reg5_reg[23]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[23]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => P(22 downto 20),
      O(3 downto 0) => D(23 downto 20),
      S(3) => \slv_reg5[23]_i_2_n_0\,
      S(2) => \slv_reg5[23]_i_3_n_0\,
      S(1) => \slv_reg5[23]_i_4_n_0\,
      S(0) => \slv_reg5[23]_i_5_n_0\
    );
\slv_reg5_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \slv_reg5_reg[3]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[3]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[3]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(3 downto 0),
      O(3 downto 1) => D(3 downto 1),
      O(0) => \NLW_slv_reg5_reg[3]_i_1_O_UNCONNECTED\(0),
      S(3) => \slv_reg5[3]_i_2_n_0\,
      S(2) => \slv_reg5[3]_i_3_n_0\,
      S(1) => \slv_reg5[3]_i_4_n_0\,
      S(0) => \slv_reg5[3]_i_5_n_0\
    );
\slv_reg5_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \slv_reg5_reg[3]_i_1_n_0\,
      CO(3) => \slv_reg5_reg[7]_i_1_n_0\,
      CO(2) => \slv_reg5_reg[7]_i_1_n_1\,
      CO(1) => \slv_reg5_reg[7]_i_1_n_2\,
      CO(0) => \slv_reg5_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => P(7 downto 4),
      O(3 downto 0) => D(7 downto 4),
      S(3) => \slv_reg5[7]_i_2_n_0\,
      S(2) => \slv_reg5[7]_i_3_n_0\,
      S(1) => \slv_reg5[7]_i_4_n_0\,
      S(0) => \slv_reg5[7]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0 is
  port (
    S : out STD_LOGIC_VECTOR ( 0 to 0 );
    \multi_2_s__0\ : out STD_LOGIC_VECTOR ( 22 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    P : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[23]_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0 : entity is "reg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0 is
  signal \adder_1_s__0_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_9_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_9_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_9_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_9_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_9_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_9_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_9_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_9_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_9_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_9_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_9_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_9_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_8_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_8_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_8_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_10_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_11_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_12_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_8_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_8_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_8_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_8_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_9_n_0\ : STD_LOGIC;
  signal adder_3_s : STD_LOGIC_VECTOR ( 23 to 23 );
  signal \^multi_2_s__0\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal \q[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[12]_i_2_n_0\ : STD_LOGIC;
  signal \q[12]_i_3_n_0\ : STD_LOGIC;
  signal \q[12]_i_4_n_0\ : STD_LOGIC;
  signal \q[12]_i_5_n_0\ : STD_LOGIC;
  signal \q[16]_i_2_n_0\ : STD_LOGIC;
  signal \q[16]_i_3_n_0\ : STD_LOGIC;
  signal \q[16]_i_4_n_0\ : STD_LOGIC;
  signal \q[16]_i_5_n_0\ : STD_LOGIC;
  signal \q[1]_i_2_n_0\ : STD_LOGIC;
  signal \q[1]_i_3_n_0\ : STD_LOGIC;
  signal \q[1]_i_4_n_0\ : STD_LOGIC;
  signal \q[1]_i_5_n_0\ : STD_LOGIC;
  signal \q[20]_i_2_n_0\ : STD_LOGIC;
  signal \q[20]_i_3_n_0\ : STD_LOGIC;
  signal \q[20]_i_4_n_0\ : STD_LOGIC;
  signal \q[20]_i_5_n_0\ : STD_LOGIC;
  signal \q[4]_i_2_n_0\ : STD_LOGIC;
  signal \q[4]_i_3_n_0\ : STD_LOGIC;
  signal \q[4]_i_4_n_0\ : STD_LOGIC;
  signal \q[4]_i_5_n_0\ : STD_LOGIC;
  signal \q[8]_i_2_n_0\ : STD_LOGIC;
  signal \q[8]_i_3_n_0\ : STD_LOGIC;
  signal \q[8]_i_4_n_0\ : STD_LOGIC;
  signal \q[8]_i_5_n_0\ : STD_LOGIC;
  signal q_reg : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \q_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[1]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_adder_1_s__0_carry__4_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_adder_1_s__0_carry_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_q_reg[20]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  \multi_2_s__0\(22 downto 0) <= \^multi_2_s__0\(22 downto 0);
\adder_1_s__0_carry__0_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(7),
      I1 => q_reg(7),
      O => \adder_1_s__0_carry__0_i_10_n_0\
    );
\adder_1_s__0_carry__0_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(6),
      I1 => q_reg(6),
      O => \adder_1_s__0_carry__0_i_11_n_0\
    );
\adder_1_s__0_carry__0_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(5),
      I1 => q_reg(5),
      O => \adder_1_s__0_carry__0_i_12_n_0\
    );
\adder_1_s__0_carry__0_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(4),
      I1 => q_reg(4),
      O => \adder_1_s__0_carry__0_i_13_n_0\
    );
\adder_1_s__0_carry__0_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry_i_8_n_0\,
      CO(3) => \adder_1_s__0_carry__0_i_9_n_0\,
      CO(2) => \adder_1_s__0_carry__0_i_9_n_1\,
      CO(1) => \adder_1_s__0_carry__0_i_9_n_2\,
      CO(0) => \adder_1_s__0_carry__0_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(7 downto 4),
      O(3 downto 0) => \^multi_2_s__0\(7 downto 4),
      S(3) => \adder_1_s__0_carry__0_i_10_n_0\,
      S(2) => \adder_1_s__0_carry__0_i_11_n_0\,
      S(1) => \adder_1_s__0_carry__0_i_12_n_0\,
      S(0) => \adder_1_s__0_carry__0_i_13_n_0\
    );
\adder_1_s__0_carry__1_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(11),
      I1 => q_reg(11),
      O => \adder_1_s__0_carry__1_i_10_n_0\
    );
\adder_1_s__0_carry__1_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(10),
      I1 => q_reg(10),
      O => \adder_1_s__0_carry__1_i_11_n_0\
    );
\adder_1_s__0_carry__1_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(9),
      I1 => q_reg(9),
      O => \adder_1_s__0_carry__1_i_12_n_0\
    );
\adder_1_s__0_carry__1_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(8),
      I1 => q_reg(8),
      O => \adder_1_s__0_carry__1_i_13_n_0\
    );
\adder_1_s__0_carry__1_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__0_i_9_n_0\,
      CO(3) => \adder_1_s__0_carry__1_i_9_n_0\,
      CO(2) => \adder_1_s__0_carry__1_i_9_n_1\,
      CO(1) => \adder_1_s__0_carry__1_i_9_n_2\,
      CO(0) => \adder_1_s__0_carry__1_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(11 downto 8),
      O(3 downto 0) => \^multi_2_s__0\(11 downto 8),
      S(3) => \adder_1_s__0_carry__1_i_10_n_0\,
      S(2) => \adder_1_s__0_carry__1_i_11_n_0\,
      S(1) => \adder_1_s__0_carry__1_i_12_n_0\,
      S(0) => \adder_1_s__0_carry__1_i_13_n_0\
    );
\adder_1_s__0_carry__2_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(15),
      I1 => q_reg(15),
      O => \adder_1_s__0_carry__2_i_10_n_0\
    );
\adder_1_s__0_carry__2_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(14),
      I1 => q_reg(14),
      O => \adder_1_s__0_carry__2_i_11_n_0\
    );
\adder_1_s__0_carry__2_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(13),
      I1 => q_reg(13),
      O => \adder_1_s__0_carry__2_i_12_n_0\
    );
\adder_1_s__0_carry__2_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(12),
      I1 => q_reg(12),
      O => \adder_1_s__0_carry__2_i_13_n_0\
    );
\adder_1_s__0_carry__2_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__1_i_9_n_0\,
      CO(3) => \adder_1_s__0_carry__2_i_9_n_0\,
      CO(2) => \adder_1_s__0_carry__2_i_9_n_1\,
      CO(1) => \adder_1_s__0_carry__2_i_9_n_2\,
      CO(0) => \adder_1_s__0_carry__2_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(15 downto 12),
      O(3 downto 0) => \^multi_2_s__0\(15 downto 12),
      S(3) => \adder_1_s__0_carry__2_i_10_n_0\,
      S(2) => \adder_1_s__0_carry__2_i_11_n_0\,
      S(1) => \adder_1_s__0_carry__2_i_12_n_0\,
      S(0) => \adder_1_s__0_carry__2_i_13_n_0\
    );
\adder_1_s__0_carry__3_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(19),
      I1 => q_reg(19),
      O => \adder_1_s__0_carry__3_i_10_n_0\
    );
\adder_1_s__0_carry__3_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(18),
      I1 => q_reg(18),
      O => \adder_1_s__0_carry__3_i_11_n_0\
    );
\adder_1_s__0_carry__3_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(17),
      I1 => q_reg(17),
      O => \adder_1_s__0_carry__3_i_12_n_0\
    );
\adder_1_s__0_carry__3_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(16),
      I1 => q_reg(16),
      O => \adder_1_s__0_carry__3_i_13_n_0\
    );
\adder_1_s__0_carry__3_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__2_i_9_n_0\,
      CO(3) => \adder_1_s__0_carry__3_i_9_n_0\,
      CO(2) => \adder_1_s__0_carry__3_i_9_n_1\,
      CO(1) => \adder_1_s__0_carry__3_i_9_n_2\,
      CO(0) => \adder_1_s__0_carry__3_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(19 downto 16),
      O(3 downto 0) => \^multi_2_s__0\(19 downto 16),
      S(3) => \adder_1_s__0_carry__3_i_10_n_0\,
      S(2) => \adder_1_s__0_carry__3_i_11_n_0\,
      S(1) => \adder_1_s__0_carry__3_i_12_n_0\,
      S(0) => \adder_1_s__0_carry__3_i_13_n_0\
    );
\adder_1_s__0_carry__4_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(22),
      I1 => q_reg(22),
      O => \adder_1_s__0_carry__4_i_10_n_0\
    );
\adder_1_s__0_carry__4_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(21),
      I1 => q_reg(21),
      O => \adder_1_s__0_carry__4_i_11_n_0\
    );
\adder_1_s__0_carry__4_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(20),
      I1 => q_reg(20),
      O => \adder_1_s__0_carry__4_i_12_n_0\
    );
\adder_1_s__0_carry__4_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D42B2BD42BD4D42B"
    )
        port map (
      I0 => Q(0),
      I1 => \^multi_2_s__0\(22),
      I2 => P(0),
      I3 => adder_3_s(23),
      I4 => P(1),
      I5 => Q(1),
      O => S(0)
    );
\adder_1_s__0_carry__4_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__3_i_9_n_0\,
      CO(3) => \NLW_adder_1_s__0_carry__4_i_8_CO_UNCONNECTED\(3),
      CO(2) => \adder_1_s__0_carry__4_i_8_n_1\,
      CO(1) => \adder_1_s__0_carry__4_i_8_n_2\,
      CO(0) => \adder_1_s__0_carry__4_i_8_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \q_reg[23]_0\(22 downto 20),
      O(3) => adder_3_s(23),
      O(2 downto 0) => \^multi_2_s__0\(22 downto 20),
      S(3) => \adder_1_s__0_carry__4_i_9_n_0\,
      S(2) => \adder_1_s__0_carry__4_i_10_n_0\,
      S(1) => \adder_1_s__0_carry__4_i_11_n_0\,
      S(0) => \adder_1_s__0_carry__4_i_12_n_0\
    );
\adder_1_s__0_carry__4_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(23),
      I1 => q_reg(23),
      O => \adder_1_s__0_carry__4_i_9_n_0\
    );
\adder_1_s__0_carry_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(2),
      I1 => q_reg(2),
      O => \adder_1_s__0_carry_i_10_n_0\
    );
\adder_1_s__0_carry_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(1),
      I1 => q_reg(1),
      O => \adder_1_s__0_carry_i_11_n_0\
    );
\adder_1_s__0_carry_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(0),
      I1 => q_reg(0),
      O => \adder_1_s__0_carry_i_12_n_0\
    );
\adder_1_s__0_carry_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \adder_1_s__0_carry_i_8_n_0\,
      CO(2) => \adder_1_s__0_carry_i_8_n_1\,
      CO(1) => \adder_1_s__0_carry_i_8_n_2\,
      CO(0) => \adder_1_s__0_carry_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(3 downto 0),
      O(3 downto 1) => \^multi_2_s__0\(3 downto 1),
      O(0) => \NLW_adder_1_s__0_carry_i_8_O_UNCONNECTED\(0),
      S(3) => \adder_1_s__0_carry_i_9_n_0\,
      S(2) => \adder_1_s__0_carry_i_10_n_0\,
      S(1) => \adder_1_s__0_carry_i_11_n_0\,
      S(0) => \adder_1_s__0_carry_i_12_n_0\
    );
\adder_1_s__0_carry_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(3),
      I1 => q_reg(3),
      O => \adder_1_s__0_carry_i_9_n_0\
    );
\q[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(0),
      I1 => q_reg(0),
      O => \q[0]_i_1__0_n_0\
    );
\q[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(15),
      I1 => q_reg(15),
      O => \q[12]_i_2_n_0\
    );
\q[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(14),
      I1 => q_reg(14),
      O => \q[12]_i_3_n_0\
    );
\q[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(13),
      I1 => q_reg(13),
      O => \q[12]_i_4_n_0\
    );
\q[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(12),
      I1 => q_reg(12),
      O => \q[12]_i_5_n_0\
    );
\q[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(19),
      I1 => q_reg(19),
      O => \q[16]_i_2_n_0\
    );
\q[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(18),
      I1 => q_reg(18),
      O => \q[16]_i_3_n_0\
    );
\q[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(17),
      I1 => q_reg(17),
      O => \q[16]_i_4_n_0\
    );
\q[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(16),
      I1 => q_reg(16),
      O => \q[16]_i_5_n_0\
    );
\q[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(3),
      I1 => q_reg(3),
      O => \q[1]_i_2_n_0\
    );
\q[1]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(2),
      I1 => q_reg(2),
      O => \q[1]_i_3_n_0\
    );
\q[1]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(1),
      I1 => q_reg(1),
      O => \q[1]_i_4_n_0\
    );
\q[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(0),
      I1 => q_reg(0),
      O => \q[1]_i_5_n_0\
    );
\q[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(23),
      I1 => q_reg(23),
      O => \q[20]_i_2_n_0\
    );
\q[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(22),
      I1 => q_reg(22),
      O => \q[20]_i_3_n_0\
    );
\q[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(21),
      I1 => q_reg(21),
      O => \q[20]_i_4_n_0\
    );
\q[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(20),
      I1 => q_reg(20),
      O => \q[20]_i_5_n_0\
    );
\q[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(7),
      I1 => q_reg(7),
      O => \q[4]_i_2_n_0\
    );
\q[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(6),
      I1 => q_reg(6),
      O => \q[4]_i_3_n_0\
    );
\q[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(5),
      I1 => q_reg(5),
      O => \q[4]_i_4_n_0\
    );
\q[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(4),
      I1 => q_reg(4),
      O => \q[4]_i_5_n_0\
    );
\q[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(11),
      I1 => q_reg(11),
      O => \q[8]_i_2_n_0\
    );
\q[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(10),
      I1 => q_reg(10),
      O => \q[8]_i_3_n_0\
    );
\q[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(9),
      I1 => q_reg(9),
      O => \q[8]_i_4_n_0\
    );
\q[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \q_reg[23]_0\(8),
      I1 => q_reg(8),
      O => \q[8]_i_5_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q[0]_i_1__0_n_0\,
      Q => q_reg(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1_n_5\,
      Q => q_reg(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1_n_4\,
      Q => q_reg(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1_n_7\,
      Q => q_reg(12)
    );
\q_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[8]_i_1_n_0\,
      CO(3) => \q_reg[12]_i_1_n_0\,
      CO(2) => \q_reg[12]_i_1_n_1\,
      CO(1) => \q_reg[12]_i_1_n_2\,
      CO(0) => \q_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(15 downto 12),
      O(3) => \q_reg[12]_i_1_n_4\,
      O(2) => \q_reg[12]_i_1_n_5\,
      O(1) => \q_reg[12]_i_1_n_6\,
      O(0) => \q_reg[12]_i_1_n_7\,
      S(3) => \q[12]_i_2_n_0\,
      S(2) => \q[12]_i_3_n_0\,
      S(1) => \q[12]_i_4_n_0\,
      S(0) => \q[12]_i_5_n_0\
    );
\q_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1_n_6\,
      Q => q_reg(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1_n_5\,
      Q => q_reg(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[12]_i_1_n_4\,
      Q => q_reg(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1_n_7\,
      Q => q_reg(16)
    );
\q_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[12]_i_1_n_0\,
      CO(3) => \q_reg[16]_i_1_n_0\,
      CO(2) => \q_reg[16]_i_1_n_1\,
      CO(1) => \q_reg[16]_i_1_n_2\,
      CO(0) => \q_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(19 downto 16),
      O(3) => \q_reg[16]_i_1_n_4\,
      O(2) => \q_reg[16]_i_1_n_5\,
      O(1) => \q_reg[16]_i_1_n_6\,
      O(0) => \q_reg[16]_i_1_n_7\,
      S(3) => \q[16]_i_2_n_0\,
      S(2) => \q[16]_i_3_n_0\,
      S(1) => \q[16]_i_4_n_0\,
      S(0) => \q[16]_i_5_n_0\
    );
\q_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1_n_6\,
      Q => q_reg(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1_n_5\,
      Q => q_reg(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[16]_i_1_n_4\,
      Q => q_reg(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[1]_i_1_n_6\,
      Q => q_reg(1)
    );
\q_reg[1]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \q_reg[1]_i_1_n_0\,
      CO(2) => \q_reg[1]_i_1_n_1\,
      CO(1) => \q_reg[1]_i_1_n_2\,
      CO(0) => \q_reg[1]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(3 downto 0),
      O(3) => \q_reg[1]_i_1_n_4\,
      O(2) => \q_reg[1]_i_1_n_5\,
      O(1) => \q_reg[1]_i_1_n_6\,
      O(0) => \^multi_2_s__0\(0),
      S(3) => \q[1]_i_2_n_0\,
      S(2) => \q[1]_i_3_n_0\,
      S(1) => \q[1]_i_4_n_0\,
      S(0) => \q[1]_i_5_n_0\
    );
\q_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1_n_7\,
      Q => q_reg(20)
    );
\q_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[16]_i_1_n_0\,
      CO(3) => \NLW_q_reg[20]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \q_reg[20]_i_1_n_1\,
      CO(1) => \q_reg[20]_i_1_n_2\,
      CO(0) => \q_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \q_reg[23]_0\(22 downto 20),
      O(3) => \q_reg[20]_i_1_n_4\,
      O(2) => \q_reg[20]_i_1_n_5\,
      O(1) => \q_reg[20]_i_1_n_6\,
      O(0) => \q_reg[20]_i_1_n_7\,
      S(3) => \q[20]_i_2_n_0\,
      S(2) => \q[20]_i_3_n_0\,
      S(1) => \q[20]_i_4_n_0\,
      S(0) => \q[20]_i_5_n_0\
    );
\q_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1_n_6\,
      Q => q_reg(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1_n_5\,
      Q => q_reg(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[20]_i_1_n_4\,
      Q => q_reg(23)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[1]_i_1_n_5\,
      Q => q_reg(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[1]_i_1_n_4\,
      Q => q_reg(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1_n_7\,
      Q => q_reg(4)
    );
\q_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[1]_i_1_n_0\,
      CO(3) => \q_reg[4]_i_1_n_0\,
      CO(2) => \q_reg[4]_i_1_n_1\,
      CO(1) => \q_reg[4]_i_1_n_2\,
      CO(0) => \q_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(7 downto 4),
      O(3) => \q_reg[4]_i_1_n_4\,
      O(2) => \q_reg[4]_i_1_n_5\,
      O(1) => \q_reg[4]_i_1_n_6\,
      O(0) => \q_reg[4]_i_1_n_7\,
      S(3) => \q[4]_i_2_n_0\,
      S(2) => \q[4]_i_3_n_0\,
      S(1) => \q[4]_i_4_n_0\,
      S(0) => \q[4]_i_5_n_0\
    );
\q_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1_n_6\,
      Q => q_reg(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1_n_5\,
      Q => q_reg(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[4]_i_1_n_4\,
      Q => q_reg(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1_n_7\,
      Q => q_reg(8)
    );
\q_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[4]_i_1_n_0\,
      CO(3) => \q_reg[8]_i_1_n_0\,
      CO(2) => \q_reg[8]_i_1_n_1\,
      CO(1) => \q_reg[8]_i_1_n_2\,
      CO(0) => \q_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_0\(11 downto 8),
      O(3) => \q_reg[8]_i_1_n_4\,
      O(2) => \q_reg[8]_i_1_n_5\,
      O(1) => \q_reg[8]_i_1_n_6\,
      O(0) => \q_reg[8]_i_1_n_7\,
      S(3) => \q[8]_i_2_n_0\,
      S(2) => \q[8]_i_3_n_0\,
      S(1) => \q[8]_i_4_n_0\,
      S(0) => \q[8]_i_5_n_0\
    );
\q_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => s00_axi_aresetn,
      D => \q_reg[8]_i_1_n_6\,
      Q => q_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq is
  port (
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    multi_3_s_0 : in STD_LOGIC_VECTOR ( 16 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 6 downto 0 );
    multi_2_s_0 : in STD_LOGIC_VECTOR ( 16 downto 0 );
    \multi_1_s__0_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq is
  signal A : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \adder_1_s__0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__0_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__1_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__2_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__3_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry__4_n_3\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_1_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_2_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_3_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_4_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_5_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_6_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_i_7_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_n_0\ : STD_LOGIC;
  signal \adder_1_s__0_carry_n_1\ : STD_LOGIC;
  signal \adder_1_s__0_carry_n_2\ : STD_LOGIC;
  signal \adder_1_s__0_carry_n_3\ : STD_LOGIC;
  signal adder_3_s : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal \multi_1_s__0_n_100\ : STD_LOGIC;
  signal \multi_1_s__0_n_101\ : STD_LOGIC;
  signal \multi_1_s__0_n_102\ : STD_LOGIC;
  signal \multi_1_s__0_n_103\ : STD_LOGIC;
  signal \multi_1_s__0_n_104\ : STD_LOGIC;
  signal \multi_1_s__0_n_105\ : STD_LOGIC;
  signal \multi_1_s__0_n_79\ : STD_LOGIC;
  signal \multi_1_s__0_n_80\ : STD_LOGIC;
  signal \multi_1_s__0_n_81\ : STD_LOGIC;
  signal \multi_1_s__0_n_82\ : STD_LOGIC;
  signal \multi_1_s__0_n_83\ : STD_LOGIC;
  signal \multi_1_s__0_n_84\ : STD_LOGIC;
  signal \multi_1_s__0_n_85\ : STD_LOGIC;
  signal \multi_1_s__0_n_86\ : STD_LOGIC;
  signal \multi_1_s__0_n_87\ : STD_LOGIC;
  signal \multi_1_s__0_n_88\ : STD_LOGIC;
  signal \multi_1_s__0_n_89\ : STD_LOGIC;
  signal \multi_1_s__0_n_90\ : STD_LOGIC;
  signal \multi_1_s__0_n_91\ : STD_LOGIC;
  signal \multi_1_s__0_n_92\ : STD_LOGIC;
  signal \multi_1_s__0_n_93\ : STD_LOGIC;
  signal \multi_1_s__0_n_94\ : STD_LOGIC;
  signal \multi_1_s__0_n_95\ : STD_LOGIC;
  signal \multi_1_s__0_n_96\ : STD_LOGIC;
  signal \multi_1_s__0_n_97\ : STD_LOGIC;
  signal \multi_1_s__0_n_98\ : STD_LOGIC;
  signal \multi_1_s__0_n_99\ : STD_LOGIC;
  signal multi_1_s_n_100 : STD_LOGIC;
  signal multi_1_s_n_101 : STD_LOGIC;
  signal multi_1_s_n_102 : STD_LOGIC;
  signal multi_1_s_n_103 : STD_LOGIC;
  signal multi_1_s_n_104 : STD_LOGIC;
  signal multi_1_s_n_105 : STD_LOGIC;
  signal multi_1_s_n_106 : STD_LOGIC;
  signal multi_1_s_n_107 : STD_LOGIC;
  signal multi_1_s_n_108 : STD_LOGIC;
  signal multi_1_s_n_109 : STD_LOGIC;
  signal multi_1_s_n_110 : STD_LOGIC;
  signal multi_1_s_n_111 : STD_LOGIC;
  signal multi_1_s_n_112 : STD_LOGIC;
  signal multi_1_s_n_113 : STD_LOGIC;
  signal multi_1_s_n_114 : STD_LOGIC;
  signal multi_1_s_n_115 : STD_LOGIC;
  signal multi_1_s_n_116 : STD_LOGIC;
  signal multi_1_s_n_117 : STD_LOGIC;
  signal multi_1_s_n_118 : STD_LOGIC;
  signal multi_1_s_n_119 : STD_LOGIC;
  signal multi_1_s_n_120 : STD_LOGIC;
  signal multi_1_s_n_121 : STD_LOGIC;
  signal multi_1_s_n_122 : STD_LOGIC;
  signal multi_1_s_n_123 : STD_LOGIC;
  signal multi_1_s_n_124 : STD_LOGIC;
  signal multi_1_s_n_125 : STD_LOGIC;
  signal multi_1_s_n_126 : STD_LOGIC;
  signal multi_1_s_n_127 : STD_LOGIC;
  signal multi_1_s_n_128 : STD_LOGIC;
  signal multi_1_s_n_129 : STD_LOGIC;
  signal multi_1_s_n_130 : STD_LOGIC;
  signal multi_1_s_n_131 : STD_LOGIC;
  signal multi_1_s_n_132 : STD_LOGIC;
  signal multi_1_s_n_133 : STD_LOGIC;
  signal multi_1_s_n_134 : STD_LOGIC;
  signal multi_1_s_n_135 : STD_LOGIC;
  signal multi_1_s_n_136 : STD_LOGIC;
  signal multi_1_s_n_137 : STD_LOGIC;
  signal multi_1_s_n_138 : STD_LOGIC;
  signal multi_1_s_n_139 : STD_LOGIC;
  signal multi_1_s_n_140 : STD_LOGIC;
  signal multi_1_s_n_141 : STD_LOGIC;
  signal multi_1_s_n_142 : STD_LOGIC;
  signal multi_1_s_n_143 : STD_LOGIC;
  signal multi_1_s_n_144 : STD_LOGIC;
  signal multi_1_s_n_145 : STD_LOGIC;
  signal multi_1_s_n_146 : STD_LOGIC;
  signal multi_1_s_n_147 : STD_LOGIC;
  signal multi_1_s_n_148 : STD_LOGIC;
  signal multi_1_s_n_149 : STD_LOGIC;
  signal multi_1_s_n_150 : STD_LOGIC;
  signal multi_1_s_n_151 : STD_LOGIC;
  signal multi_1_s_n_152 : STD_LOGIC;
  signal multi_1_s_n_153 : STD_LOGIC;
  signal multi_1_s_n_58 : STD_LOGIC;
  signal multi_1_s_n_59 : STD_LOGIC;
  signal multi_1_s_n_60 : STD_LOGIC;
  signal multi_1_s_n_61 : STD_LOGIC;
  signal multi_1_s_n_62 : STD_LOGIC;
  signal multi_1_s_n_63 : STD_LOGIC;
  signal multi_1_s_n_64 : STD_LOGIC;
  signal multi_1_s_n_65 : STD_LOGIC;
  signal multi_1_s_n_66 : STD_LOGIC;
  signal multi_1_s_n_67 : STD_LOGIC;
  signal multi_1_s_n_68 : STD_LOGIC;
  signal multi_1_s_n_69 : STD_LOGIC;
  signal multi_1_s_n_70 : STD_LOGIC;
  signal multi_1_s_n_71 : STD_LOGIC;
  signal multi_1_s_n_72 : STD_LOGIC;
  signal multi_1_s_n_73 : STD_LOGIC;
  signal multi_1_s_n_74 : STD_LOGIC;
  signal multi_1_s_n_75 : STD_LOGIC;
  signal multi_1_s_n_76 : STD_LOGIC;
  signal multi_1_s_n_77 : STD_LOGIC;
  signal multi_1_s_n_78 : STD_LOGIC;
  signal multi_1_s_n_79 : STD_LOGIC;
  signal multi_1_s_n_80 : STD_LOGIC;
  signal multi_1_s_n_81 : STD_LOGIC;
  signal multi_1_s_n_82 : STD_LOGIC;
  signal multi_1_s_n_83 : STD_LOGIC;
  signal multi_1_s_n_84 : STD_LOGIC;
  signal multi_1_s_n_85 : STD_LOGIC;
  signal multi_1_s_n_86 : STD_LOGIC;
  signal multi_1_s_n_87 : STD_LOGIC;
  signal multi_1_s_n_88 : STD_LOGIC;
  signal multi_1_s_n_89 : STD_LOGIC;
  signal multi_1_s_n_90 : STD_LOGIC;
  signal multi_1_s_n_91 : STD_LOGIC;
  signal multi_1_s_n_92 : STD_LOGIC;
  signal multi_1_s_n_93 : STD_LOGIC;
  signal multi_1_s_n_94 : STD_LOGIC;
  signal multi_1_s_n_95 : STD_LOGIC;
  signal multi_1_s_n_96 : STD_LOGIC;
  signal multi_1_s_n_97 : STD_LOGIC;
  signal multi_1_s_n_98 : STD_LOGIC;
  signal multi_1_s_n_99 : STD_LOGIC;
  signal \multi_2_s__0_n_100\ : STD_LOGIC;
  signal \multi_2_s__0_n_101\ : STD_LOGIC;
  signal \multi_2_s__0_n_102\ : STD_LOGIC;
  signal \multi_2_s__0_n_103\ : STD_LOGIC;
  signal \multi_2_s__0_n_104\ : STD_LOGIC;
  signal \multi_2_s__0_n_105\ : STD_LOGIC;
  signal \multi_2_s__0_n_79\ : STD_LOGIC;
  signal \multi_2_s__0_n_80\ : STD_LOGIC;
  signal \multi_2_s__0_n_81\ : STD_LOGIC;
  signal \multi_2_s__0_n_82\ : STD_LOGIC;
  signal \multi_2_s__0_n_83\ : STD_LOGIC;
  signal \multi_2_s__0_n_84\ : STD_LOGIC;
  signal \multi_2_s__0_n_85\ : STD_LOGIC;
  signal \multi_2_s__0_n_86\ : STD_LOGIC;
  signal \multi_2_s__0_n_87\ : STD_LOGIC;
  signal \multi_2_s__0_n_88\ : STD_LOGIC;
  signal \multi_2_s__0_n_89\ : STD_LOGIC;
  signal \multi_2_s__0_n_90\ : STD_LOGIC;
  signal \multi_2_s__0_n_91\ : STD_LOGIC;
  signal \multi_2_s__0_n_92\ : STD_LOGIC;
  signal \multi_2_s__0_n_93\ : STD_LOGIC;
  signal \multi_2_s__0_n_94\ : STD_LOGIC;
  signal \multi_2_s__0_n_95\ : STD_LOGIC;
  signal \multi_2_s__0_n_96\ : STD_LOGIC;
  signal \multi_2_s__0_n_97\ : STD_LOGIC;
  signal \multi_2_s__0_n_98\ : STD_LOGIC;
  signal \multi_2_s__0_n_99\ : STD_LOGIC;
  signal multi_2_s_n_100 : STD_LOGIC;
  signal multi_2_s_n_101 : STD_LOGIC;
  signal multi_2_s_n_102 : STD_LOGIC;
  signal multi_2_s_n_103 : STD_LOGIC;
  signal multi_2_s_n_104 : STD_LOGIC;
  signal multi_2_s_n_105 : STD_LOGIC;
  signal multi_2_s_n_106 : STD_LOGIC;
  signal multi_2_s_n_107 : STD_LOGIC;
  signal multi_2_s_n_108 : STD_LOGIC;
  signal multi_2_s_n_109 : STD_LOGIC;
  signal multi_2_s_n_110 : STD_LOGIC;
  signal multi_2_s_n_111 : STD_LOGIC;
  signal multi_2_s_n_112 : STD_LOGIC;
  signal multi_2_s_n_113 : STD_LOGIC;
  signal multi_2_s_n_114 : STD_LOGIC;
  signal multi_2_s_n_115 : STD_LOGIC;
  signal multi_2_s_n_116 : STD_LOGIC;
  signal multi_2_s_n_117 : STD_LOGIC;
  signal multi_2_s_n_118 : STD_LOGIC;
  signal multi_2_s_n_119 : STD_LOGIC;
  signal multi_2_s_n_120 : STD_LOGIC;
  signal multi_2_s_n_121 : STD_LOGIC;
  signal multi_2_s_n_122 : STD_LOGIC;
  signal multi_2_s_n_123 : STD_LOGIC;
  signal multi_2_s_n_124 : STD_LOGIC;
  signal multi_2_s_n_125 : STD_LOGIC;
  signal multi_2_s_n_126 : STD_LOGIC;
  signal multi_2_s_n_127 : STD_LOGIC;
  signal multi_2_s_n_128 : STD_LOGIC;
  signal multi_2_s_n_129 : STD_LOGIC;
  signal multi_2_s_n_130 : STD_LOGIC;
  signal multi_2_s_n_131 : STD_LOGIC;
  signal multi_2_s_n_132 : STD_LOGIC;
  signal multi_2_s_n_133 : STD_LOGIC;
  signal multi_2_s_n_134 : STD_LOGIC;
  signal multi_2_s_n_135 : STD_LOGIC;
  signal multi_2_s_n_136 : STD_LOGIC;
  signal multi_2_s_n_137 : STD_LOGIC;
  signal multi_2_s_n_138 : STD_LOGIC;
  signal multi_2_s_n_139 : STD_LOGIC;
  signal multi_2_s_n_140 : STD_LOGIC;
  signal multi_2_s_n_141 : STD_LOGIC;
  signal multi_2_s_n_142 : STD_LOGIC;
  signal multi_2_s_n_143 : STD_LOGIC;
  signal multi_2_s_n_144 : STD_LOGIC;
  signal multi_2_s_n_145 : STD_LOGIC;
  signal multi_2_s_n_146 : STD_LOGIC;
  signal multi_2_s_n_147 : STD_LOGIC;
  signal multi_2_s_n_148 : STD_LOGIC;
  signal multi_2_s_n_149 : STD_LOGIC;
  signal multi_2_s_n_150 : STD_LOGIC;
  signal multi_2_s_n_151 : STD_LOGIC;
  signal multi_2_s_n_152 : STD_LOGIC;
  signal multi_2_s_n_153 : STD_LOGIC;
  signal multi_2_s_n_58 : STD_LOGIC;
  signal multi_2_s_n_59 : STD_LOGIC;
  signal multi_2_s_n_60 : STD_LOGIC;
  signal multi_2_s_n_61 : STD_LOGIC;
  signal multi_2_s_n_62 : STD_LOGIC;
  signal multi_2_s_n_63 : STD_LOGIC;
  signal multi_2_s_n_64 : STD_LOGIC;
  signal multi_2_s_n_65 : STD_LOGIC;
  signal multi_2_s_n_66 : STD_LOGIC;
  signal multi_2_s_n_67 : STD_LOGIC;
  signal multi_2_s_n_68 : STD_LOGIC;
  signal multi_2_s_n_69 : STD_LOGIC;
  signal multi_2_s_n_70 : STD_LOGIC;
  signal multi_2_s_n_71 : STD_LOGIC;
  signal multi_2_s_n_72 : STD_LOGIC;
  signal multi_2_s_n_73 : STD_LOGIC;
  signal multi_2_s_n_74 : STD_LOGIC;
  signal multi_2_s_n_75 : STD_LOGIC;
  signal multi_2_s_n_76 : STD_LOGIC;
  signal multi_2_s_n_77 : STD_LOGIC;
  signal multi_2_s_n_78 : STD_LOGIC;
  signal multi_2_s_n_79 : STD_LOGIC;
  signal multi_2_s_n_80 : STD_LOGIC;
  signal multi_2_s_n_81 : STD_LOGIC;
  signal multi_2_s_n_82 : STD_LOGIC;
  signal multi_2_s_n_83 : STD_LOGIC;
  signal multi_2_s_n_84 : STD_LOGIC;
  signal multi_2_s_n_85 : STD_LOGIC;
  signal multi_2_s_n_86 : STD_LOGIC;
  signal multi_2_s_n_87 : STD_LOGIC;
  signal multi_2_s_n_88 : STD_LOGIC;
  signal multi_2_s_n_89 : STD_LOGIC;
  signal multi_2_s_n_90 : STD_LOGIC;
  signal multi_2_s_n_91 : STD_LOGIC;
  signal multi_2_s_n_92 : STD_LOGIC;
  signal multi_2_s_n_93 : STD_LOGIC;
  signal multi_2_s_n_94 : STD_LOGIC;
  signal multi_2_s_n_95 : STD_LOGIC;
  signal multi_2_s_n_96 : STD_LOGIC;
  signal multi_2_s_n_97 : STD_LOGIC;
  signal multi_2_s_n_98 : STD_LOGIC;
  signal multi_2_s_n_99 : STD_LOGIC;
  signal \multi_3_s__0_n_100\ : STD_LOGIC;
  signal \multi_3_s__0_n_101\ : STD_LOGIC;
  signal \multi_3_s__0_n_102\ : STD_LOGIC;
  signal \multi_3_s__0_n_103\ : STD_LOGIC;
  signal \multi_3_s__0_n_104\ : STD_LOGIC;
  signal \multi_3_s__0_n_105\ : STD_LOGIC;
  signal \multi_3_s__0_n_79\ : STD_LOGIC;
  signal \multi_3_s__0_n_80\ : STD_LOGIC;
  signal \multi_3_s__0_n_81\ : STD_LOGIC;
  signal \multi_3_s__0_n_82\ : STD_LOGIC;
  signal \multi_3_s__0_n_83\ : STD_LOGIC;
  signal \multi_3_s__0_n_84\ : STD_LOGIC;
  signal \multi_3_s__0_n_85\ : STD_LOGIC;
  signal \multi_3_s__0_n_86\ : STD_LOGIC;
  signal \multi_3_s__0_n_87\ : STD_LOGIC;
  signal \multi_3_s__0_n_88\ : STD_LOGIC;
  signal \multi_3_s__0_n_89\ : STD_LOGIC;
  signal \multi_3_s__0_n_90\ : STD_LOGIC;
  signal \multi_3_s__0_n_91\ : STD_LOGIC;
  signal \multi_3_s__0_n_92\ : STD_LOGIC;
  signal \multi_3_s__0_n_93\ : STD_LOGIC;
  signal \multi_3_s__0_n_94\ : STD_LOGIC;
  signal \multi_3_s__0_n_95\ : STD_LOGIC;
  signal \multi_3_s__0_n_96\ : STD_LOGIC;
  signal \multi_3_s__0_n_97\ : STD_LOGIC;
  signal \multi_3_s__0_n_98\ : STD_LOGIC;
  signal \multi_3_s__0_n_99\ : STD_LOGIC;
  signal multi_3_s_n_100 : STD_LOGIC;
  signal multi_3_s_n_101 : STD_LOGIC;
  signal multi_3_s_n_102 : STD_LOGIC;
  signal multi_3_s_n_103 : STD_LOGIC;
  signal multi_3_s_n_104 : STD_LOGIC;
  signal multi_3_s_n_105 : STD_LOGIC;
  signal multi_3_s_n_106 : STD_LOGIC;
  signal multi_3_s_n_107 : STD_LOGIC;
  signal multi_3_s_n_108 : STD_LOGIC;
  signal multi_3_s_n_109 : STD_LOGIC;
  signal multi_3_s_n_110 : STD_LOGIC;
  signal multi_3_s_n_111 : STD_LOGIC;
  signal multi_3_s_n_112 : STD_LOGIC;
  signal multi_3_s_n_113 : STD_LOGIC;
  signal multi_3_s_n_114 : STD_LOGIC;
  signal multi_3_s_n_115 : STD_LOGIC;
  signal multi_3_s_n_116 : STD_LOGIC;
  signal multi_3_s_n_117 : STD_LOGIC;
  signal multi_3_s_n_118 : STD_LOGIC;
  signal multi_3_s_n_119 : STD_LOGIC;
  signal multi_3_s_n_120 : STD_LOGIC;
  signal multi_3_s_n_121 : STD_LOGIC;
  signal multi_3_s_n_122 : STD_LOGIC;
  signal multi_3_s_n_123 : STD_LOGIC;
  signal multi_3_s_n_124 : STD_LOGIC;
  signal multi_3_s_n_125 : STD_LOGIC;
  signal multi_3_s_n_126 : STD_LOGIC;
  signal multi_3_s_n_127 : STD_LOGIC;
  signal multi_3_s_n_128 : STD_LOGIC;
  signal multi_3_s_n_129 : STD_LOGIC;
  signal multi_3_s_n_130 : STD_LOGIC;
  signal multi_3_s_n_131 : STD_LOGIC;
  signal multi_3_s_n_132 : STD_LOGIC;
  signal multi_3_s_n_133 : STD_LOGIC;
  signal multi_3_s_n_134 : STD_LOGIC;
  signal multi_3_s_n_135 : STD_LOGIC;
  signal multi_3_s_n_136 : STD_LOGIC;
  signal multi_3_s_n_137 : STD_LOGIC;
  signal multi_3_s_n_138 : STD_LOGIC;
  signal multi_3_s_n_139 : STD_LOGIC;
  signal multi_3_s_n_140 : STD_LOGIC;
  signal multi_3_s_n_141 : STD_LOGIC;
  signal multi_3_s_n_142 : STD_LOGIC;
  signal multi_3_s_n_143 : STD_LOGIC;
  signal multi_3_s_n_144 : STD_LOGIC;
  signal multi_3_s_n_145 : STD_LOGIC;
  signal multi_3_s_n_146 : STD_LOGIC;
  signal multi_3_s_n_147 : STD_LOGIC;
  signal multi_3_s_n_148 : STD_LOGIC;
  signal multi_3_s_n_149 : STD_LOGIC;
  signal multi_3_s_n_150 : STD_LOGIC;
  signal multi_3_s_n_151 : STD_LOGIC;
  signal multi_3_s_n_152 : STD_LOGIC;
  signal multi_3_s_n_153 : STD_LOGIC;
  signal multi_3_s_n_58 : STD_LOGIC;
  signal multi_3_s_n_59 : STD_LOGIC;
  signal multi_3_s_n_60 : STD_LOGIC;
  signal multi_3_s_n_61 : STD_LOGIC;
  signal multi_3_s_n_62 : STD_LOGIC;
  signal multi_3_s_n_63 : STD_LOGIC;
  signal multi_3_s_n_64 : STD_LOGIC;
  signal multi_3_s_n_65 : STD_LOGIC;
  signal multi_3_s_n_66 : STD_LOGIC;
  signal multi_3_s_n_67 : STD_LOGIC;
  signal multi_3_s_n_68 : STD_LOGIC;
  signal multi_3_s_n_69 : STD_LOGIC;
  signal multi_3_s_n_70 : STD_LOGIC;
  signal multi_3_s_n_71 : STD_LOGIC;
  signal multi_3_s_n_72 : STD_LOGIC;
  signal multi_3_s_n_73 : STD_LOGIC;
  signal multi_3_s_n_74 : STD_LOGIC;
  signal multi_3_s_n_75 : STD_LOGIC;
  signal multi_3_s_n_76 : STD_LOGIC;
  signal multi_3_s_n_77 : STD_LOGIC;
  signal multi_3_s_n_78 : STD_LOGIC;
  signal multi_3_s_n_79 : STD_LOGIC;
  signal multi_3_s_n_80 : STD_LOGIC;
  signal multi_3_s_n_81 : STD_LOGIC;
  signal multi_3_s_n_82 : STD_LOGIC;
  signal multi_3_s_n_83 : STD_LOGIC;
  signal multi_3_s_n_84 : STD_LOGIC;
  signal multi_3_s_n_85 : STD_LOGIC;
  signal multi_3_s_n_86 : STD_LOGIC;
  signal multi_3_s_n_87 : STD_LOGIC;
  signal multi_3_s_n_88 : STD_LOGIC;
  signal multi_3_s_n_89 : STD_LOGIC;
  signal multi_3_s_n_90 : STD_LOGIC;
  signal multi_3_s_n_91 : STD_LOGIC;
  signal multi_3_s_n_92 : STD_LOGIC;
  signal multi_3_s_n_93 : STD_LOGIC;
  signal multi_3_s_n_94 : STD_LOGIC;
  signal multi_3_s_n_95 : STD_LOGIC;
  signal multi_3_s_n_96 : STD_LOGIC;
  signal multi_3_s_n_97 : STD_LOGIC;
  signal multi_3_s_n_98 : STD_LOGIC;
  signal multi_3_s_n_99 : STD_LOGIC;
  signal q_reg : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal second_reg_n_0 : STD_LOGIC;
  signal \NLW_adder_1_s__0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_multi_1_s_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_1_s_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_multi_1_s_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_multi_1_s_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_1_s__0_CARRYCASCOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_MULTSIGNOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_OVERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_PATTERNBDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_PATTERNDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_UNDERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_1_s__0_ACOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal \NLW_multi_1_s__0_BCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \NLW_multi_1_s__0_CARRYOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_1_s__0_P_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 27 );
  signal \NLW_multi_1_s__0_PCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_multi_2_s_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_2_s_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_multi_2_s_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_multi_2_s_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_2_s__0_CARRYCASCOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_MULTSIGNOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_OVERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_PATTERNBDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_PATTERNDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_UNDERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_2_s__0_ACOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal \NLW_multi_2_s__0_BCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \NLW_multi_2_s__0_CARRYOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_2_s__0_P_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 27 );
  signal \NLW_multi_2_s__0_PCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_multi_3_s_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_multi_3_s_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_multi_3_s_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_multi_3_s_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_3_s__0_CARRYCASCOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_MULTSIGNOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_OVERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_PATTERNBDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_PATTERNDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_UNDERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_multi_3_s__0_ACOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal \NLW_multi_3_s__0_BCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \NLW_multi_3_s__0_CARRYOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_multi_3_s__0_P_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 27 );
  signal \NLW_multi_3_s__0_PCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute HLUTNM : string;
  attribute HLUTNM of \adder_1_s__0_carry__0_i_1\ : label is "lutpair5";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_2\ : label is "lutpair4";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_3\ : label is "lutpair3";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_4\ : label is "lutpair2";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_5\ : label is "lutpair6";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_6\ : label is "lutpair5";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_7\ : label is "lutpair4";
  attribute HLUTNM of \adder_1_s__0_carry__0_i_8\ : label is "lutpair3";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_1\ : label is "lutpair9";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_2\ : label is "lutpair8";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_3\ : label is "lutpair7";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_4\ : label is "lutpair6";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_5\ : label is "lutpair10";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_6\ : label is "lutpair9";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_7\ : label is "lutpair8";
  attribute HLUTNM of \adder_1_s__0_carry__1_i_8\ : label is "lutpair7";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_1\ : label is "lutpair13";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_2\ : label is "lutpair12";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_3\ : label is "lutpair11";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_4\ : label is "lutpair10";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_5\ : label is "lutpair14";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_6\ : label is "lutpair13";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_7\ : label is "lutpair12";
  attribute HLUTNM of \adder_1_s__0_carry__2_i_8\ : label is "lutpair11";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_1\ : label is "lutpair17";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_2\ : label is "lutpair16";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_3\ : label is "lutpair15";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_4\ : label is "lutpair14";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_5\ : label is "lutpair18";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_6\ : label is "lutpair17";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_7\ : label is "lutpair16";
  attribute HLUTNM of \adder_1_s__0_carry__3_i_8\ : label is "lutpair15";
  attribute HLUTNM of \adder_1_s__0_carry__4_i_1\ : label is "lutpair20";
  attribute HLUTNM of \adder_1_s__0_carry__4_i_2\ : label is "lutpair19";
  attribute HLUTNM of \adder_1_s__0_carry__4_i_3\ : label is "lutpair18";
  attribute HLUTNM of \adder_1_s__0_carry__4_i_6\ : label is "lutpair20";
  attribute HLUTNM of \adder_1_s__0_carry__4_i_7\ : label is "lutpair19";
  attribute HLUTNM of \adder_1_s__0_carry_i_1\ : label is "lutpair1";
  attribute HLUTNM of \adder_1_s__0_carry_i_2\ : label is "lutpair0";
  attribute HLUTNM of \adder_1_s__0_carry_i_3\ : label is "lutpair21";
  attribute HLUTNM of \adder_1_s__0_carry_i_4\ : label is "lutpair2";
  attribute HLUTNM of \adder_1_s__0_carry_i_5\ : label is "lutpair1";
  attribute HLUTNM of \adder_1_s__0_carry_i_6\ : label is "lutpair0";
  attribute HLUTNM of \adder_1_s__0_carry_i_7\ : label is "lutpair21";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of multi_1_s : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \multi_1_s__0\ : label is "{SYNTH-11 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of multi_2_s : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \multi_2_s__0\ : label is "{SYNTH-11 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of multi_3_s : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \multi_3_s__0\ : label is "{SYNTH-11 {cell *THIS*}}";
begin
\adder_1_s__0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \adder_1_s__0_carry_n_0\,
      CO(2) => \adder_1_s__0_carry_n_1\,
      CO(1) => \adder_1_s__0_carry_n_2\,
      CO(0) => \adder_1_s__0_carry_n_3\,
      CYINIT => '1',
      DI(3) => \adder_1_s__0_carry_i_1_n_0\,
      DI(2) => \adder_1_s__0_carry_i_2_n_0\,
      DI(1) => \adder_1_s__0_carry_i_3_n_0\,
      DI(0) => '1',
      O(3 downto 0) => A(3 downto 0),
      S(3) => \adder_1_s__0_carry_i_4_n_0\,
      S(2) => \adder_1_s__0_carry_i_5_n_0\,
      S(1) => \adder_1_s__0_carry_i_6_n_0\,
      S(0) => \adder_1_s__0_carry_i_7_n_0\
    );
\adder_1_s__0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry_n_0\,
      CO(3) => \adder_1_s__0_carry__0_n_0\,
      CO(2) => \adder_1_s__0_carry__0_n_1\,
      CO(1) => \adder_1_s__0_carry__0_n_2\,
      CO(0) => \adder_1_s__0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \adder_1_s__0_carry__0_i_1_n_0\,
      DI(2) => \adder_1_s__0_carry__0_i_2_n_0\,
      DI(1) => \adder_1_s__0_carry__0_i_3_n_0\,
      DI(0) => \adder_1_s__0_carry__0_i_4_n_0\,
      O(3 downto 0) => A(7 downto 4),
      S(3) => \adder_1_s__0_carry__0_i_5_n_0\,
      S(2) => \adder_1_s__0_carry__0_i_6_n_0\,
      S(1) => \adder_1_s__0_carry__0_i_7_n_0\,
      S(0) => \adder_1_s__0_carry__0_i_8_n_0\
    );
\adder_1_s__0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_96\,
      I1 => adder_3_s(6),
      I2 => Q(6),
      O => \adder_1_s__0_carry__0_i_1_n_0\
    );
\adder_1_s__0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_97\,
      I1 => adder_3_s(5),
      I2 => Q(5),
      O => \adder_1_s__0_carry__0_i_2_n_0\
    );
\adder_1_s__0_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_98\,
      I1 => adder_3_s(4),
      I2 => Q(4),
      O => \adder_1_s__0_carry__0_i_3_n_0\
    );
\adder_1_s__0_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_99\,
      I1 => adder_3_s(3),
      I2 => Q(3),
      O => \adder_1_s__0_carry__0_i_4_n_0\
    );
\adder_1_s__0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_95\,
      I1 => adder_3_s(7),
      I2 => Q(7),
      I3 => \adder_1_s__0_carry__0_i_1_n_0\,
      O => \adder_1_s__0_carry__0_i_5_n_0\
    );
\adder_1_s__0_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_96\,
      I1 => adder_3_s(6),
      I2 => Q(6),
      I3 => \adder_1_s__0_carry__0_i_2_n_0\,
      O => \adder_1_s__0_carry__0_i_6_n_0\
    );
\adder_1_s__0_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_97\,
      I1 => adder_3_s(5),
      I2 => Q(5),
      I3 => \adder_1_s__0_carry__0_i_3_n_0\,
      O => \adder_1_s__0_carry__0_i_7_n_0\
    );
\adder_1_s__0_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_98\,
      I1 => adder_3_s(4),
      I2 => Q(4),
      I3 => \adder_1_s__0_carry__0_i_4_n_0\,
      O => \adder_1_s__0_carry__0_i_8_n_0\
    );
\adder_1_s__0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__0_n_0\,
      CO(3) => \adder_1_s__0_carry__1_n_0\,
      CO(2) => \adder_1_s__0_carry__1_n_1\,
      CO(1) => \adder_1_s__0_carry__1_n_2\,
      CO(0) => \adder_1_s__0_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \adder_1_s__0_carry__1_i_1_n_0\,
      DI(2) => \adder_1_s__0_carry__1_i_2_n_0\,
      DI(1) => \adder_1_s__0_carry__1_i_3_n_0\,
      DI(0) => \adder_1_s__0_carry__1_i_4_n_0\,
      O(3 downto 0) => A(11 downto 8),
      S(3) => \adder_1_s__0_carry__1_i_5_n_0\,
      S(2) => \adder_1_s__0_carry__1_i_6_n_0\,
      S(1) => \adder_1_s__0_carry__1_i_7_n_0\,
      S(0) => \adder_1_s__0_carry__1_i_8_n_0\
    );
\adder_1_s__0_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_92\,
      I1 => adder_3_s(10),
      I2 => Q(10),
      O => \adder_1_s__0_carry__1_i_1_n_0\
    );
\adder_1_s__0_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_93\,
      I1 => adder_3_s(9),
      I2 => Q(9),
      O => \adder_1_s__0_carry__1_i_2_n_0\
    );
\adder_1_s__0_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_94\,
      I1 => adder_3_s(8),
      I2 => Q(8),
      O => \adder_1_s__0_carry__1_i_3_n_0\
    );
\adder_1_s__0_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_95\,
      I1 => adder_3_s(7),
      I2 => Q(7),
      O => \adder_1_s__0_carry__1_i_4_n_0\
    );
\adder_1_s__0_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_91\,
      I1 => adder_3_s(11),
      I2 => Q(11),
      I3 => \adder_1_s__0_carry__1_i_1_n_0\,
      O => \adder_1_s__0_carry__1_i_5_n_0\
    );
\adder_1_s__0_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_92\,
      I1 => adder_3_s(10),
      I2 => Q(10),
      I3 => \adder_1_s__0_carry__1_i_2_n_0\,
      O => \adder_1_s__0_carry__1_i_6_n_0\
    );
\adder_1_s__0_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_93\,
      I1 => adder_3_s(9),
      I2 => Q(9),
      I3 => \adder_1_s__0_carry__1_i_3_n_0\,
      O => \adder_1_s__0_carry__1_i_7_n_0\
    );
\adder_1_s__0_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_94\,
      I1 => adder_3_s(8),
      I2 => Q(8),
      I3 => \adder_1_s__0_carry__1_i_4_n_0\,
      O => \adder_1_s__0_carry__1_i_8_n_0\
    );
\adder_1_s__0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__1_n_0\,
      CO(3) => \adder_1_s__0_carry__2_n_0\,
      CO(2) => \adder_1_s__0_carry__2_n_1\,
      CO(1) => \adder_1_s__0_carry__2_n_2\,
      CO(0) => \adder_1_s__0_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \adder_1_s__0_carry__2_i_1_n_0\,
      DI(2) => \adder_1_s__0_carry__2_i_2_n_0\,
      DI(1) => \adder_1_s__0_carry__2_i_3_n_0\,
      DI(0) => \adder_1_s__0_carry__2_i_4_n_0\,
      O(3 downto 0) => A(15 downto 12),
      S(3) => \adder_1_s__0_carry__2_i_5_n_0\,
      S(2) => \adder_1_s__0_carry__2_i_6_n_0\,
      S(1) => \adder_1_s__0_carry__2_i_7_n_0\,
      S(0) => \adder_1_s__0_carry__2_i_8_n_0\
    );
\adder_1_s__0_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_88\,
      I1 => adder_3_s(14),
      I2 => Q(14),
      O => \adder_1_s__0_carry__2_i_1_n_0\
    );
\adder_1_s__0_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_89\,
      I1 => adder_3_s(13),
      I2 => Q(13),
      O => \adder_1_s__0_carry__2_i_2_n_0\
    );
\adder_1_s__0_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_90\,
      I1 => adder_3_s(12),
      I2 => Q(12),
      O => \adder_1_s__0_carry__2_i_3_n_0\
    );
\adder_1_s__0_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_91\,
      I1 => adder_3_s(11),
      I2 => Q(11),
      O => \adder_1_s__0_carry__2_i_4_n_0\
    );
\adder_1_s__0_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_87\,
      I1 => adder_3_s(15),
      I2 => Q(15),
      I3 => \adder_1_s__0_carry__2_i_1_n_0\,
      O => \adder_1_s__0_carry__2_i_5_n_0\
    );
\adder_1_s__0_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_88\,
      I1 => adder_3_s(14),
      I2 => Q(14),
      I3 => \adder_1_s__0_carry__2_i_2_n_0\,
      O => \adder_1_s__0_carry__2_i_6_n_0\
    );
\adder_1_s__0_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_89\,
      I1 => adder_3_s(13),
      I2 => Q(13),
      I3 => \adder_1_s__0_carry__2_i_3_n_0\,
      O => \adder_1_s__0_carry__2_i_7_n_0\
    );
\adder_1_s__0_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_90\,
      I1 => adder_3_s(12),
      I2 => Q(12),
      I3 => \adder_1_s__0_carry__2_i_4_n_0\,
      O => \adder_1_s__0_carry__2_i_8_n_0\
    );
\adder_1_s__0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__2_n_0\,
      CO(3) => \adder_1_s__0_carry__3_n_0\,
      CO(2) => \adder_1_s__0_carry__3_n_1\,
      CO(1) => \adder_1_s__0_carry__3_n_2\,
      CO(0) => \adder_1_s__0_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \adder_1_s__0_carry__3_i_1_n_0\,
      DI(2) => \adder_1_s__0_carry__3_i_2_n_0\,
      DI(1) => \adder_1_s__0_carry__3_i_3_n_0\,
      DI(0) => \adder_1_s__0_carry__3_i_4_n_0\,
      O(3 downto 0) => A(19 downto 16),
      S(3) => \adder_1_s__0_carry__3_i_5_n_0\,
      S(2) => \adder_1_s__0_carry__3_i_6_n_0\,
      S(1) => \adder_1_s__0_carry__3_i_7_n_0\,
      S(0) => \adder_1_s__0_carry__3_i_8_n_0\
    );
\adder_1_s__0_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_84\,
      I1 => adder_3_s(18),
      I2 => Q(18),
      O => \adder_1_s__0_carry__3_i_1_n_0\
    );
\adder_1_s__0_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_85\,
      I1 => adder_3_s(17),
      I2 => Q(17),
      O => \adder_1_s__0_carry__3_i_2_n_0\
    );
\adder_1_s__0_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_86\,
      I1 => adder_3_s(16),
      I2 => Q(16),
      O => \adder_1_s__0_carry__3_i_3_n_0\
    );
\adder_1_s__0_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_87\,
      I1 => adder_3_s(15),
      I2 => Q(15),
      O => \adder_1_s__0_carry__3_i_4_n_0\
    );
\adder_1_s__0_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_83\,
      I1 => adder_3_s(19),
      I2 => Q(19),
      I3 => \adder_1_s__0_carry__3_i_1_n_0\,
      O => \adder_1_s__0_carry__3_i_5_n_0\
    );
\adder_1_s__0_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_84\,
      I1 => adder_3_s(18),
      I2 => Q(18),
      I3 => \adder_1_s__0_carry__3_i_2_n_0\,
      O => \adder_1_s__0_carry__3_i_6_n_0\
    );
\adder_1_s__0_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_85\,
      I1 => adder_3_s(17),
      I2 => Q(17),
      I3 => \adder_1_s__0_carry__3_i_3_n_0\,
      O => \adder_1_s__0_carry__3_i_7_n_0\
    );
\adder_1_s__0_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_86\,
      I1 => adder_3_s(16),
      I2 => Q(16),
      I3 => \adder_1_s__0_carry__3_i_4_n_0\,
      O => \adder_1_s__0_carry__3_i_8_n_0\
    );
\adder_1_s__0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \adder_1_s__0_carry__3_n_0\,
      CO(3) => \NLW_adder_1_s__0_carry__4_CO_UNCONNECTED\(3),
      CO(2) => \adder_1_s__0_carry__4_n_1\,
      CO(1) => \adder_1_s__0_carry__4_n_2\,
      CO(0) => \adder_1_s__0_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \adder_1_s__0_carry__4_i_1_n_0\,
      DI(1) => \adder_1_s__0_carry__4_i_2_n_0\,
      DI(0) => \adder_1_s__0_carry__4_i_3_n_0\,
      O(3 downto 0) => A(23 downto 20),
      S(3) => second_reg_n_0,
      S(2) => \adder_1_s__0_carry__4_i_5_n_0\,
      S(1) => \adder_1_s__0_carry__4_i_6_n_0\,
      S(0) => \adder_1_s__0_carry__4_i_7_n_0\
    );
\adder_1_s__0_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_81\,
      I1 => adder_3_s(21),
      I2 => Q(21),
      O => \adder_1_s__0_carry__4_i_1_n_0\
    );
\adder_1_s__0_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_82\,
      I1 => adder_3_s(20),
      I2 => Q(20),
      O => \adder_1_s__0_carry__4_i_2_n_0\
    );
\adder_1_s__0_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_83\,
      I1 => adder_3_s(19),
      I2 => Q(19),
      O => \adder_1_s__0_carry__4_i_3_n_0\
    );
\adder_1_s__0_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \adder_1_s__0_carry__4_i_1_n_0\,
      I1 => adder_3_s(22),
      I2 => \multi_3_s__0_n_80\,
      I3 => Q(22),
      O => \adder_1_s__0_carry__4_i_5_n_0\
    );
\adder_1_s__0_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_81\,
      I1 => adder_3_s(21),
      I2 => Q(21),
      I3 => \adder_1_s__0_carry__4_i_2_n_0\,
      O => \adder_1_s__0_carry__4_i_6_n_0\
    );
\adder_1_s__0_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_82\,
      I1 => adder_3_s(20),
      I2 => Q(20),
      I3 => \adder_1_s__0_carry__4_i_3_n_0\,
      O => \adder_1_s__0_carry__4_i_7_n_0\
    );
\adder_1_s__0_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_100\,
      I1 => adder_3_s(2),
      I2 => Q(2),
      O => \adder_1_s__0_carry_i_1_n_0\
    );
\adder_1_s__0_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_101\,
      I1 => adder_3_s(1),
      I2 => Q(1),
      O => \adder_1_s__0_carry_i_2_n_0\
    );
\adder_1_s__0_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \multi_3_s__0_n_102\,
      I1 => adder_3_s(0),
      I2 => Q(0),
      O => \adder_1_s__0_carry_i_3_n_0\
    );
\adder_1_s__0_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_99\,
      I1 => adder_3_s(3),
      I2 => Q(3),
      I3 => \adder_1_s__0_carry_i_1_n_0\,
      O => \adder_1_s__0_carry_i_4_n_0\
    );
\adder_1_s__0_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_100\,
      I1 => adder_3_s(2),
      I2 => Q(2),
      I3 => \adder_1_s__0_carry_i_2_n_0\,
      O => \adder_1_s__0_carry_i_5_n_0\
    );
\adder_1_s__0_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \multi_3_s__0_n_101\,
      I1 => adder_3_s(1),
      I2 => Q(1),
      I3 => \adder_1_s__0_carry_i_3_n_0\,
      O => \adder_1_s__0_carry_i_6_n_0\
    );
\adder_1_s__0_carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \multi_3_s__0_n_102\,
      I1 => adder_3_s(0),
      I2 => Q(0),
      O => \adder_1_s__0_carry_i_7_n_0\
    );
first_reg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg
     port map (
      D(23 downto 0) => D(23 downto 0),
      P(23) => \multi_1_s__0_n_79\,
      P(22) => \multi_1_s__0_n_80\,
      P(21) => \multi_1_s__0_n_81\,
      P(20) => \multi_1_s__0_n_82\,
      P(19) => \multi_1_s__0_n_83\,
      P(18) => \multi_1_s__0_n_84\,
      P(17) => \multi_1_s__0_n_85\,
      P(16) => \multi_1_s__0_n_86\,
      P(15) => \multi_1_s__0_n_87\,
      P(14) => \multi_1_s__0_n_88\,
      P(13) => \multi_1_s__0_n_89\,
      P(12) => \multi_1_s__0_n_90\,
      P(11) => \multi_1_s__0_n_91\,
      P(10) => \multi_1_s__0_n_92\,
      P(9) => \multi_1_s__0_n_93\,
      P(8) => \multi_1_s__0_n_94\,
      P(7) => \multi_1_s__0_n_95\,
      P(6) => \multi_1_s__0_n_96\,
      P(5) => \multi_1_s__0_n_97\,
      P(4) => \multi_1_s__0_n_98\,
      P(3) => \multi_1_s__0_n_99\,
      P(2) => \multi_1_s__0_n_100\,
      P(1) => \multi_1_s__0_n_101\,
      P(0) => \multi_1_s__0_n_102\,
      \out\(23 downto 0) => q_reg(23 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn
    );
multi_1_s: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(23),
      A(28) => A(23),
      A(27) => A(23),
      A(26) => A(23),
      A(25) => A(23),
      A(24) => A(23),
      A(23 downto 0) => A(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_multi_1_s_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => multi_2_s_0(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_multi_1_s_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_multi_1_s_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_multi_1_s_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_multi_1_s_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_multi_1_s_OVERFLOW_UNCONNECTED,
      P(47) => multi_1_s_n_58,
      P(46) => multi_1_s_n_59,
      P(45) => multi_1_s_n_60,
      P(44) => multi_1_s_n_61,
      P(43) => multi_1_s_n_62,
      P(42) => multi_1_s_n_63,
      P(41) => multi_1_s_n_64,
      P(40) => multi_1_s_n_65,
      P(39) => multi_1_s_n_66,
      P(38) => multi_1_s_n_67,
      P(37) => multi_1_s_n_68,
      P(36) => multi_1_s_n_69,
      P(35) => multi_1_s_n_70,
      P(34) => multi_1_s_n_71,
      P(33) => multi_1_s_n_72,
      P(32) => multi_1_s_n_73,
      P(31) => multi_1_s_n_74,
      P(30) => multi_1_s_n_75,
      P(29) => multi_1_s_n_76,
      P(28) => multi_1_s_n_77,
      P(27) => multi_1_s_n_78,
      P(26) => multi_1_s_n_79,
      P(25) => multi_1_s_n_80,
      P(24) => multi_1_s_n_81,
      P(23) => multi_1_s_n_82,
      P(22) => multi_1_s_n_83,
      P(21) => multi_1_s_n_84,
      P(20) => multi_1_s_n_85,
      P(19) => multi_1_s_n_86,
      P(18) => multi_1_s_n_87,
      P(17) => multi_1_s_n_88,
      P(16) => multi_1_s_n_89,
      P(15) => multi_1_s_n_90,
      P(14) => multi_1_s_n_91,
      P(13) => multi_1_s_n_92,
      P(12) => multi_1_s_n_93,
      P(11) => multi_1_s_n_94,
      P(10) => multi_1_s_n_95,
      P(9) => multi_1_s_n_96,
      P(8) => multi_1_s_n_97,
      P(7) => multi_1_s_n_98,
      P(6) => multi_1_s_n_99,
      P(5) => multi_1_s_n_100,
      P(4) => multi_1_s_n_101,
      P(3) => multi_1_s_n_102,
      P(2) => multi_1_s_n_103,
      P(1) => multi_1_s_n_104,
      P(0) => multi_1_s_n_105,
      PATTERNBDETECT => NLW_multi_1_s_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_multi_1_s_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => multi_1_s_n_106,
      PCOUT(46) => multi_1_s_n_107,
      PCOUT(45) => multi_1_s_n_108,
      PCOUT(44) => multi_1_s_n_109,
      PCOUT(43) => multi_1_s_n_110,
      PCOUT(42) => multi_1_s_n_111,
      PCOUT(41) => multi_1_s_n_112,
      PCOUT(40) => multi_1_s_n_113,
      PCOUT(39) => multi_1_s_n_114,
      PCOUT(38) => multi_1_s_n_115,
      PCOUT(37) => multi_1_s_n_116,
      PCOUT(36) => multi_1_s_n_117,
      PCOUT(35) => multi_1_s_n_118,
      PCOUT(34) => multi_1_s_n_119,
      PCOUT(33) => multi_1_s_n_120,
      PCOUT(32) => multi_1_s_n_121,
      PCOUT(31) => multi_1_s_n_122,
      PCOUT(30) => multi_1_s_n_123,
      PCOUT(29) => multi_1_s_n_124,
      PCOUT(28) => multi_1_s_n_125,
      PCOUT(27) => multi_1_s_n_126,
      PCOUT(26) => multi_1_s_n_127,
      PCOUT(25) => multi_1_s_n_128,
      PCOUT(24) => multi_1_s_n_129,
      PCOUT(23) => multi_1_s_n_130,
      PCOUT(22) => multi_1_s_n_131,
      PCOUT(21) => multi_1_s_n_132,
      PCOUT(20) => multi_1_s_n_133,
      PCOUT(19) => multi_1_s_n_134,
      PCOUT(18) => multi_1_s_n_135,
      PCOUT(17) => multi_1_s_n_136,
      PCOUT(16) => multi_1_s_n_137,
      PCOUT(15) => multi_1_s_n_138,
      PCOUT(14) => multi_1_s_n_139,
      PCOUT(13) => multi_1_s_n_140,
      PCOUT(12) => multi_1_s_n_141,
      PCOUT(11) => multi_1_s_n_142,
      PCOUT(10) => multi_1_s_n_143,
      PCOUT(9) => multi_1_s_n_144,
      PCOUT(8) => multi_1_s_n_145,
      PCOUT(7) => multi_1_s_n_146,
      PCOUT(6) => multi_1_s_n_147,
      PCOUT(5) => multi_1_s_n_148,
      PCOUT(4) => multi_1_s_n_149,
      PCOUT(3) => multi_1_s_n_150,
      PCOUT(2) => multi_1_s_n_151,
      PCOUT(1) => multi_1_s_n_152,
      PCOUT(0) => multi_1_s_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_multi_1_s_UNDERFLOW_UNCONNECTED
    );
\multi_1_s__0\: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(23),
      A(28) => A(23),
      A(27) => A(23),
      A(26) => A(23),
      A(25) => A(23),
      A(24) => A(23),
      A(23 downto 0) => A(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => \NLW_multi_1_s__0_ACOUT_UNCONNECTED\(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => s00_axi_wdata(6),
      B(16) => s00_axi_wdata(6),
      B(15) => s00_axi_wdata(6),
      B(14) => s00_axi_wdata(6),
      B(13) => s00_axi_wdata(6),
      B(12) => s00_axi_wdata(6),
      B(11) => s00_axi_wdata(6),
      B(10) => s00_axi_wdata(6),
      B(9) => s00_axi_wdata(6),
      B(8) => s00_axi_wdata(6),
      B(7) => s00_axi_wdata(6),
      B(6 downto 0) => s00_axi_wdata(6 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => \NLW_multi_1_s__0_BCOUT_UNCONNECTED\(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => \NLW_multi_1_s__0_CARRYCASCOUT_UNCONNECTED\,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => \NLW_multi_1_s__0_CARRYOUT_UNCONNECTED\(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => \multi_1_s__0_0\(0),
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => \NLW_multi_1_s__0_MULTSIGNOUT_UNCONNECTED\,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => \NLW_multi_1_s__0_OVERFLOW_UNCONNECTED\,
      P(47 downto 27) => \NLW_multi_1_s__0_P_UNCONNECTED\(47 downto 27),
      P(26) => \multi_1_s__0_n_79\,
      P(25) => \multi_1_s__0_n_80\,
      P(24) => \multi_1_s__0_n_81\,
      P(23) => \multi_1_s__0_n_82\,
      P(22) => \multi_1_s__0_n_83\,
      P(21) => \multi_1_s__0_n_84\,
      P(20) => \multi_1_s__0_n_85\,
      P(19) => \multi_1_s__0_n_86\,
      P(18) => \multi_1_s__0_n_87\,
      P(17) => \multi_1_s__0_n_88\,
      P(16) => \multi_1_s__0_n_89\,
      P(15) => \multi_1_s__0_n_90\,
      P(14) => \multi_1_s__0_n_91\,
      P(13) => \multi_1_s__0_n_92\,
      P(12) => \multi_1_s__0_n_93\,
      P(11) => \multi_1_s__0_n_94\,
      P(10) => \multi_1_s__0_n_95\,
      P(9) => \multi_1_s__0_n_96\,
      P(8) => \multi_1_s__0_n_97\,
      P(7) => \multi_1_s__0_n_98\,
      P(6) => \multi_1_s__0_n_99\,
      P(5) => \multi_1_s__0_n_100\,
      P(4) => \multi_1_s__0_n_101\,
      P(3) => \multi_1_s__0_n_102\,
      P(2) => \multi_1_s__0_n_103\,
      P(1) => \multi_1_s__0_n_104\,
      P(0) => \multi_1_s__0_n_105\,
      PATTERNBDETECT => \NLW_multi_1_s__0_PATTERNBDETECT_UNCONNECTED\,
      PATTERNDETECT => \NLW_multi_1_s__0_PATTERNDETECT_UNCONNECTED\,
      PCIN(47) => multi_1_s_n_106,
      PCIN(46) => multi_1_s_n_107,
      PCIN(45) => multi_1_s_n_108,
      PCIN(44) => multi_1_s_n_109,
      PCIN(43) => multi_1_s_n_110,
      PCIN(42) => multi_1_s_n_111,
      PCIN(41) => multi_1_s_n_112,
      PCIN(40) => multi_1_s_n_113,
      PCIN(39) => multi_1_s_n_114,
      PCIN(38) => multi_1_s_n_115,
      PCIN(37) => multi_1_s_n_116,
      PCIN(36) => multi_1_s_n_117,
      PCIN(35) => multi_1_s_n_118,
      PCIN(34) => multi_1_s_n_119,
      PCIN(33) => multi_1_s_n_120,
      PCIN(32) => multi_1_s_n_121,
      PCIN(31) => multi_1_s_n_122,
      PCIN(30) => multi_1_s_n_123,
      PCIN(29) => multi_1_s_n_124,
      PCIN(28) => multi_1_s_n_125,
      PCIN(27) => multi_1_s_n_126,
      PCIN(26) => multi_1_s_n_127,
      PCIN(25) => multi_1_s_n_128,
      PCIN(24) => multi_1_s_n_129,
      PCIN(23) => multi_1_s_n_130,
      PCIN(22) => multi_1_s_n_131,
      PCIN(21) => multi_1_s_n_132,
      PCIN(20) => multi_1_s_n_133,
      PCIN(19) => multi_1_s_n_134,
      PCIN(18) => multi_1_s_n_135,
      PCIN(17) => multi_1_s_n_136,
      PCIN(16) => multi_1_s_n_137,
      PCIN(15) => multi_1_s_n_138,
      PCIN(14) => multi_1_s_n_139,
      PCIN(13) => multi_1_s_n_140,
      PCIN(12) => multi_1_s_n_141,
      PCIN(11) => multi_1_s_n_142,
      PCIN(10) => multi_1_s_n_143,
      PCIN(9) => multi_1_s_n_144,
      PCIN(8) => multi_1_s_n_145,
      PCIN(7) => multi_1_s_n_146,
      PCIN(6) => multi_1_s_n_147,
      PCIN(5) => multi_1_s_n_148,
      PCIN(4) => multi_1_s_n_149,
      PCIN(3) => multi_1_s_n_150,
      PCIN(2) => multi_1_s_n_151,
      PCIN(1) => multi_1_s_n_152,
      PCIN(0) => multi_1_s_n_153,
      PCOUT(47 downto 0) => \NLW_multi_1_s__0_PCOUT_UNCONNECTED\(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => SR(0),
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => \NLW_multi_1_s__0_UNDERFLOW_UNCONNECTED\
    );
multi_2_s: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => q_reg(23),
      A(28) => q_reg(23),
      A(27) => q_reg(23),
      A(26) => q_reg(23),
      A(25) => q_reg(23),
      A(24) => q_reg(23),
      A(23 downto 0) => q_reg(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_multi_2_s_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => multi_2_s_0(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_multi_2_s_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_multi_2_s_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_multi_2_s_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_multi_2_s_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_multi_2_s_OVERFLOW_UNCONNECTED,
      P(47) => multi_2_s_n_58,
      P(46) => multi_2_s_n_59,
      P(45) => multi_2_s_n_60,
      P(44) => multi_2_s_n_61,
      P(43) => multi_2_s_n_62,
      P(42) => multi_2_s_n_63,
      P(41) => multi_2_s_n_64,
      P(40) => multi_2_s_n_65,
      P(39) => multi_2_s_n_66,
      P(38) => multi_2_s_n_67,
      P(37) => multi_2_s_n_68,
      P(36) => multi_2_s_n_69,
      P(35) => multi_2_s_n_70,
      P(34) => multi_2_s_n_71,
      P(33) => multi_2_s_n_72,
      P(32) => multi_2_s_n_73,
      P(31) => multi_2_s_n_74,
      P(30) => multi_2_s_n_75,
      P(29) => multi_2_s_n_76,
      P(28) => multi_2_s_n_77,
      P(27) => multi_2_s_n_78,
      P(26) => multi_2_s_n_79,
      P(25) => multi_2_s_n_80,
      P(24) => multi_2_s_n_81,
      P(23) => multi_2_s_n_82,
      P(22) => multi_2_s_n_83,
      P(21) => multi_2_s_n_84,
      P(20) => multi_2_s_n_85,
      P(19) => multi_2_s_n_86,
      P(18) => multi_2_s_n_87,
      P(17) => multi_2_s_n_88,
      P(16) => multi_2_s_n_89,
      P(15) => multi_2_s_n_90,
      P(14) => multi_2_s_n_91,
      P(13) => multi_2_s_n_92,
      P(12) => multi_2_s_n_93,
      P(11) => multi_2_s_n_94,
      P(10) => multi_2_s_n_95,
      P(9) => multi_2_s_n_96,
      P(8) => multi_2_s_n_97,
      P(7) => multi_2_s_n_98,
      P(6) => multi_2_s_n_99,
      P(5) => multi_2_s_n_100,
      P(4) => multi_2_s_n_101,
      P(3) => multi_2_s_n_102,
      P(2) => multi_2_s_n_103,
      P(1) => multi_2_s_n_104,
      P(0) => multi_2_s_n_105,
      PATTERNBDETECT => NLW_multi_2_s_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_multi_2_s_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => multi_2_s_n_106,
      PCOUT(46) => multi_2_s_n_107,
      PCOUT(45) => multi_2_s_n_108,
      PCOUT(44) => multi_2_s_n_109,
      PCOUT(43) => multi_2_s_n_110,
      PCOUT(42) => multi_2_s_n_111,
      PCOUT(41) => multi_2_s_n_112,
      PCOUT(40) => multi_2_s_n_113,
      PCOUT(39) => multi_2_s_n_114,
      PCOUT(38) => multi_2_s_n_115,
      PCOUT(37) => multi_2_s_n_116,
      PCOUT(36) => multi_2_s_n_117,
      PCOUT(35) => multi_2_s_n_118,
      PCOUT(34) => multi_2_s_n_119,
      PCOUT(33) => multi_2_s_n_120,
      PCOUT(32) => multi_2_s_n_121,
      PCOUT(31) => multi_2_s_n_122,
      PCOUT(30) => multi_2_s_n_123,
      PCOUT(29) => multi_2_s_n_124,
      PCOUT(28) => multi_2_s_n_125,
      PCOUT(27) => multi_2_s_n_126,
      PCOUT(26) => multi_2_s_n_127,
      PCOUT(25) => multi_2_s_n_128,
      PCOUT(24) => multi_2_s_n_129,
      PCOUT(23) => multi_2_s_n_130,
      PCOUT(22) => multi_2_s_n_131,
      PCOUT(21) => multi_2_s_n_132,
      PCOUT(20) => multi_2_s_n_133,
      PCOUT(19) => multi_2_s_n_134,
      PCOUT(18) => multi_2_s_n_135,
      PCOUT(17) => multi_2_s_n_136,
      PCOUT(16) => multi_2_s_n_137,
      PCOUT(15) => multi_2_s_n_138,
      PCOUT(14) => multi_2_s_n_139,
      PCOUT(13) => multi_2_s_n_140,
      PCOUT(12) => multi_2_s_n_141,
      PCOUT(11) => multi_2_s_n_142,
      PCOUT(10) => multi_2_s_n_143,
      PCOUT(9) => multi_2_s_n_144,
      PCOUT(8) => multi_2_s_n_145,
      PCOUT(7) => multi_2_s_n_146,
      PCOUT(6) => multi_2_s_n_147,
      PCOUT(5) => multi_2_s_n_148,
      PCOUT(4) => multi_2_s_n_149,
      PCOUT(3) => multi_2_s_n_150,
      PCOUT(2) => multi_2_s_n_151,
      PCOUT(1) => multi_2_s_n_152,
      PCOUT(0) => multi_2_s_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_multi_2_s_UNDERFLOW_UNCONNECTED
    );
\multi_2_s__0\: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => q_reg(23),
      A(28) => q_reg(23),
      A(27) => q_reg(23),
      A(26) => q_reg(23),
      A(25) => q_reg(23),
      A(24) => q_reg(23),
      A(23 downto 0) => q_reg(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => \NLW_multi_2_s__0_ACOUT_UNCONNECTED\(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => s00_axi_wdata(6),
      B(16) => s00_axi_wdata(6),
      B(15) => s00_axi_wdata(6),
      B(14) => s00_axi_wdata(6),
      B(13) => s00_axi_wdata(6),
      B(12) => s00_axi_wdata(6),
      B(11) => s00_axi_wdata(6),
      B(10) => s00_axi_wdata(6),
      B(9) => s00_axi_wdata(6),
      B(8) => s00_axi_wdata(6),
      B(7) => s00_axi_wdata(6),
      B(6 downto 0) => s00_axi_wdata(6 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => \NLW_multi_2_s__0_BCOUT_UNCONNECTED\(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => \NLW_multi_2_s__0_CARRYCASCOUT_UNCONNECTED\,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => \NLW_multi_2_s__0_CARRYOUT_UNCONNECTED\(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => \multi_1_s__0_0\(0),
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => \NLW_multi_2_s__0_MULTSIGNOUT_UNCONNECTED\,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => \NLW_multi_2_s__0_OVERFLOW_UNCONNECTED\,
      P(47 downto 27) => \NLW_multi_2_s__0_P_UNCONNECTED\(47 downto 27),
      P(26) => \multi_2_s__0_n_79\,
      P(25) => \multi_2_s__0_n_80\,
      P(24) => \multi_2_s__0_n_81\,
      P(23) => \multi_2_s__0_n_82\,
      P(22) => \multi_2_s__0_n_83\,
      P(21) => \multi_2_s__0_n_84\,
      P(20) => \multi_2_s__0_n_85\,
      P(19) => \multi_2_s__0_n_86\,
      P(18) => \multi_2_s__0_n_87\,
      P(17) => \multi_2_s__0_n_88\,
      P(16) => \multi_2_s__0_n_89\,
      P(15) => \multi_2_s__0_n_90\,
      P(14) => \multi_2_s__0_n_91\,
      P(13) => \multi_2_s__0_n_92\,
      P(12) => \multi_2_s__0_n_93\,
      P(11) => \multi_2_s__0_n_94\,
      P(10) => \multi_2_s__0_n_95\,
      P(9) => \multi_2_s__0_n_96\,
      P(8) => \multi_2_s__0_n_97\,
      P(7) => \multi_2_s__0_n_98\,
      P(6) => \multi_2_s__0_n_99\,
      P(5) => \multi_2_s__0_n_100\,
      P(4) => \multi_2_s__0_n_101\,
      P(3) => \multi_2_s__0_n_102\,
      P(2) => \multi_2_s__0_n_103\,
      P(1) => \multi_2_s__0_n_104\,
      P(0) => \multi_2_s__0_n_105\,
      PATTERNBDETECT => \NLW_multi_2_s__0_PATTERNBDETECT_UNCONNECTED\,
      PATTERNDETECT => \NLW_multi_2_s__0_PATTERNDETECT_UNCONNECTED\,
      PCIN(47) => multi_2_s_n_106,
      PCIN(46) => multi_2_s_n_107,
      PCIN(45) => multi_2_s_n_108,
      PCIN(44) => multi_2_s_n_109,
      PCIN(43) => multi_2_s_n_110,
      PCIN(42) => multi_2_s_n_111,
      PCIN(41) => multi_2_s_n_112,
      PCIN(40) => multi_2_s_n_113,
      PCIN(39) => multi_2_s_n_114,
      PCIN(38) => multi_2_s_n_115,
      PCIN(37) => multi_2_s_n_116,
      PCIN(36) => multi_2_s_n_117,
      PCIN(35) => multi_2_s_n_118,
      PCIN(34) => multi_2_s_n_119,
      PCIN(33) => multi_2_s_n_120,
      PCIN(32) => multi_2_s_n_121,
      PCIN(31) => multi_2_s_n_122,
      PCIN(30) => multi_2_s_n_123,
      PCIN(29) => multi_2_s_n_124,
      PCIN(28) => multi_2_s_n_125,
      PCIN(27) => multi_2_s_n_126,
      PCIN(26) => multi_2_s_n_127,
      PCIN(25) => multi_2_s_n_128,
      PCIN(24) => multi_2_s_n_129,
      PCIN(23) => multi_2_s_n_130,
      PCIN(22) => multi_2_s_n_131,
      PCIN(21) => multi_2_s_n_132,
      PCIN(20) => multi_2_s_n_133,
      PCIN(19) => multi_2_s_n_134,
      PCIN(18) => multi_2_s_n_135,
      PCIN(17) => multi_2_s_n_136,
      PCIN(16) => multi_2_s_n_137,
      PCIN(15) => multi_2_s_n_138,
      PCIN(14) => multi_2_s_n_139,
      PCIN(13) => multi_2_s_n_140,
      PCIN(12) => multi_2_s_n_141,
      PCIN(11) => multi_2_s_n_142,
      PCIN(10) => multi_2_s_n_143,
      PCIN(9) => multi_2_s_n_144,
      PCIN(8) => multi_2_s_n_145,
      PCIN(7) => multi_2_s_n_146,
      PCIN(6) => multi_2_s_n_147,
      PCIN(5) => multi_2_s_n_148,
      PCIN(4) => multi_2_s_n_149,
      PCIN(3) => multi_2_s_n_150,
      PCIN(2) => multi_2_s_n_151,
      PCIN(1) => multi_2_s_n_152,
      PCIN(0) => multi_2_s_n_153,
      PCOUT(47 downto 0) => \NLW_multi_2_s__0_PCOUT_UNCONNECTED\(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => SR(0),
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => \NLW_multi_2_s__0_UNDERFLOW_UNCONNECTED\
    );
multi_3_s: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => q_reg(23),
      A(28) => q_reg(23),
      A(27) => q_reg(23),
      A(26) => q_reg(23),
      A(25) => q_reg(23),
      A(24) => q_reg(23),
      A(23 downto 0) => q_reg(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_multi_3_s_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => multi_3_s_0(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_multi_3_s_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_multi_3_s_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_multi_3_s_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_multi_3_s_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_multi_3_s_OVERFLOW_UNCONNECTED,
      P(47) => multi_3_s_n_58,
      P(46) => multi_3_s_n_59,
      P(45) => multi_3_s_n_60,
      P(44) => multi_3_s_n_61,
      P(43) => multi_3_s_n_62,
      P(42) => multi_3_s_n_63,
      P(41) => multi_3_s_n_64,
      P(40) => multi_3_s_n_65,
      P(39) => multi_3_s_n_66,
      P(38) => multi_3_s_n_67,
      P(37) => multi_3_s_n_68,
      P(36) => multi_3_s_n_69,
      P(35) => multi_3_s_n_70,
      P(34) => multi_3_s_n_71,
      P(33) => multi_3_s_n_72,
      P(32) => multi_3_s_n_73,
      P(31) => multi_3_s_n_74,
      P(30) => multi_3_s_n_75,
      P(29) => multi_3_s_n_76,
      P(28) => multi_3_s_n_77,
      P(27) => multi_3_s_n_78,
      P(26) => multi_3_s_n_79,
      P(25) => multi_3_s_n_80,
      P(24) => multi_3_s_n_81,
      P(23) => multi_3_s_n_82,
      P(22) => multi_3_s_n_83,
      P(21) => multi_3_s_n_84,
      P(20) => multi_3_s_n_85,
      P(19) => multi_3_s_n_86,
      P(18) => multi_3_s_n_87,
      P(17) => multi_3_s_n_88,
      P(16) => multi_3_s_n_89,
      P(15) => multi_3_s_n_90,
      P(14) => multi_3_s_n_91,
      P(13) => multi_3_s_n_92,
      P(12) => multi_3_s_n_93,
      P(11) => multi_3_s_n_94,
      P(10) => multi_3_s_n_95,
      P(9) => multi_3_s_n_96,
      P(8) => multi_3_s_n_97,
      P(7) => multi_3_s_n_98,
      P(6) => multi_3_s_n_99,
      P(5) => multi_3_s_n_100,
      P(4) => multi_3_s_n_101,
      P(3) => multi_3_s_n_102,
      P(2) => multi_3_s_n_103,
      P(1) => multi_3_s_n_104,
      P(0) => multi_3_s_n_105,
      PATTERNBDETECT => NLW_multi_3_s_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_multi_3_s_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => multi_3_s_n_106,
      PCOUT(46) => multi_3_s_n_107,
      PCOUT(45) => multi_3_s_n_108,
      PCOUT(44) => multi_3_s_n_109,
      PCOUT(43) => multi_3_s_n_110,
      PCOUT(42) => multi_3_s_n_111,
      PCOUT(41) => multi_3_s_n_112,
      PCOUT(40) => multi_3_s_n_113,
      PCOUT(39) => multi_3_s_n_114,
      PCOUT(38) => multi_3_s_n_115,
      PCOUT(37) => multi_3_s_n_116,
      PCOUT(36) => multi_3_s_n_117,
      PCOUT(35) => multi_3_s_n_118,
      PCOUT(34) => multi_3_s_n_119,
      PCOUT(33) => multi_3_s_n_120,
      PCOUT(32) => multi_3_s_n_121,
      PCOUT(31) => multi_3_s_n_122,
      PCOUT(30) => multi_3_s_n_123,
      PCOUT(29) => multi_3_s_n_124,
      PCOUT(28) => multi_3_s_n_125,
      PCOUT(27) => multi_3_s_n_126,
      PCOUT(26) => multi_3_s_n_127,
      PCOUT(25) => multi_3_s_n_128,
      PCOUT(24) => multi_3_s_n_129,
      PCOUT(23) => multi_3_s_n_130,
      PCOUT(22) => multi_3_s_n_131,
      PCOUT(21) => multi_3_s_n_132,
      PCOUT(20) => multi_3_s_n_133,
      PCOUT(19) => multi_3_s_n_134,
      PCOUT(18) => multi_3_s_n_135,
      PCOUT(17) => multi_3_s_n_136,
      PCOUT(16) => multi_3_s_n_137,
      PCOUT(15) => multi_3_s_n_138,
      PCOUT(14) => multi_3_s_n_139,
      PCOUT(13) => multi_3_s_n_140,
      PCOUT(12) => multi_3_s_n_141,
      PCOUT(11) => multi_3_s_n_142,
      PCOUT(10) => multi_3_s_n_143,
      PCOUT(9) => multi_3_s_n_144,
      PCOUT(8) => multi_3_s_n_145,
      PCOUT(7) => multi_3_s_n_146,
      PCOUT(6) => multi_3_s_n_147,
      PCOUT(5) => multi_3_s_n_148,
      PCOUT(4) => multi_3_s_n_149,
      PCOUT(3) => multi_3_s_n_150,
      PCOUT(2) => multi_3_s_n_151,
      PCOUT(1) => multi_3_s_n_152,
      PCOUT(0) => multi_3_s_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_multi_3_s_UNDERFLOW_UNCONNECTED
    );
\multi_3_s__0\: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => q_reg(23),
      A(28) => q_reg(23),
      A(27) => q_reg(23),
      A(26) => q_reg(23),
      A(25) => q_reg(23),
      A(24) => q_reg(23),
      A(23 downto 0) => q_reg(23 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => \NLW_multi_3_s__0_ACOUT_UNCONNECTED\(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => s00_axi_wdata(6),
      B(16) => s00_axi_wdata(6),
      B(15) => s00_axi_wdata(6),
      B(14) => s00_axi_wdata(6),
      B(13) => s00_axi_wdata(6),
      B(12) => s00_axi_wdata(6),
      B(11) => s00_axi_wdata(6),
      B(10) => s00_axi_wdata(6),
      B(9) => s00_axi_wdata(6),
      B(8) => s00_axi_wdata(6),
      B(7) => s00_axi_wdata(6),
      B(6 downto 0) => s00_axi_wdata(6 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => \NLW_multi_3_s__0_BCOUT_UNCONNECTED\(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => \NLW_multi_3_s__0_CARRYCASCOUT_UNCONNECTED\,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => \NLW_multi_3_s__0_CARRYOUT_UNCONNECTED\(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => E(0),
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => \NLW_multi_3_s__0_MULTSIGNOUT_UNCONNECTED\,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => \NLW_multi_3_s__0_OVERFLOW_UNCONNECTED\,
      P(47 downto 27) => \NLW_multi_3_s__0_P_UNCONNECTED\(47 downto 27),
      P(26) => \multi_3_s__0_n_79\,
      P(25) => \multi_3_s__0_n_80\,
      P(24) => \multi_3_s__0_n_81\,
      P(23) => \multi_3_s__0_n_82\,
      P(22) => \multi_3_s__0_n_83\,
      P(21) => \multi_3_s__0_n_84\,
      P(20) => \multi_3_s__0_n_85\,
      P(19) => \multi_3_s__0_n_86\,
      P(18) => \multi_3_s__0_n_87\,
      P(17) => \multi_3_s__0_n_88\,
      P(16) => \multi_3_s__0_n_89\,
      P(15) => \multi_3_s__0_n_90\,
      P(14) => \multi_3_s__0_n_91\,
      P(13) => \multi_3_s__0_n_92\,
      P(12) => \multi_3_s__0_n_93\,
      P(11) => \multi_3_s__0_n_94\,
      P(10) => \multi_3_s__0_n_95\,
      P(9) => \multi_3_s__0_n_96\,
      P(8) => \multi_3_s__0_n_97\,
      P(7) => \multi_3_s__0_n_98\,
      P(6) => \multi_3_s__0_n_99\,
      P(5) => \multi_3_s__0_n_100\,
      P(4) => \multi_3_s__0_n_101\,
      P(3) => \multi_3_s__0_n_102\,
      P(2) => \multi_3_s__0_n_103\,
      P(1) => \multi_3_s__0_n_104\,
      P(0) => \multi_3_s__0_n_105\,
      PATTERNBDETECT => \NLW_multi_3_s__0_PATTERNBDETECT_UNCONNECTED\,
      PATTERNDETECT => \NLW_multi_3_s__0_PATTERNDETECT_UNCONNECTED\,
      PCIN(47) => multi_3_s_n_106,
      PCIN(46) => multi_3_s_n_107,
      PCIN(45) => multi_3_s_n_108,
      PCIN(44) => multi_3_s_n_109,
      PCIN(43) => multi_3_s_n_110,
      PCIN(42) => multi_3_s_n_111,
      PCIN(41) => multi_3_s_n_112,
      PCIN(40) => multi_3_s_n_113,
      PCIN(39) => multi_3_s_n_114,
      PCIN(38) => multi_3_s_n_115,
      PCIN(37) => multi_3_s_n_116,
      PCIN(36) => multi_3_s_n_117,
      PCIN(35) => multi_3_s_n_118,
      PCIN(34) => multi_3_s_n_119,
      PCIN(33) => multi_3_s_n_120,
      PCIN(32) => multi_3_s_n_121,
      PCIN(31) => multi_3_s_n_122,
      PCIN(30) => multi_3_s_n_123,
      PCIN(29) => multi_3_s_n_124,
      PCIN(28) => multi_3_s_n_125,
      PCIN(27) => multi_3_s_n_126,
      PCIN(26) => multi_3_s_n_127,
      PCIN(25) => multi_3_s_n_128,
      PCIN(24) => multi_3_s_n_129,
      PCIN(23) => multi_3_s_n_130,
      PCIN(22) => multi_3_s_n_131,
      PCIN(21) => multi_3_s_n_132,
      PCIN(20) => multi_3_s_n_133,
      PCIN(19) => multi_3_s_n_134,
      PCIN(18) => multi_3_s_n_135,
      PCIN(17) => multi_3_s_n_136,
      PCIN(16) => multi_3_s_n_137,
      PCIN(15) => multi_3_s_n_138,
      PCIN(14) => multi_3_s_n_139,
      PCIN(13) => multi_3_s_n_140,
      PCIN(12) => multi_3_s_n_141,
      PCIN(11) => multi_3_s_n_142,
      PCIN(10) => multi_3_s_n_143,
      PCIN(9) => multi_3_s_n_144,
      PCIN(8) => multi_3_s_n_145,
      PCIN(7) => multi_3_s_n_146,
      PCIN(6) => multi_3_s_n_147,
      PCIN(5) => multi_3_s_n_148,
      PCIN(4) => multi_3_s_n_149,
      PCIN(3) => multi_3_s_n_150,
      PCIN(2) => multi_3_s_n_151,
      PCIN(1) => multi_3_s_n_152,
      PCIN(0) => multi_3_s_n_153,
      PCOUT(47 downto 0) => \NLW_multi_3_s__0_PCOUT_UNCONNECTED\(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => SR(0),
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => \NLW_multi_3_s__0_UNDERFLOW_UNCONNECTED\
    );
second_reg: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0
     port map (
      P(1) => \multi_3_s__0_n_79\,
      P(0) => \multi_3_s__0_n_80\,
      Q(1 downto 0) => Q(23 downto 22),
      S(0) => second_reg_n_0,
      \multi_2_s__0\(22 downto 0) => adder_3_s(22 downto 0),
      \q_reg[23]_0\(23) => \multi_2_s__0_n_79\,
      \q_reg[23]_0\(22) => \multi_2_s__0_n_80\,
      \q_reg[23]_0\(21) => \multi_2_s__0_n_81\,
      \q_reg[23]_0\(20) => \multi_2_s__0_n_82\,
      \q_reg[23]_0\(19) => \multi_2_s__0_n_83\,
      \q_reg[23]_0\(18) => \multi_2_s__0_n_84\,
      \q_reg[23]_0\(17) => \multi_2_s__0_n_85\,
      \q_reg[23]_0\(16) => \multi_2_s__0_n_86\,
      \q_reg[23]_0\(15) => \multi_2_s__0_n_87\,
      \q_reg[23]_0\(14) => \multi_2_s__0_n_88\,
      \q_reg[23]_0\(13) => \multi_2_s__0_n_89\,
      \q_reg[23]_0\(12) => \multi_2_s__0_n_90\,
      \q_reg[23]_0\(11) => \multi_2_s__0_n_91\,
      \q_reg[23]_0\(10) => \multi_2_s__0_n_92\,
      \q_reg[23]_0\(9) => \multi_2_s__0_n_93\,
      \q_reg[23]_0\(8) => \multi_2_s__0_n_94\,
      \q_reg[23]_0\(7) => \multi_2_s__0_n_95\,
      \q_reg[23]_0\(6) => \multi_2_s__0_n_96\,
      \q_reg[23]_0\(5) => \multi_2_s__0_n_97\,
      \q_reg[23]_0\(4) => \multi_2_s__0_n_98\,
      \q_reg[23]_0\(3) => \multi_2_s__0_n_99\,
      \q_reg[23]_0\(2) => \multi_2_s__0_n_100\,
      \q_reg[23]_0\(1) => \multi_2_s__0_n_101\,
      \q_reg[23]_0\(0) => \multi_2_s__0_n_102\,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0 is
  port (
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0 is
  signal adder_2_s : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal digital_parametric_eq_v1_0_S00_AXI_inst_n_1 : STD_LOGIC;
  signal digital_parametric_eq_v1_0_S00_AXI_inst_n_65 : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 23 to 23 );
  signal slv_reg2 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal slv_reg3 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 23 downto 0 );
begin
digital_parametric_eq_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI
     port map (
      D(23 downto 0) => adder_2_s(23 downto 0),
      E(0) => p_1_in(23),
      Q(23 downto 0) => slv_reg4(23 downto 0),
      SR(0) => digital_parametric_eq_v1_0_S00_AXI_inst_n_1,
      \axi_awaddr_reg[4]_0\(0) => digital_parametric_eq_v1_0_S00_AXI_inst_n_65,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      \slv_reg2_reg[16]_0\(16 downto 0) => slv_reg2(16 downto 0),
      \slv_reg3_reg[16]_0\(16 downto 0) => slv_reg3(16 downto 0)
    );
parametric_eq_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq
     port map (
      D(23 downto 0) => adder_2_s(23 downto 0),
      E(0) => p_1_in(23),
      Q(23 downto 0) => slv_reg4(23 downto 0),
      SR(0) => digital_parametric_eq_v1_0_S00_AXI_inst_n_1,
      \multi_1_s__0_0\(0) => digital_parametric_eq_v1_0_S00_AXI_inst_n_65,
      multi_2_s_0(16 downto 0) => slv_reg3(16 downto 0),
      multi_3_s_0(16 downto 0) => slv_reg2(16 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_wdata(6 downto 0) => s00_axi_wdata(23 downto 17)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Parametric_EQ_digital_parametric_eq_0_0,digital_parametric_eq_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "digital_parametric_eq_v1_0,Vivado 2019.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Parametric_EQ_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 6, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN Parametric_EQ_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0
     port map (
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
