// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Fri Jun 12 23:49:55 2020
// Host        : kakaroto-PC running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Parametric_EQ_digital_parametric_eq_0_0_sim_netlist.v
// Design      : Parametric_EQ_digital_parametric_eq_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "Parametric_EQ_digital_parametric_eq_0_0,digital_parametric_eq_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "digital_parametric_eq_v1_0,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axi_aclk,
    s00_axi_aresetn,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Parametric_EQ_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 6, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN Parametric_EQ_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0 U0
       (.s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0
   (s00_axi_awready,
    s00_axi_wready,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output s00_axi_awready;
  output s00_axi_wready;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [23:0]adder_2_s;
  wire digital_parametric_eq_v1_0_S00_AXI_inst_n_1;
  wire digital_parametric_eq_v1_0_S00_AXI_inst_n_65;
  wire [23:23]p_1_in;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [16:0]slv_reg2;
  wire [16:0]slv_reg3;
  wire [23:0]slv_reg4;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI digital_parametric_eq_v1_0_S00_AXI_inst
       (.D(adder_2_s),
        .E(p_1_in),
        .Q(slv_reg4),
        .SR(digital_parametric_eq_v1_0_S00_AXI_inst_n_1),
        .\axi_awaddr_reg[4]_0 (digital_parametric_eq_v1_0_S00_AXI_inst_n_65),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .\slv_reg2_reg[16]_0 (slv_reg2),
        .\slv_reg3_reg[16]_0 (slv_reg3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq parametric_eq_inst
       (.D(adder_2_s),
        .E(p_1_in),
        .Q(slv_reg4),
        .SR(digital_parametric_eq_v1_0_S00_AXI_inst_n_1),
        .multi_1_s__0_0(digital_parametric_eq_v1_0_S00_AXI_inst_n_65),
        .multi_2_s_0(slv_reg3),
        .multi_3_s_0(slv_reg2),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_wdata(s00_axi_wdata[23:17]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_digital_parametric_eq_v1_0_S00_AXI
   (s00_axi_awready,
    SR,
    s00_axi_wready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    Q,
    \slv_reg3_reg[16]_0 ,
    \slv_reg2_reg[16]_0 ,
    E,
    \axi_awaddr_reg[4]_0 ,
    s00_axi_rdata,
    s00_axi_aclk,
    s00_axi_aresetn,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_wdata,
    D,
    s00_axi_araddr,
    s00_axi_wstrb);
  output s00_axi_awready;
  output [0:0]SR;
  output s00_axi_wready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output [23:0]Q;
  output [16:0]\slv_reg3_reg[16]_0 ;
  output [16:0]\slv_reg2_reg[16]_0 ;
  output [0:0]E;
  output [0:0]\axi_awaddr_reg[4]_0 ;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [23:0]D;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire [23:0]D;
  wire [0:0]E;
  wire [23:0]Q;
  wire [0:0]SR;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire [0:0]\axi_awaddr_reg[4]_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [2:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [31:0]slv_reg0;
  wire \slv_reg0[15]_i_1_n_0 ;
  wire \slv_reg0[23]_i_1_n_0 ;
  wire \slv_reg0[31]_i_1_n_0 ;
  wire \slv_reg0[7]_i_1_n_0 ;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:17]slv_reg2;
  wire [16:0]\slv_reg2_reg[16]_0 ;
  wire [31:17]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [16:0]\slv_reg3_reg[16]_0 ;
  wire [31:24]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [23:0]slv_reg5;
  wire slv_reg_rden;
  wire slv_reg_wren__2;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(s00_axi_awready),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(SR));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .S(SR));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .S(SR));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .S(SR));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s00_axi_arready),
        .R(SR));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_awready),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_awready),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_awready),
        .I5(p_0_in[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in[2]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_awready),
        .I2(s00_axi_wready),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(SR));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[0]),
        .I4(sel0[0]),
        .I5(Q[0]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [0]),
        .I1(\slv_reg2_reg[16]_0 [0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(sel0[0]),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[10]),
        .I4(sel0[0]),
        .I5(Q[10]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [10]),
        .I1(\slv_reg2_reg[16]_0 [10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(sel0[0]),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[11]),
        .I4(sel0[0]),
        .I5(Q[11]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [11]),
        .I1(\slv_reg2_reg[16]_0 [11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(sel0[0]),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[12]),
        .I4(sel0[0]),
        .I5(Q[12]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [12]),
        .I1(\slv_reg2_reg[16]_0 [12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(sel0[0]),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[13]),
        .I4(sel0[0]),
        .I5(Q[13]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [13]),
        .I1(\slv_reg2_reg[16]_0 [13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[14]),
        .I4(sel0[0]),
        .I5(Q[14]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [14]),
        .I1(\slv_reg2_reg[16]_0 [14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[15]),
        .I4(sel0[0]),
        .I5(Q[15]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [15]),
        .I1(\slv_reg2_reg[16]_0 [15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(Q[16]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [16]),
        .I1(\slv_reg2_reg[16]_0 [16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(Q[17]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(Q[18]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(Q[19]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[1]),
        .I4(sel0[0]),
        .I5(Q[1]),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [1]),
        .I1(\slv_reg2_reg[16]_0 [1]),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(sel0[0]),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(Q[20]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(Q[21]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(Q[22]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(Q[23]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[24]),
        .I4(sel0[0]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[25]),
        .I4(sel0[0]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[26]),
        .I4(sel0[0]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[27]),
        .I4(sel0[0]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[28]),
        .I4(sel0[0]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[29]),
        .I4(sel0[0]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[2]),
        .I4(sel0[0]),
        .I5(Q[2]),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [2]),
        .I1(\slv_reg2_reg[16]_0 [2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(sel0[0]),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[30]),
        .I4(sel0[0]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'h0A0A3A0A)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[31]),
        .I4(sel0[0]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[3]),
        .I4(sel0[0]),
        .I5(Q[3]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [3]),
        .I1(\slv_reg2_reg[16]_0 [3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(sel0[0]),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[4]),
        .I4(sel0[0]),
        .I5(Q[4]),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [4]),
        .I1(\slv_reg2_reg[16]_0 [4]),
        .I2(sel0[1]),
        .I3(slv_reg1[4]),
        .I4(sel0[0]),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[5]),
        .I4(sel0[0]),
        .I5(Q[5]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [5]),
        .I1(\slv_reg2_reg[16]_0 [5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(sel0[0]),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[6]),
        .I4(sel0[0]),
        .I5(Q[6]),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [6]),
        .I1(\slv_reg2_reg[16]_0 [6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(sel0[0]),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[7]),
        .I4(sel0[0]),
        .I5(Q[7]),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [7]),
        .I1(\slv_reg2_reg[16]_0 [7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(sel0[0]),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[8]),
        .I4(sel0[0]),
        .I5(Q[8]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [8]),
        .I1(\slv_reg2_reg[16]_0 [8]),
        .I2(sel0[1]),
        .I3(slv_reg1[8]),
        .I4(sel0[0]),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg5[9]),
        .I4(sel0[0]),
        .I5(Q[9]),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg3_reg[16]_0 [9]),
        .I1(\slv_reg2_reg[16]_0 [9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(sel0[0]),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(SR));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(SR));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(SR));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(SR));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(SR));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(SR));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(SR));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(SR));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(SR));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(SR));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(SR));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(SR));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(SR));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(SR));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(SR));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(SR));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(SR));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(SR));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(SR));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(SR));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(SR));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(SR));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(SR));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(SR));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(SR));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(SR));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(SR));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(SR));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(SR));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(SR));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(SR));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(SR));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg0[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg0[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg0[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_awready),
        .I2(s00_axi_wready),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg0[7]_i_1_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(SR));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(SR));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(SR));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(SR));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(SR));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(SR));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(SR));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(SR));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(SR));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(SR));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(SR));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(SR));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(SR));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(SR));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(SR));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(SR));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(SR));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(SR));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(SR));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(SR));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(SR));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(SR));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(SR));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(SR));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(SR));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(SR));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(SR));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(SR));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(SR));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(SR));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(SR));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SR));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SR));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SR));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SR));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SR));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SR));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SR));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(SR));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(SR));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(SR));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(SR));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SR));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(SR));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(SR));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(SR));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(SR));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(SR));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(SR));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(SR));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(SR));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(SR));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(SR));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SR));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(SR));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(SR));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SR));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(SR));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SR));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SR));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SR));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SR));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .O(E));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .O(p_1_in[31]));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg2_reg[16]_0 [0]),
        .R(SR));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg2_reg[16]_0 [10]),
        .R(SR));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg2_reg[16]_0 [11]),
        .R(SR));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg2_reg[16]_0 [12]),
        .R(SR));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg2_reg[16]_0 [13]),
        .R(SR));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg2_reg[16]_0 [14]),
        .R(SR));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg2_reg[16]_0 [15]),
        .R(SR));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg2_reg[16]_0 [16]),
        .R(SR));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(SR));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(SR));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(SR));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg2_reg[16]_0 [1]),
        .R(SR));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(SR));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(SR));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(SR));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(SR));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(SR));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(SR));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(SR));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(SR));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(SR));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(SR));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg2_reg[16]_0 [2]),
        .R(SR));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(SR));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(SR));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg2_reg[16]_0 [3]),
        .R(SR));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg2_reg[16]_0 [4]),
        .R(SR));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg2_reg[16]_0 [5]),
        .R(SR));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg2_reg[16]_0 [6]),
        .R(SR));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg2_reg[16]_0 [7]),
        .R(SR));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg2_reg[16]_0 [8]),
        .R(SR));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg2_reg[16]_0 [9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[2]),
        .O(\axi_awaddr_reg[4]_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg3_reg[16]_0 [0]),
        .R(SR));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg3_reg[16]_0 [10]),
        .R(SR));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg3_reg[16]_0 [11]),
        .R(SR));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg3_reg[16]_0 [12]),
        .R(SR));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg3_reg[16]_0 [13]),
        .R(SR));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg3_reg[16]_0 [14]),
        .R(SR));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg3_reg[16]_0 [15]),
        .R(SR));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg3_reg[16]_0 [16]),
        .R(SR));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(SR));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(SR));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(SR));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg3_reg[16]_0 [1]),
        .R(SR));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(SR));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(SR));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(SR));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr_reg[4]_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(SR));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(SR));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(SR));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(SR));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(SR));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(SR));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(SR));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg3_reg[16]_0 [2]),
        .R(SR));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(SR));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(SR));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg3_reg[16]_0 [3]),
        .R(SR));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg3_reg[16]_0 [4]),
        .R(SR));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg3_reg[16]_0 [5]),
        .R(SR));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg3_reg[16]_0 [6]),
        .R(SR));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg3_reg[16]_0 [7]),
        .R(SR));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg3_reg[16]_0 [8]),
        .R(SR));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg3_reg[16]_0 [9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(SR));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(SR));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(SR));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(SR));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(SR));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(SR));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(SR));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(SR));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(Q[9]),
        .R(SR));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(slv_reg5[0]),
        .R(1'b0));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[10]),
        .Q(slv_reg5[10]),
        .R(1'b0));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[11]),
        .Q(slv_reg5[11]),
        .R(1'b0));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[12]),
        .Q(slv_reg5[12]),
        .R(1'b0));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[13]),
        .Q(slv_reg5[13]),
        .R(1'b0));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[14]),
        .Q(slv_reg5[14]),
        .R(1'b0));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[15]),
        .Q(slv_reg5[15]),
        .R(1'b0));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[16]),
        .Q(slv_reg5[16]),
        .R(1'b0));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[17]),
        .Q(slv_reg5[17]),
        .R(1'b0));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[18]),
        .Q(slv_reg5[18]),
        .R(1'b0));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[19]),
        .Q(slv_reg5[19]),
        .R(1'b0));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(slv_reg5[1]),
        .R(1'b0));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[20]),
        .Q(slv_reg5[20]),
        .R(1'b0));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[21]),
        .Q(slv_reg5[21]),
        .R(1'b0));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[22]),
        .Q(slv_reg5[22]),
        .R(1'b0));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[23]),
        .Q(slv_reg5[23]),
        .R(1'b0));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[2]),
        .Q(slv_reg5[2]),
        .R(1'b0));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[3]),
        .Q(slv_reg5[3]),
        .R(1'b0));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[4]),
        .Q(slv_reg5[4]),
        .R(1'b0));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[5]),
        .Q(slv_reg5[5]),
        .R(1'b0));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[6]),
        .Q(slv_reg5[6]),
        .R(1'b0));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[7]),
        .Q(slv_reg5[7]),
        .R(1'b0));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[8]),
        .Q(slv_reg5[8]),
        .R(1'b0));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[9]),
        .Q(slv_reg5[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_parametric_eq
   (D,
    multi_3_s_0,
    E,
    s00_axi_aclk,
    SR,
    s00_axi_wdata,
    multi_2_s_0,
    multi_1_s__0_0,
    Q,
    s00_axi_aresetn);
  output [23:0]D;
  input [16:0]multi_3_s_0;
  input [0:0]E;
  input s00_axi_aclk;
  input [0:0]SR;
  input [6:0]s00_axi_wdata;
  input [16:0]multi_2_s_0;
  input [0:0]multi_1_s__0_0;
  input [23:0]Q;
  input s00_axi_aresetn;

  wire [23:0]A;
  wire [23:0]D;
  wire [0:0]E;
  wire [23:0]Q;
  wire [0:0]SR;
  wire adder_1_s__0_carry__0_i_1_n_0;
  wire adder_1_s__0_carry__0_i_2_n_0;
  wire adder_1_s__0_carry__0_i_3_n_0;
  wire adder_1_s__0_carry__0_i_4_n_0;
  wire adder_1_s__0_carry__0_i_5_n_0;
  wire adder_1_s__0_carry__0_i_6_n_0;
  wire adder_1_s__0_carry__0_i_7_n_0;
  wire adder_1_s__0_carry__0_i_8_n_0;
  wire adder_1_s__0_carry__0_n_0;
  wire adder_1_s__0_carry__0_n_1;
  wire adder_1_s__0_carry__0_n_2;
  wire adder_1_s__0_carry__0_n_3;
  wire adder_1_s__0_carry__1_i_1_n_0;
  wire adder_1_s__0_carry__1_i_2_n_0;
  wire adder_1_s__0_carry__1_i_3_n_0;
  wire adder_1_s__0_carry__1_i_4_n_0;
  wire adder_1_s__0_carry__1_i_5_n_0;
  wire adder_1_s__0_carry__1_i_6_n_0;
  wire adder_1_s__0_carry__1_i_7_n_0;
  wire adder_1_s__0_carry__1_i_8_n_0;
  wire adder_1_s__0_carry__1_n_0;
  wire adder_1_s__0_carry__1_n_1;
  wire adder_1_s__0_carry__1_n_2;
  wire adder_1_s__0_carry__1_n_3;
  wire adder_1_s__0_carry__2_i_1_n_0;
  wire adder_1_s__0_carry__2_i_2_n_0;
  wire adder_1_s__0_carry__2_i_3_n_0;
  wire adder_1_s__0_carry__2_i_4_n_0;
  wire adder_1_s__0_carry__2_i_5_n_0;
  wire adder_1_s__0_carry__2_i_6_n_0;
  wire adder_1_s__0_carry__2_i_7_n_0;
  wire adder_1_s__0_carry__2_i_8_n_0;
  wire adder_1_s__0_carry__2_n_0;
  wire adder_1_s__0_carry__2_n_1;
  wire adder_1_s__0_carry__2_n_2;
  wire adder_1_s__0_carry__2_n_3;
  wire adder_1_s__0_carry__3_i_1_n_0;
  wire adder_1_s__0_carry__3_i_2_n_0;
  wire adder_1_s__0_carry__3_i_3_n_0;
  wire adder_1_s__0_carry__3_i_4_n_0;
  wire adder_1_s__0_carry__3_i_5_n_0;
  wire adder_1_s__0_carry__3_i_6_n_0;
  wire adder_1_s__0_carry__3_i_7_n_0;
  wire adder_1_s__0_carry__3_i_8_n_0;
  wire adder_1_s__0_carry__3_n_0;
  wire adder_1_s__0_carry__3_n_1;
  wire adder_1_s__0_carry__3_n_2;
  wire adder_1_s__0_carry__3_n_3;
  wire adder_1_s__0_carry__4_i_1_n_0;
  wire adder_1_s__0_carry__4_i_2_n_0;
  wire adder_1_s__0_carry__4_i_3_n_0;
  wire adder_1_s__0_carry__4_i_5_n_0;
  wire adder_1_s__0_carry__4_i_6_n_0;
  wire adder_1_s__0_carry__4_i_7_n_0;
  wire adder_1_s__0_carry__4_n_1;
  wire adder_1_s__0_carry__4_n_2;
  wire adder_1_s__0_carry__4_n_3;
  wire adder_1_s__0_carry_i_1_n_0;
  wire adder_1_s__0_carry_i_2_n_0;
  wire adder_1_s__0_carry_i_3_n_0;
  wire adder_1_s__0_carry_i_4_n_0;
  wire adder_1_s__0_carry_i_5_n_0;
  wire adder_1_s__0_carry_i_6_n_0;
  wire adder_1_s__0_carry_i_7_n_0;
  wire adder_1_s__0_carry_n_0;
  wire adder_1_s__0_carry_n_1;
  wire adder_1_s__0_carry_n_2;
  wire adder_1_s__0_carry_n_3;
  wire [22:0]adder_3_s;
  wire [0:0]multi_1_s__0_0;
  wire multi_1_s__0_n_100;
  wire multi_1_s__0_n_101;
  wire multi_1_s__0_n_102;
  wire multi_1_s__0_n_103;
  wire multi_1_s__0_n_104;
  wire multi_1_s__0_n_105;
  wire multi_1_s__0_n_79;
  wire multi_1_s__0_n_80;
  wire multi_1_s__0_n_81;
  wire multi_1_s__0_n_82;
  wire multi_1_s__0_n_83;
  wire multi_1_s__0_n_84;
  wire multi_1_s__0_n_85;
  wire multi_1_s__0_n_86;
  wire multi_1_s__0_n_87;
  wire multi_1_s__0_n_88;
  wire multi_1_s__0_n_89;
  wire multi_1_s__0_n_90;
  wire multi_1_s__0_n_91;
  wire multi_1_s__0_n_92;
  wire multi_1_s__0_n_93;
  wire multi_1_s__0_n_94;
  wire multi_1_s__0_n_95;
  wire multi_1_s__0_n_96;
  wire multi_1_s__0_n_97;
  wire multi_1_s__0_n_98;
  wire multi_1_s__0_n_99;
  wire multi_1_s_n_100;
  wire multi_1_s_n_101;
  wire multi_1_s_n_102;
  wire multi_1_s_n_103;
  wire multi_1_s_n_104;
  wire multi_1_s_n_105;
  wire multi_1_s_n_106;
  wire multi_1_s_n_107;
  wire multi_1_s_n_108;
  wire multi_1_s_n_109;
  wire multi_1_s_n_110;
  wire multi_1_s_n_111;
  wire multi_1_s_n_112;
  wire multi_1_s_n_113;
  wire multi_1_s_n_114;
  wire multi_1_s_n_115;
  wire multi_1_s_n_116;
  wire multi_1_s_n_117;
  wire multi_1_s_n_118;
  wire multi_1_s_n_119;
  wire multi_1_s_n_120;
  wire multi_1_s_n_121;
  wire multi_1_s_n_122;
  wire multi_1_s_n_123;
  wire multi_1_s_n_124;
  wire multi_1_s_n_125;
  wire multi_1_s_n_126;
  wire multi_1_s_n_127;
  wire multi_1_s_n_128;
  wire multi_1_s_n_129;
  wire multi_1_s_n_130;
  wire multi_1_s_n_131;
  wire multi_1_s_n_132;
  wire multi_1_s_n_133;
  wire multi_1_s_n_134;
  wire multi_1_s_n_135;
  wire multi_1_s_n_136;
  wire multi_1_s_n_137;
  wire multi_1_s_n_138;
  wire multi_1_s_n_139;
  wire multi_1_s_n_140;
  wire multi_1_s_n_141;
  wire multi_1_s_n_142;
  wire multi_1_s_n_143;
  wire multi_1_s_n_144;
  wire multi_1_s_n_145;
  wire multi_1_s_n_146;
  wire multi_1_s_n_147;
  wire multi_1_s_n_148;
  wire multi_1_s_n_149;
  wire multi_1_s_n_150;
  wire multi_1_s_n_151;
  wire multi_1_s_n_152;
  wire multi_1_s_n_153;
  wire multi_1_s_n_58;
  wire multi_1_s_n_59;
  wire multi_1_s_n_60;
  wire multi_1_s_n_61;
  wire multi_1_s_n_62;
  wire multi_1_s_n_63;
  wire multi_1_s_n_64;
  wire multi_1_s_n_65;
  wire multi_1_s_n_66;
  wire multi_1_s_n_67;
  wire multi_1_s_n_68;
  wire multi_1_s_n_69;
  wire multi_1_s_n_70;
  wire multi_1_s_n_71;
  wire multi_1_s_n_72;
  wire multi_1_s_n_73;
  wire multi_1_s_n_74;
  wire multi_1_s_n_75;
  wire multi_1_s_n_76;
  wire multi_1_s_n_77;
  wire multi_1_s_n_78;
  wire multi_1_s_n_79;
  wire multi_1_s_n_80;
  wire multi_1_s_n_81;
  wire multi_1_s_n_82;
  wire multi_1_s_n_83;
  wire multi_1_s_n_84;
  wire multi_1_s_n_85;
  wire multi_1_s_n_86;
  wire multi_1_s_n_87;
  wire multi_1_s_n_88;
  wire multi_1_s_n_89;
  wire multi_1_s_n_90;
  wire multi_1_s_n_91;
  wire multi_1_s_n_92;
  wire multi_1_s_n_93;
  wire multi_1_s_n_94;
  wire multi_1_s_n_95;
  wire multi_1_s_n_96;
  wire multi_1_s_n_97;
  wire multi_1_s_n_98;
  wire multi_1_s_n_99;
  wire [16:0]multi_2_s_0;
  wire multi_2_s__0_n_100;
  wire multi_2_s__0_n_101;
  wire multi_2_s__0_n_102;
  wire multi_2_s__0_n_103;
  wire multi_2_s__0_n_104;
  wire multi_2_s__0_n_105;
  wire multi_2_s__0_n_79;
  wire multi_2_s__0_n_80;
  wire multi_2_s__0_n_81;
  wire multi_2_s__0_n_82;
  wire multi_2_s__0_n_83;
  wire multi_2_s__0_n_84;
  wire multi_2_s__0_n_85;
  wire multi_2_s__0_n_86;
  wire multi_2_s__0_n_87;
  wire multi_2_s__0_n_88;
  wire multi_2_s__0_n_89;
  wire multi_2_s__0_n_90;
  wire multi_2_s__0_n_91;
  wire multi_2_s__0_n_92;
  wire multi_2_s__0_n_93;
  wire multi_2_s__0_n_94;
  wire multi_2_s__0_n_95;
  wire multi_2_s__0_n_96;
  wire multi_2_s__0_n_97;
  wire multi_2_s__0_n_98;
  wire multi_2_s__0_n_99;
  wire multi_2_s_n_100;
  wire multi_2_s_n_101;
  wire multi_2_s_n_102;
  wire multi_2_s_n_103;
  wire multi_2_s_n_104;
  wire multi_2_s_n_105;
  wire multi_2_s_n_106;
  wire multi_2_s_n_107;
  wire multi_2_s_n_108;
  wire multi_2_s_n_109;
  wire multi_2_s_n_110;
  wire multi_2_s_n_111;
  wire multi_2_s_n_112;
  wire multi_2_s_n_113;
  wire multi_2_s_n_114;
  wire multi_2_s_n_115;
  wire multi_2_s_n_116;
  wire multi_2_s_n_117;
  wire multi_2_s_n_118;
  wire multi_2_s_n_119;
  wire multi_2_s_n_120;
  wire multi_2_s_n_121;
  wire multi_2_s_n_122;
  wire multi_2_s_n_123;
  wire multi_2_s_n_124;
  wire multi_2_s_n_125;
  wire multi_2_s_n_126;
  wire multi_2_s_n_127;
  wire multi_2_s_n_128;
  wire multi_2_s_n_129;
  wire multi_2_s_n_130;
  wire multi_2_s_n_131;
  wire multi_2_s_n_132;
  wire multi_2_s_n_133;
  wire multi_2_s_n_134;
  wire multi_2_s_n_135;
  wire multi_2_s_n_136;
  wire multi_2_s_n_137;
  wire multi_2_s_n_138;
  wire multi_2_s_n_139;
  wire multi_2_s_n_140;
  wire multi_2_s_n_141;
  wire multi_2_s_n_142;
  wire multi_2_s_n_143;
  wire multi_2_s_n_144;
  wire multi_2_s_n_145;
  wire multi_2_s_n_146;
  wire multi_2_s_n_147;
  wire multi_2_s_n_148;
  wire multi_2_s_n_149;
  wire multi_2_s_n_150;
  wire multi_2_s_n_151;
  wire multi_2_s_n_152;
  wire multi_2_s_n_153;
  wire multi_2_s_n_58;
  wire multi_2_s_n_59;
  wire multi_2_s_n_60;
  wire multi_2_s_n_61;
  wire multi_2_s_n_62;
  wire multi_2_s_n_63;
  wire multi_2_s_n_64;
  wire multi_2_s_n_65;
  wire multi_2_s_n_66;
  wire multi_2_s_n_67;
  wire multi_2_s_n_68;
  wire multi_2_s_n_69;
  wire multi_2_s_n_70;
  wire multi_2_s_n_71;
  wire multi_2_s_n_72;
  wire multi_2_s_n_73;
  wire multi_2_s_n_74;
  wire multi_2_s_n_75;
  wire multi_2_s_n_76;
  wire multi_2_s_n_77;
  wire multi_2_s_n_78;
  wire multi_2_s_n_79;
  wire multi_2_s_n_80;
  wire multi_2_s_n_81;
  wire multi_2_s_n_82;
  wire multi_2_s_n_83;
  wire multi_2_s_n_84;
  wire multi_2_s_n_85;
  wire multi_2_s_n_86;
  wire multi_2_s_n_87;
  wire multi_2_s_n_88;
  wire multi_2_s_n_89;
  wire multi_2_s_n_90;
  wire multi_2_s_n_91;
  wire multi_2_s_n_92;
  wire multi_2_s_n_93;
  wire multi_2_s_n_94;
  wire multi_2_s_n_95;
  wire multi_2_s_n_96;
  wire multi_2_s_n_97;
  wire multi_2_s_n_98;
  wire multi_2_s_n_99;
  wire [16:0]multi_3_s_0;
  wire multi_3_s__0_n_100;
  wire multi_3_s__0_n_101;
  wire multi_3_s__0_n_102;
  wire multi_3_s__0_n_103;
  wire multi_3_s__0_n_104;
  wire multi_3_s__0_n_105;
  wire multi_3_s__0_n_79;
  wire multi_3_s__0_n_80;
  wire multi_3_s__0_n_81;
  wire multi_3_s__0_n_82;
  wire multi_3_s__0_n_83;
  wire multi_3_s__0_n_84;
  wire multi_3_s__0_n_85;
  wire multi_3_s__0_n_86;
  wire multi_3_s__0_n_87;
  wire multi_3_s__0_n_88;
  wire multi_3_s__0_n_89;
  wire multi_3_s__0_n_90;
  wire multi_3_s__0_n_91;
  wire multi_3_s__0_n_92;
  wire multi_3_s__0_n_93;
  wire multi_3_s__0_n_94;
  wire multi_3_s__0_n_95;
  wire multi_3_s__0_n_96;
  wire multi_3_s__0_n_97;
  wire multi_3_s__0_n_98;
  wire multi_3_s__0_n_99;
  wire multi_3_s_n_100;
  wire multi_3_s_n_101;
  wire multi_3_s_n_102;
  wire multi_3_s_n_103;
  wire multi_3_s_n_104;
  wire multi_3_s_n_105;
  wire multi_3_s_n_106;
  wire multi_3_s_n_107;
  wire multi_3_s_n_108;
  wire multi_3_s_n_109;
  wire multi_3_s_n_110;
  wire multi_3_s_n_111;
  wire multi_3_s_n_112;
  wire multi_3_s_n_113;
  wire multi_3_s_n_114;
  wire multi_3_s_n_115;
  wire multi_3_s_n_116;
  wire multi_3_s_n_117;
  wire multi_3_s_n_118;
  wire multi_3_s_n_119;
  wire multi_3_s_n_120;
  wire multi_3_s_n_121;
  wire multi_3_s_n_122;
  wire multi_3_s_n_123;
  wire multi_3_s_n_124;
  wire multi_3_s_n_125;
  wire multi_3_s_n_126;
  wire multi_3_s_n_127;
  wire multi_3_s_n_128;
  wire multi_3_s_n_129;
  wire multi_3_s_n_130;
  wire multi_3_s_n_131;
  wire multi_3_s_n_132;
  wire multi_3_s_n_133;
  wire multi_3_s_n_134;
  wire multi_3_s_n_135;
  wire multi_3_s_n_136;
  wire multi_3_s_n_137;
  wire multi_3_s_n_138;
  wire multi_3_s_n_139;
  wire multi_3_s_n_140;
  wire multi_3_s_n_141;
  wire multi_3_s_n_142;
  wire multi_3_s_n_143;
  wire multi_3_s_n_144;
  wire multi_3_s_n_145;
  wire multi_3_s_n_146;
  wire multi_3_s_n_147;
  wire multi_3_s_n_148;
  wire multi_3_s_n_149;
  wire multi_3_s_n_150;
  wire multi_3_s_n_151;
  wire multi_3_s_n_152;
  wire multi_3_s_n_153;
  wire multi_3_s_n_58;
  wire multi_3_s_n_59;
  wire multi_3_s_n_60;
  wire multi_3_s_n_61;
  wire multi_3_s_n_62;
  wire multi_3_s_n_63;
  wire multi_3_s_n_64;
  wire multi_3_s_n_65;
  wire multi_3_s_n_66;
  wire multi_3_s_n_67;
  wire multi_3_s_n_68;
  wire multi_3_s_n_69;
  wire multi_3_s_n_70;
  wire multi_3_s_n_71;
  wire multi_3_s_n_72;
  wire multi_3_s_n_73;
  wire multi_3_s_n_74;
  wire multi_3_s_n_75;
  wire multi_3_s_n_76;
  wire multi_3_s_n_77;
  wire multi_3_s_n_78;
  wire multi_3_s_n_79;
  wire multi_3_s_n_80;
  wire multi_3_s_n_81;
  wire multi_3_s_n_82;
  wire multi_3_s_n_83;
  wire multi_3_s_n_84;
  wire multi_3_s_n_85;
  wire multi_3_s_n_86;
  wire multi_3_s_n_87;
  wire multi_3_s_n_88;
  wire multi_3_s_n_89;
  wire multi_3_s_n_90;
  wire multi_3_s_n_91;
  wire multi_3_s_n_92;
  wire multi_3_s_n_93;
  wire multi_3_s_n_94;
  wire multi_3_s_n_95;
  wire multi_3_s_n_96;
  wire multi_3_s_n_97;
  wire multi_3_s_n_98;
  wire multi_3_s_n_99;
  wire [23:0]q_reg;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [6:0]s00_axi_wdata;
  wire second_reg_n_0;
  wire [3:3]NLW_adder_1_s__0_carry__4_CO_UNCONNECTED;
  wire NLW_multi_1_s_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_1_s_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_1_s_OVERFLOW_UNCONNECTED;
  wire NLW_multi_1_s_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_1_s_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_1_s_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_1_s_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_1_s_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_1_s_CARRYOUT_UNCONNECTED;
  wire NLW_multi_1_s__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_1_s__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_1_s__0_OVERFLOW_UNCONNECTED;
  wire NLW_multi_1_s__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_1_s__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_1_s__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_1_s__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_1_s__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_1_s__0_CARRYOUT_UNCONNECTED;
  wire [47:27]NLW_multi_1_s__0_P_UNCONNECTED;
  wire [47:0]NLW_multi_1_s__0_PCOUT_UNCONNECTED;
  wire NLW_multi_2_s_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_2_s_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_2_s_OVERFLOW_UNCONNECTED;
  wire NLW_multi_2_s_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_2_s_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_2_s_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_2_s_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_2_s_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_2_s_CARRYOUT_UNCONNECTED;
  wire NLW_multi_2_s__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_2_s__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_2_s__0_OVERFLOW_UNCONNECTED;
  wire NLW_multi_2_s__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_2_s__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_2_s__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_2_s__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_2_s__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_2_s__0_CARRYOUT_UNCONNECTED;
  wire [47:27]NLW_multi_2_s__0_P_UNCONNECTED;
  wire [47:0]NLW_multi_2_s__0_PCOUT_UNCONNECTED;
  wire NLW_multi_3_s_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_3_s_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_3_s_OVERFLOW_UNCONNECTED;
  wire NLW_multi_3_s_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_3_s_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_3_s_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_3_s_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_3_s_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_3_s_CARRYOUT_UNCONNECTED;
  wire NLW_multi_3_s__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_multi_3_s__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_multi_3_s__0_OVERFLOW_UNCONNECTED;
  wire NLW_multi_3_s__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_multi_3_s__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_multi_3_s__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_multi_3_s__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_multi_3_s__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_multi_3_s__0_CARRYOUT_UNCONNECTED;
  wire [47:27]NLW_multi_3_s__0_P_UNCONNECTED;
  wire [47:0]NLW_multi_3_s__0_PCOUT_UNCONNECTED;

  CARRY4 adder_1_s__0_carry
       (.CI(1'b0),
        .CO({adder_1_s__0_carry_n_0,adder_1_s__0_carry_n_1,adder_1_s__0_carry_n_2,adder_1_s__0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({adder_1_s__0_carry_i_1_n_0,adder_1_s__0_carry_i_2_n_0,adder_1_s__0_carry_i_3_n_0,1'b1}),
        .O(A[3:0]),
        .S({adder_1_s__0_carry_i_4_n_0,adder_1_s__0_carry_i_5_n_0,adder_1_s__0_carry_i_6_n_0,adder_1_s__0_carry_i_7_n_0}));
  CARRY4 adder_1_s__0_carry__0
       (.CI(adder_1_s__0_carry_n_0),
        .CO({adder_1_s__0_carry__0_n_0,adder_1_s__0_carry__0_n_1,adder_1_s__0_carry__0_n_2,adder_1_s__0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({adder_1_s__0_carry__0_i_1_n_0,adder_1_s__0_carry__0_i_2_n_0,adder_1_s__0_carry__0_i_3_n_0,adder_1_s__0_carry__0_i_4_n_0}),
        .O(A[7:4]),
        .S({adder_1_s__0_carry__0_i_5_n_0,adder_1_s__0_carry__0_i_6_n_0,adder_1_s__0_carry__0_i_7_n_0,adder_1_s__0_carry__0_i_8_n_0}));
  (* HLUTNM = "lutpair5" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__0_i_1
       (.I0(multi_3_s__0_n_96),
        .I1(adder_3_s[6]),
        .I2(Q[6]),
        .O(adder_1_s__0_carry__0_i_1_n_0));
  (* HLUTNM = "lutpair4" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__0_i_2
       (.I0(multi_3_s__0_n_97),
        .I1(adder_3_s[5]),
        .I2(Q[5]),
        .O(adder_1_s__0_carry__0_i_2_n_0));
  (* HLUTNM = "lutpair3" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__0_i_3
       (.I0(multi_3_s__0_n_98),
        .I1(adder_3_s[4]),
        .I2(Q[4]),
        .O(adder_1_s__0_carry__0_i_3_n_0));
  (* HLUTNM = "lutpair2" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__0_i_4
       (.I0(multi_3_s__0_n_99),
        .I1(adder_3_s[3]),
        .I2(Q[3]),
        .O(adder_1_s__0_carry__0_i_4_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__0_i_5
       (.I0(multi_3_s__0_n_95),
        .I1(adder_3_s[7]),
        .I2(Q[7]),
        .I3(adder_1_s__0_carry__0_i_1_n_0),
        .O(adder_1_s__0_carry__0_i_5_n_0));
  (* HLUTNM = "lutpair5" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__0_i_6
       (.I0(multi_3_s__0_n_96),
        .I1(adder_3_s[6]),
        .I2(Q[6]),
        .I3(adder_1_s__0_carry__0_i_2_n_0),
        .O(adder_1_s__0_carry__0_i_6_n_0));
  (* HLUTNM = "lutpair4" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__0_i_7
       (.I0(multi_3_s__0_n_97),
        .I1(adder_3_s[5]),
        .I2(Q[5]),
        .I3(adder_1_s__0_carry__0_i_3_n_0),
        .O(adder_1_s__0_carry__0_i_7_n_0));
  (* HLUTNM = "lutpair3" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__0_i_8
       (.I0(multi_3_s__0_n_98),
        .I1(adder_3_s[4]),
        .I2(Q[4]),
        .I3(adder_1_s__0_carry__0_i_4_n_0),
        .O(adder_1_s__0_carry__0_i_8_n_0));
  CARRY4 adder_1_s__0_carry__1
       (.CI(adder_1_s__0_carry__0_n_0),
        .CO({adder_1_s__0_carry__1_n_0,adder_1_s__0_carry__1_n_1,adder_1_s__0_carry__1_n_2,adder_1_s__0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({adder_1_s__0_carry__1_i_1_n_0,adder_1_s__0_carry__1_i_2_n_0,adder_1_s__0_carry__1_i_3_n_0,adder_1_s__0_carry__1_i_4_n_0}),
        .O(A[11:8]),
        .S({adder_1_s__0_carry__1_i_5_n_0,adder_1_s__0_carry__1_i_6_n_0,adder_1_s__0_carry__1_i_7_n_0,adder_1_s__0_carry__1_i_8_n_0}));
  (* HLUTNM = "lutpair9" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__1_i_1
       (.I0(multi_3_s__0_n_92),
        .I1(adder_3_s[10]),
        .I2(Q[10]),
        .O(adder_1_s__0_carry__1_i_1_n_0));
  (* HLUTNM = "lutpair8" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__1_i_2
       (.I0(multi_3_s__0_n_93),
        .I1(adder_3_s[9]),
        .I2(Q[9]),
        .O(adder_1_s__0_carry__1_i_2_n_0));
  (* HLUTNM = "lutpair7" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__1_i_3
       (.I0(multi_3_s__0_n_94),
        .I1(adder_3_s[8]),
        .I2(Q[8]),
        .O(adder_1_s__0_carry__1_i_3_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__1_i_4
       (.I0(multi_3_s__0_n_95),
        .I1(adder_3_s[7]),
        .I2(Q[7]),
        .O(adder_1_s__0_carry__1_i_4_n_0));
  (* HLUTNM = "lutpair10" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__1_i_5
       (.I0(multi_3_s__0_n_91),
        .I1(adder_3_s[11]),
        .I2(Q[11]),
        .I3(adder_1_s__0_carry__1_i_1_n_0),
        .O(adder_1_s__0_carry__1_i_5_n_0));
  (* HLUTNM = "lutpair9" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__1_i_6
       (.I0(multi_3_s__0_n_92),
        .I1(adder_3_s[10]),
        .I2(Q[10]),
        .I3(adder_1_s__0_carry__1_i_2_n_0),
        .O(adder_1_s__0_carry__1_i_6_n_0));
  (* HLUTNM = "lutpair8" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__1_i_7
       (.I0(multi_3_s__0_n_93),
        .I1(adder_3_s[9]),
        .I2(Q[9]),
        .I3(adder_1_s__0_carry__1_i_3_n_0),
        .O(adder_1_s__0_carry__1_i_7_n_0));
  (* HLUTNM = "lutpair7" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__1_i_8
       (.I0(multi_3_s__0_n_94),
        .I1(adder_3_s[8]),
        .I2(Q[8]),
        .I3(adder_1_s__0_carry__1_i_4_n_0),
        .O(adder_1_s__0_carry__1_i_8_n_0));
  CARRY4 adder_1_s__0_carry__2
       (.CI(adder_1_s__0_carry__1_n_0),
        .CO({adder_1_s__0_carry__2_n_0,adder_1_s__0_carry__2_n_1,adder_1_s__0_carry__2_n_2,adder_1_s__0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({adder_1_s__0_carry__2_i_1_n_0,adder_1_s__0_carry__2_i_2_n_0,adder_1_s__0_carry__2_i_3_n_0,adder_1_s__0_carry__2_i_4_n_0}),
        .O(A[15:12]),
        .S({adder_1_s__0_carry__2_i_5_n_0,adder_1_s__0_carry__2_i_6_n_0,adder_1_s__0_carry__2_i_7_n_0,adder_1_s__0_carry__2_i_8_n_0}));
  (* HLUTNM = "lutpair13" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__2_i_1
       (.I0(multi_3_s__0_n_88),
        .I1(adder_3_s[14]),
        .I2(Q[14]),
        .O(adder_1_s__0_carry__2_i_1_n_0));
  (* HLUTNM = "lutpair12" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__2_i_2
       (.I0(multi_3_s__0_n_89),
        .I1(adder_3_s[13]),
        .I2(Q[13]),
        .O(adder_1_s__0_carry__2_i_2_n_0));
  (* HLUTNM = "lutpair11" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__2_i_3
       (.I0(multi_3_s__0_n_90),
        .I1(adder_3_s[12]),
        .I2(Q[12]),
        .O(adder_1_s__0_carry__2_i_3_n_0));
  (* HLUTNM = "lutpair10" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__2_i_4
       (.I0(multi_3_s__0_n_91),
        .I1(adder_3_s[11]),
        .I2(Q[11]),
        .O(adder_1_s__0_carry__2_i_4_n_0));
  (* HLUTNM = "lutpair14" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__2_i_5
       (.I0(multi_3_s__0_n_87),
        .I1(adder_3_s[15]),
        .I2(Q[15]),
        .I3(adder_1_s__0_carry__2_i_1_n_0),
        .O(adder_1_s__0_carry__2_i_5_n_0));
  (* HLUTNM = "lutpair13" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__2_i_6
       (.I0(multi_3_s__0_n_88),
        .I1(adder_3_s[14]),
        .I2(Q[14]),
        .I3(adder_1_s__0_carry__2_i_2_n_0),
        .O(adder_1_s__0_carry__2_i_6_n_0));
  (* HLUTNM = "lutpair12" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__2_i_7
       (.I0(multi_3_s__0_n_89),
        .I1(adder_3_s[13]),
        .I2(Q[13]),
        .I3(adder_1_s__0_carry__2_i_3_n_0),
        .O(adder_1_s__0_carry__2_i_7_n_0));
  (* HLUTNM = "lutpair11" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__2_i_8
       (.I0(multi_3_s__0_n_90),
        .I1(adder_3_s[12]),
        .I2(Q[12]),
        .I3(adder_1_s__0_carry__2_i_4_n_0),
        .O(adder_1_s__0_carry__2_i_8_n_0));
  CARRY4 adder_1_s__0_carry__3
       (.CI(adder_1_s__0_carry__2_n_0),
        .CO({adder_1_s__0_carry__3_n_0,adder_1_s__0_carry__3_n_1,adder_1_s__0_carry__3_n_2,adder_1_s__0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({adder_1_s__0_carry__3_i_1_n_0,adder_1_s__0_carry__3_i_2_n_0,adder_1_s__0_carry__3_i_3_n_0,adder_1_s__0_carry__3_i_4_n_0}),
        .O(A[19:16]),
        .S({adder_1_s__0_carry__3_i_5_n_0,adder_1_s__0_carry__3_i_6_n_0,adder_1_s__0_carry__3_i_7_n_0,adder_1_s__0_carry__3_i_8_n_0}));
  (* HLUTNM = "lutpair17" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__3_i_1
       (.I0(multi_3_s__0_n_84),
        .I1(adder_3_s[18]),
        .I2(Q[18]),
        .O(adder_1_s__0_carry__3_i_1_n_0));
  (* HLUTNM = "lutpair16" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__3_i_2
       (.I0(multi_3_s__0_n_85),
        .I1(adder_3_s[17]),
        .I2(Q[17]),
        .O(adder_1_s__0_carry__3_i_2_n_0));
  (* HLUTNM = "lutpair15" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__3_i_3
       (.I0(multi_3_s__0_n_86),
        .I1(adder_3_s[16]),
        .I2(Q[16]),
        .O(adder_1_s__0_carry__3_i_3_n_0));
  (* HLUTNM = "lutpair14" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__3_i_4
       (.I0(multi_3_s__0_n_87),
        .I1(adder_3_s[15]),
        .I2(Q[15]),
        .O(adder_1_s__0_carry__3_i_4_n_0));
  (* HLUTNM = "lutpair18" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__3_i_5
       (.I0(multi_3_s__0_n_83),
        .I1(adder_3_s[19]),
        .I2(Q[19]),
        .I3(adder_1_s__0_carry__3_i_1_n_0),
        .O(adder_1_s__0_carry__3_i_5_n_0));
  (* HLUTNM = "lutpair17" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__3_i_6
       (.I0(multi_3_s__0_n_84),
        .I1(adder_3_s[18]),
        .I2(Q[18]),
        .I3(adder_1_s__0_carry__3_i_2_n_0),
        .O(adder_1_s__0_carry__3_i_6_n_0));
  (* HLUTNM = "lutpair16" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__3_i_7
       (.I0(multi_3_s__0_n_85),
        .I1(adder_3_s[17]),
        .I2(Q[17]),
        .I3(adder_1_s__0_carry__3_i_3_n_0),
        .O(adder_1_s__0_carry__3_i_7_n_0));
  (* HLUTNM = "lutpair15" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__3_i_8
       (.I0(multi_3_s__0_n_86),
        .I1(adder_3_s[16]),
        .I2(Q[16]),
        .I3(adder_1_s__0_carry__3_i_4_n_0),
        .O(adder_1_s__0_carry__3_i_8_n_0));
  CARRY4 adder_1_s__0_carry__4
       (.CI(adder_1_s__0_carry__3_n_0),
        .CO({NLW_adder_1_s__0_carry__4_CO_UNCONNECTED[3],adder_1_s__0_carry__4_n_1,adder_1_s__0_carry__4_n_2,adder_1_s__0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,adder_1_s__0_carry__4_i_1_n_0,adder_1_s__0_carry__4_i_2_n_0,adder_1_s__0_carry__4_i_3_n_0}),
        .O(A[23:20]),
        .S({second_reg_n_0,adder_1_s__0_carry__4_i_5_n_0,adder_1_s__0_carry__4_i_6_n_0,adder_1_s__0_carry__4_i_7_n_0}));
  (* HLUTNM = "lutpair20" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__4_i_1
       (.I0(multi_3_s__0_n_81),
        .I1(adder_3_s[21]),
        .I2(Q[21]),
        .O(adder_1_s__0_carry__4_i_1_n_0));
  (* HLUTNM = "lutpair19" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__4_i_2
       (.I0(multi_3_s__0_n_82),
        .I1(adder_3_s[20]),
        .I2(Q[20]),
        .O(adder_1_s__0_carry__4_i_2_n_0));
  (* HLUTNM = "lutpair18" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry__4_i_3
       (.I0(multi_3_s__0_n_83),
        .I1(adder_3_s[19]),
        .I2(Q[19]),
        .O(adder_1_s__0_carry__4_i_3_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__4_i_5
       (.I0(adder_1_s__0_carry__4_i_1_n_0),
        .I1(adder_3_s[22]),
        .I2(multi_3_s__0_n_80),
        .I3(Q[22]),
        .O(adder_1_s__0_carry__4_i_5_n_0));
  (* HLUTNM = "lutpair20" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__4_i_6
       (.I0(multi_3_s__0_n_81),
        .I1(adder_3_s[21]),
        .I2(Q[21]),
        .I3(adder_1_s__0_carry__4_i_2_n_0),
        .O(adder_1_s__0_carry__4_i_6_n_0));
  (* HLUTNM = "lutpair19" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry__4_i_7
       (.I0(multi_3_s__0_n_82),
        .I1(adder_3_s[20]),
        .I2(Q[20]),
        .I3(adder_1_s__0_carry__4_i_3_n_0),
        .O(adder_1_s__0_carry__4_i_7_n_0));
  (* HLUTNM = "lutpair1" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry_i_1
       (.I0(multi_3_s__0_n_100),
        .I1(adder_3_s[2]),
        .I2(Q[2]),
        .O(adder_1_s__0_carry_i_1_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry_i_2
       (.I0(multi_3_s__0_n_101),
        .I1(adder_3_s[1]),
        .I2(Q[1]),
        .O(adder_1_s__0_carry_i_2_n_0));
  (* HLUTNM = "lutpair21" *) 
  LUT3 #(
    .INIT(8'h71)) 
    adder_1_s__0_carry_i_3
       (.I0(multi_3_s__0_n_102),
        .I1(adder_3_s[0]),
        .I2(Q[0]),
        .O(adder_1_s__0_carry_i_3_n_0));
  (* HLUTNM = "lutpair2" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry_i_4
       (.I0(multi_3_s__0_n_99),
        .I1(adder_3_s[3]),
        .I2(Q[3]),
        .I3(adder_1_s__0_carry_i_1_n_0),
        .O(adder_1_s__0_carry_i_4_n_0));
  (* HLUTNM = "lutpair1" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry_i_5
       (.I0(multi_3_s__0_n_100),
        .I1(adder_3_s[2]),
        .I2(Q[2]),
        .I3(adder_1_s__0_carry_i_2_n_0),
        .O(adder_1_s__0_carry_i_5_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    adder_1_s__0_carry_i_6
       (.I0(multi_3_s__0_n_101),
        .I1(adder_3_s[1]),
        .I2(Q[1]),
        .I3(adder_1_s__0_carry_i_3_n_0),
        .O(adder_1_s__0_carry_i_6_n_0));
  (* HLUTNM = "lutpair21" *) 
  LUT3 #(
    .INIT(8'h69)) 
    adder_1_s__0_carry_i_7
       (.I0(multi_3_s__0_n_102),
        .I1(adder_3_s[0]),
        .I2(Q[0]),
        .O(adder_1_s__0_carry_i_7_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg first_reg
       (.D(D),
        .P({multi_1_s__0_n_79,multi_1_s__0_n_80,multi_1_s__0_n_81,multi_1_s__0_n_82,multi_1_s__0_n_83,multi_1_s__0_n_84,multi_1_s__0_n_85,multi_1_s__0_n_86,multi_1_s__0_n_87,multi_1_s__0_n_88,multi_1_s__0_n_89,multi_1_s__0_n_90,multi_1_s__0_n_91,multi_1_s__0_n_92,multi_1_s__0_n_93,multi_1_s__0_n_94,multi_1_s__0_n_95,multi_1_s__0_n_96,multi_1_s__0_n_97,multi_1_s__0_n_98,multi_1_s__0_n_99,multi_1_s__0_n_100,multi_1_s__0_n_101,multi_1_s__0_n_102}),
        .out(q_reg),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_1_s
       (.A({A[23],A[23],A[23],A[23],A[23],A[23],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_1_s_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,multi_2_s_0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_1_s_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_1_s_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_1_s_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_1_s_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_1_s_OVERFLOW_UNCONNECTED),
        .P({multi_1_s_n_58,multi_1_s_n_59,multi_1_s_n_60,multi_1_s_n_61,multi_1_s_n_62,multi_1_s_n_63,multi_1_s_n_64,multi_1_s_n_65,multi_1_s_n_66,multi_1_s_n_67,multi_1_s_n_68,multi_1_s_n_69,multi_1_s_n_70,multi_1_s_n_71,multi_1_s_n_72,multi_1_s_n_73,multi_1_s_n_74,multi_1_s_n_75,multi_1_s_n_76,multi_1_s_n_77,multi_1_s_n_78,multi_1_s_n_79,multi_1_s_n_80,multi_1_s_n_81,multi_1_s_n_82,multi_1_s_n_83,multi_1_s_n_84,multi_1_s_n_85,multi_1_s_n_86,multi_1_s_n_87,multi_1_s_n_88,multi_1_s_n_89,multi_1_s_n_90,multi_1_s_n_91,multi_1_s_n_92,multi_1_s_n_93,multi_1_s_n_94,multi_1_s_n_95,multi_1_s_n_96,multi_1_s_n_97,multi_1_s_n_98,multi_1_s_n_99,multi_1_s_n_100,multi_1_s_n_101,multi_1_s_n_102,multi_1_s_n_103,multi_1_s_n_104,multi_1_s_n_105}),
        .PATTERNBDETECT(NLW_multi_1_s_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_1_s_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({multi_1_s_n_106,multi_1_s_n_107,multi_1_s_n_108,multi_1_s_n_109,multi_1_s_n_110,multi_1_s_n_111,multi_1_s_n_112,multi_1_s_n_113,multi_1_s_n_114,multi_1_s_n_115,multi_1_s_n_116,multi_1_s_n_117,multi_1_s_n_118,multi_1_s_n_119,multi_1_s_n_120,multi_1_s_n_121,multi_1_s_n_122,multi_1_s_n_123,multi_1_s_n_124,multi_1_s_n_125,multi_1_s_n_126,multi_1_s_n_127,multi_1_s_n_128,multi_1_s_n_129,multi_1_s_n_130,multi_1_s_n_131,multi_1_s_n_132,multi_1_s_n_133,multi_1_s_n_134,multi_1_s_n_135,multi_1_s_n_136,multi_1_s_n_137,multi_1_s_n_138,multi_1_s_n_139,multi_1_s_n_140,multi_1_s_n_141,multi_1_s_n_142,multi_1_s_n_143,multi_1_s_n_144,multi_1_s_n_145,multi_1_s_n_146,multi_1_s_n_147,multi_1_s_n_148,multi_1_s_n_149,multi_1_s_n_150,multi_1_s_n_151,multi_1_s_n_152,multi_1_s_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_1_s_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_1_s__0
       (.A({A[23],A[23],A[23],A[23],A[23],A[23],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_1_s__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_1_s__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_1_s__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_1_s__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(multi_1_s__0_0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_1_s__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_1_s__0_OVERFLOW_UNCONNECTED),
        .P({NLW_multi_1_s__0_P_UNCONNECTED[47:27],multi_1_s__0_n_79,multi_1_s__0_n_80,multi_1_s__0_n_81,multi_1_s__0_n_82,multi_1_s__0_n_83,multi_1_s__0_n_84,multi_1_s__0_n_85,multi_1_s__0_n_86,multi_1_s__0_n_87,multi_1_s__0_n_88,multi_1_s__0_n_89,multi_1_s__0_n_90,multi_1_s__0_n_91,multi_1_s__0_n_92,multi_1_s__0_n_93,multi_1_s__0_n_94,multi_1_s__0_n_95,multi_1_s__0_n_96,multi_1_s__0_n_97,multi_1_s__0_n_98,multi_1_s__0_n_99,multi_1_s__0_n_100,multi_1_s__0_n_101,multi_1_s__0_n_102,multi_1_s__0_n_103,multi_1_s__0_n_104,multi_1_s__0_n_105}),
        .PATTERNBDETECT(NLW_multi_1_s__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_1_s__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({multi_1_s_n_106,multi_1_s_n_107,multi_1_s_n_108,multi_1_s_n_109,multi_1_s_n_110,multi_1_s_n_111,multi_1_s_n_112,multi_1_s_n_113,multi_1_s_n_114,multi_1_s_n_115,multi_1_s_n_116,multi_1_s_n_117,multi_1_s_n_118,multi_1_s_n_119,multi_1_s_n_120,multi_1_s_n_121,multi_1_s_n_122,multi_1_s_n_123,multi_1_s_n_124,multi_1_s_n_125,multi_1_s_n_126,multi_1_s_n_127,multi_1_s_n_128,multi_1_s_n_129,multi_1_s_n_130,multi_1_s_n_131,multi_1_s_n_132,multi_1_s_n_133,multi_1_s_n_134,multi_1_s_n_135,multi_1_s_n_136,multi_1_s_n_137,multi_1_s_n_138,multi_1_s_n_139,multi_1_s_n_140,multi_1_s_n_141,multi_1_s_n_142,multi_1_s_n_143,multi_1_s_n_144,multi_1_s_n_145,multi_1_s_n_146,multi_1_s_n_147,multi_1_s_n_148,multi_1_s_n_149,multi_1_s_n_150,multi_1_s_n_151,multi_1_s_n_152,multi_1_s_n_153}),
        .PCOUT(NLW_multi_1_s__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(SR),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_1_s__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_2_s
       (.A({q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_2_s_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,multi_2_s_0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_2_s_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_2_s_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_2_s_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_2_s_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_2_s_OVERFLOW_UNCONNECTED),
        .P({multi_2_s_n_58,multi_2_s_n_59,multi_2_s_n_60,multi_2_s_n_61,multi_2_s_n_62,multi_2_s_n_63,multi_2_s_n_64,multi_2_s_n_65,multi_2_s_n_66,multi_2_s_n_67,multi_2_s_n_68,multi_2_s_n_69,multi_2_s_n_70,multi_2_s_n_71,multi_2_s_n_72,multi_2_s_n_73,multi_2_s_n_74,multi_2_s_n_75,multi_2_s_n_76,multi_2_s_n_77,multi_2_s_n_78,multi_2_s_n_79,multi_2_s_n_80,multi_2_s_n_81,multi_2_s_n_82,multi_2_s_n_83,multi_2_s_n_84,multi_2_s_n_85,multi_2_s_n_86,multi_2_s_n_87,multi_2_s_n_88,multi_2_s_n_89,multi_2_s_n_90,multi_2_s_n_91,multi_2_s_n_92,multi_2_s_n_93,multi_2_s_n_94,multi_2_s_n_95,multi_2_s_n_96,multi_2_s_n_97,multi_2_s_n_98,multi_2_s_n_99,multi_2_s_n_100,multi_2_s_n_101,multi_2_s_n_102,multi_2_s_n_103,multi_2_s_n_104,multi_2_s_n_105}),
        .PATTERNBDETECT(NLW_multi_2_s_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_2_s_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({multi_2_s_n_106,multi_2_s_n_107,multi_2_s_n_108,multi_2_s_n_109,multi_2_s_n_110,multi_2_s_n_111,multi_2_s_n_112,multi_2_s_n_113,multi_2_s_n_114,multi_2_s_n_115,multi_2_s_n_116,multi_2_s_n_117,multi_2_s_n_118,multi_2_s_n_119,multi_2_s_n_120,multi_2_s_n_121,multi_2_s_n_122,multi_2_s_n_123,multi_2_s_n_124,multi_2_s_n_125,multi_2_s_n_126,multi_2_s_n_127,multi_2_s_n_128,multi_2_s_n_129,multi_2_s_n_130,multi_2_s_n_131,multi_2_s_n_132,multi_2_s_n_133,multi_2_s_n_134,multi_2_s_n_135,multi_2_s_n_136,multi_2_s_n_137,multi_2_s_n_138,multi_2_s_n_139,multi_2_s_n_140,multi_2_s_n_141,multi_2_s_n_142,multi_2_s_n_143,multi_2_s_n_144,multi_2_s_n_145,multi_2_s_n_146,multi_2_s_n_147,multi_2_s_n_148,multi_2_s_n_149,multi_2_s_n_150,multi_2_s_n_151,multi_2_s_n_152,multi_2_s_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_2_s_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_2_s__0
       (.A({q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_2_s__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_2_s__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_2_s__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_2_s__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(multi_1_s__0_0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_2_s__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_2_s__0_OVERFLOW_UNCONNECTED),
        .P({NLW_multi_2_s__0_P_UNCONNECTED[47:27],multi_2_s__0_n_79,multi_2_s__0_n_80,multi_2_s__0_n_81,multi_2_s__0_n_82,multi_2_s__0_n_83,multi_2_s__0_n_84,multi_2_s__0_n_85,multi_2_s__0_n_86,multi_2_s__0_n_87,multi_2_s__0_n_88,multi_2_s__0_n_89,multi_2_s__0_n_90,multi_2_s__0_n_91,multi_2_s__0_n_92,multi_2_s__0_n_93,multi_2_s__0_n_94,multi_2_s__0_n_95,multi_2_s__0_n_96,multi_2_s__0_n_97,multi_2_s__0_n_98,multi_2_s__0_n_99,multi_2_s__0_n_100,multi_2_s__0_n_101,multi_2_s__0_n_102,multi_2_s__0_n_103,multi_2_s__0_n_104,multi_2_s__0_n_105}),
        .PATTERNBDETECT(NLW_multi_2_s__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_2_s__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({multi_2_s_n_106,multi_2_s_n_107,multi_2_s_n_108,multi_2_s_n_109,multi_2_s_n_110,multi_2_s_n_111,multi_2_s_n_112,multi_2_s_n_113,multi_2_s_n_114,multi_2_s_n_115,multi_2_s_n_116,multi_2_s_n_117,multi_2_s_n_118,multi_2_s_n_119,multi_2_s_n_120,multi_2_s_n_121,multi_2_s_n_122,multi_2_s_n_123,multi_2_s_n_124,multi_2_s_n_125,multi_2_s_n_126,multi_2_s_n_127,multi_2_s_n_128,multi_2_s_n_129,multi_2_s_n_130,multi_2_s_n_131,multi_2_s_n_132,multi_2_s_n_133,multi_2_s_n_134,multi_2_s_n_135,multi_2_s_n_136,multi_2_s_n_137,multi_2_s_n_138,multi_2_s_n_139,multi_2_s_n_140,multi_2_s_n_141,multi_2_s_n_142,multi_2_s_n_143,multi_2_s_n_144,multi_2_s_n_145,multi_2_s_n_146,multi_2_s_n_147,multi_2_s_n_148,multi_2_s_n_149,multi_2_s_n_150,multi_2_s_n_151,multi_2_s_n_152,multi_2_s_n_153}),
        .PCOUT(NLW_multi_2_s__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(SR),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_2_s__0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_3_s
       (.A({q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_3_s_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,multi_3_s_0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_3_s_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_3_s_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_3_s_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_3_s_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_3_s_OVERFLOW_UNCONNECTED),
        .P({multi_3_s_n_58,multi_3_s_n_59,multi_3_s_n_60,multi_3_s_n_61,multi_3_s_n_62,multi_3_s_n_63,multi_3_s_n_64,multi_3_s_n_65,multi_3_s_n_66,multi_3_s_n_67,multi_3_s_n_68,multi_3_s_n_69,multi_3_s_n_70,multi_3_s_n_71,multi_3_s_n_72,multi_3_s_n_73,multi_3_s_n_74,multi_3_s_n_75,multi_3_s_n_76,multi_3_s_n_77,multi_3_s_n_78,multi_3_s_n_79,multi_3_s_n_80,multi_3_s_n_81,multi_3_s_n_82,multi_3_s_n_83,multi_3_s_n_84,multi_3_s_n_85,multi_3_s_n_86,multi_3_s_n_87,multi_3_s_n_88,multi_3_s_n_89,multi_3_s_n_90,multi_3_s_n_91,multi_3_s_n_92,multi_3_s_n_93,multi_3_s_n_94,multi_3_s_n_95,multi_3_s_n_96,multi_3_s_n_97,multi_3_s_n_98,multi_3_s_n_99,multi_3_s_n_100,multi_3_s_n_101,multi_3_s_n_102,multi_3_s_n_103,multi_3_s_n_104,multi_3_s_n_105}),
        .PATTERNBDETECT(NLW_multi_3_s_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_3_s_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({multi_3_s_n_106,multi_3_s_n_107,multi_3_s_n_108,multi_3_s_n_109,multi_3_s_n_110,multi_3_s_n_111,multi_3_s_n_112,multi_3_s_n_113,multi_3_s_n_114,multi_3_s_n_115,multi_3_s_n_116,multi_3_s_n_117,multi_3_s_n_118,multi_3_s_n_119,multi_3_s_n_120,multi_3_s_n_121,multi_3_s_n_122,multi_3_s_n_123,multi_3_s_n_124,multi_3_s_n_125,multi_3_s_n_126,multi_3_s_n_127,multi_3_s_n_128,multi_3_s_n_129,multi_3_s_n_130,multi_3_s_n_131,multi_3_s_n_132,multi_3_s_n_133,multi_3_s_n_134,multi_3_s_n_135,multi_3_s_n_136,multi_3_s_n_137,multi_3_s_n_138,multi_3_s_n_139,multi_3_s_n_140,multi_3_s_n_141,multi_3_s_n_142,multi_3_s_n_143,multi_3_s_n_144,multi_3_s_n_145,multi_3_s_n_146,multi_3_s_n_147,multi_3_s_n_148,multi_3_s_n_149,multi_3_s_n_150,multi_3_s_n_151,multi_3_s_n_152,multi_3_s_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_3_s_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    multi_3_s__0
       (.A({q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg[23],q_reg}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_multi_3_s__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata[6],s00_axi_wdata}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_multi_3_s__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_multi_3_s__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_multi_3_s__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(E),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_multi_3_s__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_multi_3_s__0_OVERFLOW_UNCONNECTED),
        .P({NLW_multi_3_s__0_P_UNCONNECTED[47:27],multi_3_s__0_n_79,multi_3_s__0_n_80,multi_3_s__0_n_81,multi_3_s__0_n_82,multi_3_s__0_n_83,multi_3_s__0_n_84,multi_3_s__0_n_85,multi_3_s__0_n_86,multi_3_s__0_n_87,multi_3_s__0_n_88,multi_3_s__0_n_89,multi_3_s__0_n_90,multi_3_s__0_n_91,multi_3_s__0_n_92,multi_3_s__0_n_93,multi_3_s__0_n_94,multi_3_s__0_n_95,multi_3_s__0_n_96,multi_3_s__0_n_97,multi_3_s__0_n_98,multi_3_s__0_n_99,multi_3_s__0_n_100,multi_3_s__0_n_101,multi_3_s__0_n_102,multi_3_s__0_n_103,multi_3_s__0_n_104,multi_3_s__0_n_105}),
        .PATTERNBDETECT(NLW_multi_3_s__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_multi_3_s__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({multi_3_s_n_106,multi_3_s_n_107,multi_3_s_n_108,multi_3_s_n_109,multi_3_s_n_110,multi_3_s_n_111,multi_3_s_n_112,multi_3_s_n_113,multi_3_s_n_114,multi_3_s_n_115,multi_3_s_n_116,multi_3_s_n_117,multi_3_s_n_118,multi_3_s_n_119,multi_3_s_n_120,multi_3_s_n_121,multi_3_s_n_122,multi_3_s_n_123,multi_3_s_n_124,multi_3_s_n_125,multi_3_s_n_126,multi_3_s_n_127,multi_3_s_n_128,multi_3_s_n_129,multi_3_s_n_130,multi_3_s_n_131,multi_3_s_n_132,multi_3_s_n_133,multi_3_s_n_134,multi_3_s_n_135,multi_3_s_n_136,multi_3_s_n_137,multi_3_s_n_138,multi_3_s_n_139,multi_3_s_n_140,multi_3_s_n_141,multi_3_s_n_142,multi_3_s_n_143,multi_3_s_n_144,multi_3_s_n_145,multi_3_s_n_146,multi_3_s_n_147,multi_3_s_n_148,multi_3_s_n_149,multi_3_s_n_150,multi_3_s_n_151,multi_3_s_n_152,multi_3_s_n_153}),
        .PCOUT(NLW_multi_3_s__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(SR),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_multi_3_s__0_UNDERFLOW_UNCONNECTED));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0 second_reg
       (.P({multi_3_s__0_n_79,multi_3_s__0_n_80}),
        .Q(Q[23:22]),
        .S(second_reg_n_0),
        .multi_2_s__0(adder_3_s),
        .\q_reg[23]_0 ({multi_2_s__0_n_79,multi_2_s__0_n_80,multi_2_s__0_n_81,multi_2_s__0_n_82,multi_2_s__0_n_83,multi_2_s__0_n_84,multi_2_s__0_n_85,multi_2_s__0_n_86,multi_2_s__0_n_87,multi_2_s__0_n_88,multi_2_s__0_n_89,multi_2_s__0_n_90,multi_2_s__0_n_91,multi_2_s__0_n_92,multi_2_s__0_n_93,multi_2_s__0_n_94,multi_2_s__0_n_95,multi_2_s__0_n_96,multi_2_s__0_n_97,multi_2_s__0_n_98,multi_2_s__0_n_99,multi_2_s__0_n_100,multi_2_s__0_n_101,multi_2_s__0_n_102}),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg
   (out,
    D,
    P,
    s00_axi_aclk,
    s00_axi_aresetn);
  output [23:0]out;
  output [23:0]D;
  input [23:0]P;
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire [23:0]D;
  wire [23:0]P;
  wire [23:0]out;
  wire \q[0]_i_1_n_0 ;
  wire \q[12]_i_2__0_n_0 ;
  wire \q[12]_i_3__0_n_0 ;
  wire \q[12]_i_4__0_n_0 ;
  wire \q[12]_i_5__0_n_0 ;
  wire \q[16]_i_2__0_n_0 ;
  wire \q[16]_i_3__0_n_0 ;
  wire \q[16]_i_4__0_n_0 ;
  wire \q[16]_i_5__0_n_0 ;
  wire \q[20]_i_2__0_n_0 ;
  wire \q[20]_i_3__0_n_0 ;
  wire \q[20]_i_4__0_n_0 ;
  wire \q[20]_i_5__0_n_0 ;
  wire \q[4]_i_2__0_n_0 ;
  wire \q[4]_i_3__0_n_0 ;
  wire \q[4]_i_4__0_n_0 ;
  wire \q[4]_i_5__0_n_0 ;
  wire \q[8]_i_2__0_n_0 ;
  wire \q[8]_i_3__0_n_0 ;
  wire \q[8]_i_4__0_n_0 ;
  wire \q[8]_i_5__0_n_0 ;
  wire \q_reg[12]_i_1__0_n_0 ;
  wire \q_reg[12]_i_1__0_n_1 ;
  wire \q_reg[12]_i_1__0_n_2 ;
  wire \q_reg[12]_i_1__0_n_3 ;
  wire \q_reg[12]_i_1__0_n_4 ;
  wire \q_reg[12]_i_1__0_n_5 ;
  wire \q_reg[12]_i_1__0_n_6 ;
  wire \q_reg[12]_i_1__0_n_7 ;
  wire \q_reg[16]_i_1__0_n_0 ;
  wire \q_reg[16]_i_1__0_n_1 ;
  wire \q_reg[16]_i_1__0_n_2 ;
  wire \q_reg[16]_i_1__0_n_3 ;
  wire \q_reg[16]_i_1__0_n_4 ;
  wire \q_reg[16]_i_1__0_n_5 ;
  wire \q_reg[16]_i_1__0_n_6 ;
  wire \q_reg[16]_i_1__0_n_7 ;
  wire \q_reg[20]_i_1__0_n_1 ;
  wire \q_reg[20]_i_1__0_n_2 ;
  wire \q_reg[20]_i_1__0_n_3 ;
  wire \q_reg[20]_i_1__0_n_4 ;
  wire \q_reg[20]_i_1__0_n_5 ;
  wire \q_reg[20]_i_1__0_n_6 ;
  wire \q_reg[20]_i_1__0_n_7 ;
  wire \q_reg[4]_i_1__0_n_0 ;
  wire \q_reg[4]_i_1__0_n_1 ;
  wire \q_reg[4]_i_1__0_n_2 ;
  wire \q_reg[4]_i_1__0_n_3 ;
  wire \q_reg[4]_i_1__0_n_4 ;
  wire \q_reg[4]_i_1__0_n_5 ;
  wire \q_reg[4]_i_1__0_n_6 ;
  wire \q_reg[4]_i_1__0_n_7 ;
  wire \q_reg[8]_i_1__0_n_0 ;
  wire \q_reg[8]_i_1__0_n_1 ;
  wire \q_reg[8]_i_1__0_n_2 ;
  wire \q_reg[8]_i_1__0_n_3 ;
  wire \q_reg[8]_i_1__0_n_4 ;
  wire \q_reg[8]_i_1__0_n_5 ;
  wire \q_reg[8]_i_1__0_n_6 ;
  wire \q_reg[8]_i_1__0_n_7 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire \slv_reg5[0]_i_2_n_0 ;
  wire \slv_reg5[0]_i_3_n_0 ;
  wire \slv_reg5[0]_i_4_n_0 ;
  wire \slv_reg5[0]_i_5_n_0 ;
  wire \slv_reg5[11]_i_2_n_0 ;
  wire \slv_reg5[11]_i_3_n_0 ;
  wire \slv_reg5[11]_i_4_n_0 ;
  wire \slv_reg5[11]_i_5_n_0 ;
  wire \slv_reg5[15]_i_2_n_0 ;
  wire \slv_reg5[15]_i_3_n_0 ;
  wire \slv_reg5[15]_i_4_n_0 ;
  wire \slv_reg5[15]_i_5_n_0 ;
  wire \slv_reg5[19]_i_2_n_0 ;
  wire \slv_reg5[19]_i_3_n_0 ;
  wire \slv_reg5[19]_i_4_n_0 ;
  wire \slv_reg5[19]_i_5_n_0 ;
  wire \slv_reg5[23]_i_2_n_0 ;
  wire \slv_reg5[23]_i_3_n_0 ;
  wire \slv_reg5[23]_i_4_n_0 ;
  wire \slv_reg5[23]_i_5_n_0 ;
  wire \slv_reg5[3]_i_2_n_0 ;
  wire \slv_reg5[3]_i_3_n_0 ;
  wire \slv_reg5[3]_i_4_n_0 ;
  wire \slv_reg5[3]_i_5_n_0 ;
  wire \slv_reg5[7]_i_2_n_0 ;
  wire \slv_reg5[7]_i_3_n_0 ;
  wire \slv_reg5[7]_i_4_n_0 ;
  wire \slv_reg5[7]_i_5_n_0 ;
  wire \slv_reg5_reg[0]_i_1_n_0 ;
  wire \slv_reg5_reg[0]_i_1_n_1 ;
  wire \slv_reg5_reg[0]_i_1_n_2 ;
  wire \slv_reg5_reg[0]_i_1_n_3 ;
  wire \slv_reg5_reg[0]_i_1_n_4 ;
  wire \slv_reg5_reg[0]_i_1_n_5 ;
  wire \slv_reg5_reg[0]_i_1_n_6 ;
  wire \slv_reg5_reg[11]_i_1_n_0 ;
  wire \slv_reg5_reg[11]_i_1_n_1 ;
  wire \slv_reg5_reg[11]_i_1_n_2 ;
  wire \slv_reg5_reg[11]_i_1_n_3 ;
  wire \slv_reg5_reg[15]_i_1_n_0 ;
  wire \slv_reg5_reg[15]_i_1_n_1 ;
  wire \slv_reg5_reg[15]_i_1_n_2 ;
  wire \slv_reg5_reg[15]_i_1_n_3 ;
  wire \slv_reg5_reg[19]_i_1_n_0 ;
  wire \slv_reg5_reg[19]_i_1_n_1 ;
  wire \slv_reg5_reg[19]_i_1_n_2 ;
  wire \slv_reg5_reg[19]_i_1_n_3 ;
  wire \slv_reg5_reg[23]_i_1_n_1 ;
  wire \slv_reg5_reg[23]_i_1_n_2 ;
  wire \slv_reg5_reg[23]_i_1_n_3 ;
  wire \slv_reg5_reg[3]_i_1_n_0 ;
  wire \slv_reg5_reg[3]_i_1_n_1 ;
  wire \slv_reg5_reg[3]_i_1_n_2 ;
  wire \slv_reg5_reg[3]_i_1_n_3 ;
  wire \slv_reg5_reg[7]_i_1_n_0 ;
  wire \slv_reg5_reg[7]_i_1_n_1 ;
  wire \slv_reg5_reg[7]_i_1_n_2 ;
  wire \slv_reg5_reg[7]_i_1_n_3 ;
  wire [3:3]\NLW_q_reg[20]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_slv_reg5_reg[23]_i_1_CO_UNCONNECTED ;
  wire [0:0]\NLW_slv_reg5_reg[3]_i_1_O_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    \q[0]_i_1 
       (.I0(P[0]),
        .I1(out[0]),
        .O(\q[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_2__0 
       (.I0(P[15]),
        .I1(out[15]),
        .O(\q[12]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_3__0 
       (.I0(P[14]),
        .I1(out[14]),
        .O(\q[12]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_4__0 
       (.I0(P[13]),
        .I1(out[13]),
        .O(\q[12]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_5__0 
       (.I0(P[12]),
        .I1(out[12]),
        .O(\q[12]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_2__0 
       (.I0(P[19]),
        .I1(out[19]),
        .O(\q[16]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_3__0 
       (.I0(P[18]),
        .I1(out[18]),
        .O(\q[16]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_4__0 
       (.I0(P[17]),
        .I1(out[17]),
        .O(\q[16]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_5__0 
       (.I0(P[16]),
        .I1(out[16]),
        .O(\q[16]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_2__0 
       (.I0(P[23]),
        .I1(out[23]),
        .O(\q[20]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_3__0 
       (.I0(P[22]),
        .I1(out[22]),
        .O(\q[20]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_4__0 
       (.I0(P[21]),
        .I1(out[21]),
        .O(\q[20]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_5__0 
       (.I0(P[20]),
        .I1(out[20]),
        .O(\q[20]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_2__0 
       (.I0(P[7]),
        .I1(out[7]),
        .O(\q[4]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_3__0 
       (.I0(P[6]),
        .I1(out[6]),
        .O(\q[4]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_4__0 
       (.I0(P[5]),
        .I1(out[5]),
        .O(\q[4]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_5__0 
       (.I0(P[4]),
        .I1(out[4]),
        .O(\q[4]_i_5__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_2__0 
       (.I0(P[11]),
        .I1(out[11]),
        .O(\q[8]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_3__0 
       (.I0(P[10]),
        .I1(out[10]),
        .O(\q[8]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_4__0 
       (.I0(P[9]),
        .I1(out[9]),
        .O(\q[8]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_5__0 
       (.I0(P[8]),
        .I1(out[8]),
        .O(\q[8]_i_5__0_n_0 ));
  FDCE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q[0]_i_1_n_0 ),
        .Q(out[0]));
  FDCE \q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1__0_n_5 ),
        .Q(out[10]));
  FDCE \q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1__0_n_4 ),
        .Q(out[11]));
  FDCE \q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1__0_n_7 ),
        .Q(out[12]));
  CARRY4 \q_reg[12]_i_1__0 
       (.CI(\q_reg[8]_i_1__0_n_0 ),
        .CO({\q_reg[12]_i_1__0_n_0 ,\q_reg[12]_i_1__0_n_1 ,\q_reg[12]_i_1__0_n_2 ,\q_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(P[15:12]),
        .O({\q_reg[12]_i_1__0_n_4 ,\q_reg[12]_i_1__0_n_5 ,\q_reg[12]_i_1__0_n_6 ,\q_reg[12]_i_1__0_n_7 }),
        .S({\q[12]_i_2__0_n_0 ,\q[12]_i_3__0_n_0 ,\q[12]_i_4__0_n_0 ,\q[12]_i_5__0_n_0 }));
  FDCE \q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1__0_n_6 ),
        .Q(out[13]));
  FDCE \q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1__0_n_5 ),
        .Q(out[14]));
  FDCE \q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1__0_n_4 ),
        .Q(out[15]));
  FDCE \q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1__0_n_7 ),
        .Q(out[16]));
  CARRY4 \q_reg[16]_i_1__0 
       (.CI(\q_reg[12]_i_1__0_n_0 ),
        .CO({\q_reg[16]_i_1__0_n_0 ,\q_reg[16]_i_1__0_n_1 ,\q_reg[16]_i_1__0_n_2 ,\q_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(P[19:16]),
        .O({\q_reg[16]_i_1__0_n_4 ,\q_reg[16]_i_1__0_n_5 ,\q_reg[16]_i_1__0_n_6 ,\q_reg[16]_i_1__0_n_7 }),
        .S({\q[16]_i_2__0_n_0 ,\q[16]_i_3__0_n_0 ,\q[16]_i_4__0_n_0 ,\q[16]_i_5__0_n_0 }));
  FDCE \q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1__0_n_6 ),
        .Q(out[17]));
  FDCE \q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1__0_n_5 ),
        .Q(out[18]));
  FDCE \q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1__0_n_4 ),
        .Q(out[19]));
  FDCE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\slv_reg5_reg[0]_i_1_n_6 ),
        .Q(out[1]));
  FDCE \q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1__0_n_7 ),
        .Q(out[20]));
  CARRY4 \q_reg[20]_i_1__0 
       (.CI(\q_reg[16]_i_1__0_n_0 ),
        .CO({\NLW_q_reg[20]_i_1__0_CO_UNCONNECTED [3],\q_reg[20]_i_1__0_n_1 ,\q_reg[20]_i_1__0_n_2 ,\q_reg[20]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,P[22:20]}),
        .O({\q_reg[20]_i_1__0_n_4 ,\q_reg[20]_i_1__0_n_5 ,\q_reg[20]_i_1__0_n_6 ,\q_reg[20]_i_1__0_n_7 }),
        .S({\q[20]_i_2__0_n_0 ,\q[20]_i_3__0_n_0 ,\q[20]_i_4__0_n_0 ,\q[20]_i_5__0_n_0 }));
  FDCE \q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1__0_n_6 ),
        .Q(out[21]));
  FDCE \q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1__0_n_5 ),
        .Q(out[22]));
  FDCE \q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1__0_n_4 ),
        .Q(out[23]));
  FDCE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\slv_reg5_reg[0]_i_1_n_5 ),
        .Q(out[2]));
  FDCE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\slv_reg5_reg[0]_i_1_n_4 ),
        .Q(out[3]));
  FDCE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1__0_n_7 ),
        .Q(out[4]));
  CARRY4 \q_reg[4]_i_1__0 
       (.CI(\slv_reg5_reg[0]_i_1_n_0 ),
        .CO({\q_reg[4]_i_1__0_n_0 ,\q_reg[4]_i_1__0_n_1 ,\q_reg[4]_i_1__0_n_2 ,\q_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(P[7:4]),
        .O({\q_reg[4]_i_1__0_n_4 ,\q_reg[4]_i_1__0_n_5 ,\q_reg[4]_i_1__0_n_6 ,\q_reg[4]_i_1__0_n_7 }),
        .S({\q[4]_i_2__0_n_0 ,\q[4]_i_3__0_n_0 ,\q[4]_i_4__0_n_0 ,\q[4]_i_5__0_n_0 }));
  FDCE \q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1__0_n_6 ),
        .Q(out[5]));
  FDCE \q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1__0_n_5 ),
        .Q(out[6]));
  FDCE \q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1__0_n_4 ),
        .Q(out[7]));
  FDCE \q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1__0_n_7 ),
        .Q(out[8]));
  CARRY4 \q_reg[8]_i_1__0 
       (.CI(\q_reg[4]_i_1__0_n_0 ),
        .CO({\q_reg[8]_i_1__0_n_0 ,\q_reg[8]_i_1__0_n_1 ,\q_reg[8]_i_1__0_n_2 ,\q_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(P[11:8]),
        .O({\q_reg[8]_i_1__0_n_4 ,\q_reg[8]_i_1__0_n_5 ,\q_reg[8]_i_1__0_n_6 ,\q_reg[8]_i_1__0_n_7 }),
        .S({\q[8]_i_2__0_n_0 ,\q[8]_i_3__0_n_0 ,\q[8]_i_4__0_n_0 ,\q[8]_i_5__0_n_0 }));
  FDCE \q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1__0_n_6 ),
        .Q(out[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[0]_i_2 
       (.I0(P[3]),
        .I1(out[3]),
        .O(\slv_reg5[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[0]_i_3 
       (.I0(P[2]),
        .I1(out[2]),
        .O(\slv_reg5[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[0]_i_4 
       (.I0(P[1]),
        .I1(out[1]),
        .O(\slv_reg5[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[0]_i_5 
       (.I0(P[0]),
        .I1(out[0]),
        .O(\slv_reg5[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[11]_i_2 
       (.I0(P[11]),
        .I1(out[11]),
        .O(\slv_reg5[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[11]_i_3 
       (.I0(P[10]),
        .I1(out[10]),
        .O(\slv_reg5[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[11]_i_4 
       (.I0(P[9]),
        .I1(out[9]),
        .O(\slv_reg5[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[11]_i_5 
       (.I0(P[8]),
        .I1(out[8]),
        .O(\slv_reg5[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[15]_i_2 
       (.I0(P[15]),
        .I1(out[15]),
        .O(\slv_reg5[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[15]_i_3 
       (.I0(P[14]),
        .I1(out[14]),
        .O(\slv_reg5[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[15]_i_4 
       (.I0(P[13]),
        .I1(out[13]),
        .O(\slv_reg5[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[15]_i_5 
       (.I0(P[12]),
        .I1(out[12]),
        .O(\slv_reg5[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[19]_i_2 
       (.I0(P[19]),
        .I1(out[19]),
        .O(\slv_reg5[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[19]_i_3 
       (.I0(P[18]),
        .I1(out[18]),
        .O(\slv_reg5[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[19]_i_4 
       (.I0(P[17]),
        .I1(out[17]),
        .O(\slv_reg5[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[19]_i_5 
       (.I0(P[16]),
        .I1(out[16]),
        .O(\slv_reg5[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[23]_i_2 
       (.I0(P[23]),
        .I1(out[23]),
        .O(\slv_reg5[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[23]_i_3 
       (.I0(P[22]),
        .I1(out[22]),
        .O(\slv_reg5[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[23]_i_4 
       (.I0(P[21]),
        .I1(out[21]),
        .O(\slv_reg5[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[23]_i_5 
       (.I0(P[20]),
        .I1(out[20]),
        .O(\slv_reg5[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[3]_i_2 
       (.I0(P[3]),
        .I1(out[3]),
        .O(\slv_reg5[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[3]_i_3 
       (.I0(P[2]),
        .I1(out[2]),
        .O(\slv_reg5[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[3]_i_4 
       (.I0(P[1]),
        .I1(out[1]),
        .O(\slv_reg5[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[3]_i_5 
       (.I0(P[0]),
        .I1(out[0]),
        .O(\slv_reg5[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[7]_i_2 
       (.I0(P[7]),
        .I1(out[7]),
        .O(\slv_reg5[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[7]_i_3 
       (.I0(P[6]),
        .I1(out[6]),
        .O(\slv_reg5[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[7]_i_4 
       (.I0(P[5]),
        .I1(out[5]),
        .O(\slv_reg5[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \slv_reg5[7]_i_5 
       (.I0(P[4]),
        .I1(out[4]),
        .O(\slv_reg5[7]_i_5_n_0 ));
  CARRY4 \slv_reg5_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\slv_reg5_reg[0]_i_1_n_0 ,\slv_reg5_reg[0]_i_1_n_1 ,\slv_reg5_reg[0]_i_1_n_2 ,\slv_reg5_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[3:0]),
        .O({\slv_reg5_reg[0]_i_1_n_4 ,\slv_reg5_reg[0]_i_1_n_5 ,\slv_reg5_reg[0]_i_1_n_6 ,D[0]}),
        .S({\slv_reg5[0]_i_2_n_0 ,\slv_reg5[0]_i_3_n_0 ,\slv_reg5[0]_i_4_n_0 ,\slv_reg5[0]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[11]_i_1 
       (.CI(\slv_reg5_reg[7]_i_1_n_0 ),
        .CO({\slv_reg5_reg[11]_i_1_n_0 ,\slv_reg5_reg[11]_i_1_n_1 ,\slv_reg5_reg[11]_i_1_n_2 ,\slv_reg5_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[11:8]),
        .O(D[11:8]),
        .S({\slv_reg5[11]_i_2_n_0 ,\slv_reg5[11]_i_3_n_0 ,\slv_reg5[11]_i_4_n_0 ,\slv_reg5[11]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[15]_i_1 
       (.CI(\slv_reg5_reg[11]_i_1_n_0 ),
        .CO({\slv_reg5_reg[15]_i_1_n_0 ,\slv_reg5_reg[15]_i_1_n_1 ,\slv_reg5_reg[15]_i_1_n_2 ,\slv_reg5_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[15:12]),
        .O(D[15:12]),
        .S({\slv_reg5[15]_i_2_n_0 ,\slv_reg5[15]_i_3_n_0 ,\slv_reg5[15]_i_4_n_0 ,\slv_reg5[15]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[19]_i_1 
       (.CI(\slv_reg5_reg[15]_i_1_n_0 ),
        .CO({\slv_reg5_reg[19]_i_1_n_0 ,\slv_reg5_reg[19]_i_1_n_1 ,\slv_reg5_reg[19]_i_1_n_2 ,\slv_reg5_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[19:16]),
        .O(D[19:16]),
        .S({\slv_reg5[19]_i_2_n_0 ,\slv_reg5[19]_i_3_n_0 ,\slv_reg5[19]_i_4_n_0 ,\slv_reg5[19]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[23]_i_1 
       (.CI(\slv_reg5_reg[19]_i_1_n_0 ),
        .CO({\NLW_slv_reg5_reg[23]_i_1_CO_UNCONNECTED [3],\slv_reg5_reg[23]_i_1_n_1 ,\slv_reg5_reg[23]_i_1_n_2 ,\slv_reg5_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,P[22:20]}),
        .O(D[23:20]),
        .S({\slv_reg5[23]_i_2_n_0 ,\slv_reg5[23]_i_3_n_0 ,\slv_reg5[23]_i_4_n_0 ,\slv_reg5[23]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\slv_reg5_reg[3]_i_1_n_0 ,\slv_reg5_reg[3]_i_1_n_1 ,\slv_reg5_reg[3]_i_1_n_2 ,\slv_reg5_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[3:0]),
        .O({D[3:1],\NLW_slv_reg5_reg[3]_i_1_O_UNCONNECTED [0]}),
        .S({\slv_reg5[3]_i_2_n_0 ,\slv_reg5[3]_i_3_n_0 ,\slv_reg5[3]_i_4_n_0 ,\slv_reg5[3]_i_5_n_0 }));
  CARRY4 \slv_reg5_reg[7]_i_1 
       (.CI(\slv_reg5_reg[3]_i_1_n_0 ),
        .CO({\slv_reg5_reg[7]_i_1_n_0 ,\slv_reg5_reg[7]_i_1_n_1 ,\slv_reg5_reg[7]_i_1_n_2 ,\slv_reg5_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(P[7:4]),
        .O(D[7:4]),
        .S({\slv_reg5[7]_i_2_n_0 ,\slv_reg5[7]_i_3_n_0 ,\slv_reg5[7]_i_4_n_0 ,\slv_reg5[7]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_0
   (S,
    multi_2_s__0,
    Q,
    P,
    \q_reg[23]_0 ,
    s00_axi_aclk,
    s00_axi_aresetn);
  output [0:0]S;
  output [22:0]multi_2_s__0;
  input [1:0]Q;
  input [1:0]P;
  input [23:0]\q_reg[23]_0 ;
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire [1:0]P;
  wire [1:0]Q;
  wire [0:0]S;
  wire adder_1_s__0_carry__0_i_10_n_0;
  wire adder_1_s__0_carry__0_i_11_n_0;
  wire adder_1_s__0_carry__0_i_12_n_0;
  wire adder_1_s__0_carry__0_i_13_n_0;
  wire adder_1_s__0_carry__0_i_9_n_0;
  wire adder_1_s__0_carry__0_i_9_n_1;
  wire adder_1_s__0_carry__0_i_9_n_2;
  wire adder_1_s__0_carry__0_i_9_n_3;
  wire adder_1_s__0_carry__1_i_10_n_0;
  wire adder_1_s__0_carry__1_i_11_n_0;
  wire adder_1_s__0_carry__1_i_12_n_0;
  wire adder_1_s__0_carry__1_i_13_n_0;
  wire adder_1_s__0_carry__1_i_9_n_0;
  wire adder_1_s__0_carry__1_i_9_n_1;
  wire adder_1_s__0_carry__1_i_9_n_2;
  wire adder_1_s__0_carry__1_i_9_n_3;
  wire adder_1_s__0_carry__2_i_10_n_0;
  wire adder_1_s__0_carry__2_i_11_n_0;
  wire adder_1_s__0_carry__2_i_12_n_0;
  wire adder_1_s__0_carry__2_i_13_n_0;
  wire adder_1_s__0_carry__2_i_9_n_0;
  wire adder_1_s__0_carry__2_i_9_n_1;
  wire adder_1_s__0_carry__2_i_9_n_2;
  wire adder_1_s__0_carry__2_i_9_n_3;
  wire adder_1_s__0_carry__3_i_10_n_0;
  wire adder_1_s__0_carry__3_i_11_n_0;
  wire adder_1_s__0_carry__3_i_12_n_0;
  wire adder_1_s__0_carry__3_i_13_n_0;
  wire adder_1_s__0_carry__3_i_9_n_0;
  wire adder_1_s__0_carry__3_i_9_n_1;
  wire adder_1_s__0_carry__3_i_9_n_2;
  wire adder_1_s__0_carry__3_i_9_n_3;
  wire adder_1_s__0_carry__4_i_10_n_0;
  wire adder_1_s__0_carry__4_i_11_n_0;
  wire adder_1_s__0_carry__4_i_12_n_0;
  wire adder_1_s__0_carry__4_i_8_n_1;
  wire adder_1_s__0_carry__4_i_8_n_2;
  wire adder_1_s__0_carry__4_i_8_n_3;
  wire adder_1_s__0_carry__4_i_9_n_0;
  wire adder_1_s__0_carry_i_10_n_0;
  wire adder_1_s__0_carry_i_11_n_0;
  wire adder_1_s__0_carry_i_12_n_0;
  wire adder_1_s__0_carry_i_8_n_0;
  wire adder_1_s__0_carry_i_8_n_1;
  wire adder_1_s__0_carry_i_8_n_2;
  wire adder_1_s__0_carry_i_8_n_3;
  wire adder_1_s__0_carry_i_9_n_0;
  wire [23:23]adder_3_s;
  wire [22:0]multi_2_s__0;
  wire \q[0]_i_1__0_n_0 ;
  wire \q[12]_i_2_n_0 ;
  wire \q[12]_i_3_n_0 ;
  wire \q[12]_i_4_n_0 ;
  wire \q[12]_i_5_n_0 ;
  wire \q[16]_i_2_n_0 ;
  wire \q[16]_i_3_n_0 ;
  wire \q[16]_i_4_n_0 ;
  wire \q[16]_i_5_n_0 ;
  wire \q[1]_i_2_n_0 ;
  wire \q[1]_i_3_n_0 ;
  wire \q[1]_i_4_n_0 ;
  wire \q[1]_i_5_n_0 ;
  wire \q[20]_i_2_n_0 ;
  wire \q[20]_i_3_n_0 ;
  wire \q[20]_i_4_n_0 ;
  wire \q[20]_i_5_n_0 ;
  wire \q[4]_i_2_n_0 ;
  wire \q[4]_i_3_n_0 ;
  wire \q[4]_i_4_n_0 ;
  wire \q[4]_i_5_n_0 ;
  wire \q[8]_i_2_n_0 ;
  wire \q[8]_i_3_n_0 ;
  wire \q[8]_i_4_n_0 ;
  wire \q[8]_i_5_n_0 ;
  wire [23:0]q_reg;
  wire \q_reg[12]_i_1_n_0 ;
  wire \q_reg[12]_i_1_n_1 ;
  wire \q_reg[12]_i_1_n_2 ;
  wire \q_reg[12]_i_1_n_3 ;
  wire \q_reg[12]_i_1_n_4 ;
  wire \q_reg[12]_i_1_n_5 ;
  wire \q_reg[12]_i_1_n_6 ;
  wire \q_reg[12]_i_1_n_7 ;
  wire \q_reg[16]_i_1_n_0 ;
  wire \q_reg[16]_i_1_n_1 ;
  wire \q_reg[16]_i_1_n_2 ;
  wire \q_reg[16]_i_1_n_3 ;
  wire \q_reg[16]_i_1_n_4 ;
  wire \q_reg[16]_i_1_n_5 ;
  wire \q_reg[16]_i_1_n_6 ;
  wire \q_reg[16]_i_1_n_7 ;
  wire \q_reg[1]_i_1_n_0 ;
  wire \q_reg[1]_i_1_n_1 ;
  wire \q_reg[1]_i_1_n_2 ;
  wire \q_reg[1]_i_1_n_3 ;
  wire \q_reg[1]_i_1_n_4 ;
  wire \q_reg[1]_i_1_n_5 ;
  wire \q_reg[1]_i_1_n_6 ;
  wire \q_reg[20]_i_1_n_1 ;
  wire \q_reg[20]_i_1_n_2 ;
  wire \q_reg[20]_i_1_n_3 ;
  wire \q_reg[20]_i_1_n_4 ;
  wire \q_reg[20]_i_1_n_5 ;
  wire \q_reg[20]_i_1_n_6 ;
  wire \q_reg[20]_i_1_n_7 ;
  wire [23:0]\q_reg[23]_0 ;
  wire \q_reg[4]_i_1_n_0 ;
  wire \q_reg[4]_i_1_n_1 ;
  wire \q_reg[4]_i_1_n_2 ;
  wire \q_reg[4]_i_1_n_3 ;
  wire \q_reg[4]_i_1_n_4 ;
  wire \q_reg[4]_i_1_n_5 ;
  wire \q_reg[4]_i_1_n_6 ;
  wire \q_reg[4]_i_1_n_7 ;
  wire \q_reg[8]_i_1_n_0 ;
  wire \q_reg[8]_i_1_n_1 ;
  wire \q_reg[8]_i_1_n_2 ;
  wire \q_reg[8]_i_1_n_3 ;
  wire \q_reg[8]_i_1_n_4 ;
  wire \q_reg[8]_i_1_n_5 ;
  wire \q_reg[8]_i_1_n_6 ;
  wire \q_reg[8]_i_1_n_7 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [3:3]NLW_adder_1_s__0_carry__4_i_8_CO_UNCONNECTED;
  wire [0:0]NLW_adder_1_s__0_carry_i_8_O_UNCONNECTED;
  wire [3:3]\NLW_q_reg[20]_i_1_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__0_i_10
       (.I0(\q_reg[23]_0 [7]),
        .I1(q_reg[7]),
        .O(adder_1_s__0_carry__0_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__0_i_11
       (.I0(\q_reg[23]_0 [6]),
        .I1(q_reg[6]),
        .O(adder_1_s__0_carry__0_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__0_i_12
       (.I0(\q_reg[23]_0 [5]),
        .I1(q_reg[5]),
        .O(adder_1_s__0_carry__0_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__0_i_13
       (.I0(\q_reg[23]_0 [4]),
        .I1(q_reg[4]),
        .O(adder_1_s__0_carry__0_i_13_n_0));
  CARRY4 adder_1_s__0_carry__0_i_9
       (.CI(adder_1_s__0_carry_i_8_n_0),
        .CO({adder_1_s__0_carry__0_i_9_n_0,adder_1_s__0_carry__0_i_9_n_1,adder_1_s__0_carry__0_i_9_n_2,adder_1_s__0_carry__0_i_9_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [7:4]),
        .O(multi_2_s__0[7:4]),
        .S({adder_1_s__0_carry__0_i_10_n_0,adder_1_s__0_carry__0_i_11_n_0,adder_1_s__0_carry__0_i_12_n_0,adder_1_s__0_carry__0_i_13_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__1_i_10
       (.I0(\q_reg[23]_0 [11]),
        .I1(q_reg[11]),
        .O(adder_1_s__0_carry__1_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__1_i_11
       (.I0(\q_reg[23]_0 [10]),
        .I1(q_reg[10]),
        .O(adder_1_s__0_carry__1_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__1_i_12
       (.I0(\q_reg[23]_0 [9]),
        .I1(q_reg[9]),
        .O(adder_1_s__0_carry__1_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__1_i_13
       (.I0(\q_reg[23]_0 [8]),
        .I1(q_reg[8]),
        .O(adder_1_s__0_carry__1_i_13_n_0));
  CARRY4 adder_1_s__0_carry__1_i_9
       (.CI(adder_1_s__0_carry__0_i_9_n_0),
        .CO({adder_1_s__0_carry__1_i_9_n_0,adder_1_s__0_carry__1_i_9_n_1,adder_1_s__0_carry__1_i_9_n_2,adder_1_s__0_carry__1_i_9_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [11:8]),
        .O(multi_2_s__0[11:8]),
        .S({adder_1_s__0_carry__1_i_10_n_0,adder_1_s__0_carry__1_i_11_n_0,adder_1_s__0_carry__1_i_12_n_0,adder_1_s__0_carry__1_i_13_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__2_i_10
       (.I0(\q_reg[23]_0 [15]),
        .I1(q_reg[15]),
        .O(adder_1_s__0_carry__2_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__2_i_11
       (.I0(\q_reg[23]_0 [14]),
        .I1(q_reg[14]),
        .O(adder_1_s__0_carry__2_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__2_i_12
       (.I0(\q_reg[23]_0 [13]),
        .I1(q_reg[13]),
        .O(adder_1_s__0_carry__2_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__2_i_13
       (.I0(\q_reg[23]_0 [12]),
        .I1(q_reg[12]),
        .O(adder_1_s__0_carry__2_i_13_n_0));
  CARRY4 adder_1_s__0_carry__2_i_9
       (.CI(adder_1_s__0_carry__1_i_9_n_0),
        .CO({adder_1_s__0_carry__2_i_9_n_0,adder_1_s__0_carry__2_i_9_n_1,adder_1_s__0_carry__2_i_9_n_2,adder_1_s__0_carry__2_i_9_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [15:12]),
        .O(multi_2_s__0[15:12]),
        .S({adder_1_s__0_carry__2_i_10_n_0,adder_1_s__0_carry__2_i_11_n_0,adder_1_s__0_carry__2_i_12_n_0,adder_1_s__0_carry__2_i_13_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__3_i_10
       (.I0(\q_reg[23]_0 [19]),
        .I1(q_reg[19]),
        .O(adder_1_s__0_carry__3_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__3_i_11
       (.I0(\q_reg[23]_0 [18]),
        .I1(q_reg[18]),
        .O(adder_1_s__0_carry__3_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__3_i_12
       (.I0(\q_reg[23]_0 [17]),
        .I1(q_reg[17]),
        .O(adder_1_s__0_carry__3_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__3_i_13
       (.I0(\q_reg[23]_0 [16]),
        .I1(q_reg[16]),
        .O(adder_1_s__0_carry__3_i_13_n_0));
  CARRY4 adder_1_s__0_carry__3_i_9
       (.CI(adder_1_s__0_carry__2_i_9_n_0),
        .CO({adder_1_s__0_carry__3_i_9_n_0,adder_1_s__0_carry__3_i_9_n_1,adder_1_s__0_carry__3_i_9_n_2,adder_1_s__0_carry__3_i_9_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [19:16]),
        .O(multi_2_s__0[19:16]),
        .S({adder_1_s__0_carry__3_i_10_n_0,adder_1_s__0_carry__3_i_11_n_0,adder_1_s__0_carry__3_i_12_n_0,adder_1_s__0_carry__3_i_13_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__4_i_10
       (.I0(\q_reg[23]_0 [22]),
        .I1(q_reg[22]),
        .O(adder_1_s__0_carry__4_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__4_i_11
       (.I0(\q_reg[23]_0 [21]),
        .I1(q_reg[21]),
        .O(adder_1_s__0_carry__4_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__4_i_12
       (.I0(\q_reg[23]_0 [20]),
        .I1(q_reg[20]),
        .O(adder_1_s__0_carry__4_i_12_n_0));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    adder_1_s__0_carry__4_i_4
       (.I0(Q[0]),
        .I1(multi_2_s__0[22]),
        .I2(P[0]),
        .I3(adder_3_s),
        .I4(P[1]),
        .I5(Q[1]),
        .O(S));
  CARRY4 adder_1_s__0_carry__4_i_8
       (.CI(adder_1_s__0_carry__3_i_9_n_0),
        .CO({NLW_adder_1_s__0_carry__4_i_8_CO_UNCONNECTED[3],adder_1_s__0_carry__4_i_8_n_1,adder_1_s__0_carry__4_i_8_n_2,adder_1_s__0_carry__4_i_8_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,\q_reg[23]_0 [22:20]}),
        .O({adder_3_s,multi_2_s__0[22:20]}),
        .S({adder_1_s__0_carry__4_i_9_n_0,adder_1_s__0_carry__4_i_10_n_0,adder_1_s__0_carry__4_i_11_n_0,adder_1_s__0_carry__4_i_12_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry__4_i_9
       (.I0(\q_reg[23]_0 [23]),
        .I1(q_reg[23]),
        .O(adder_1_s__0_carry__4_i_9_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry_i_10
       (.I0(\q_reg[23]_0 [2]),
        .I1(q_reg[2]),
        .O(adder_1_s__0_carry_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry_i_11
       (.I0(\q_reg[23]_0 [1]),
        .I1(q_reg[1]),
        .O(adder_1_s__0_carry_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry_i_12
       (.I0(\q_reg[23]_0 [0]),
        .I1(q_reg[0]),
        .O(adder_1_s__0_carry_i_12_n_0));
  CARRY4 adder_1_s__0_carry_i_8
       (.CI(1'b0),
        .CO({adder_1_s__0_carry_i_8_n_0,adder_1_s__0_carry_i_8_n_1,adder_1_s__0_carry_i_8_n_2,adder_1_s__0_carry_i_8_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [3:0]),
        .O({multi_2_s__0[3:1],NLW_adder_1_s__0_carry_i_8_O_UNCONNECTED[0]}),
        .S({adder_1_s__0_carry_i_9_n_0,adder_1_s__0_carry_i_10_n_0,adder_1_s__0_carry_i_11_n_0,adder_1_s__0_carry_i_12_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    adder_1_s__0_carry_i_9
       (.I0(\q_reg[23]_0 [3]),
        .I1(q_reg[3]),
        .O(adder_1_s__0_carry_i_9_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    \q[0]_i_1__0 
       (.I0(\q_reg[23]_0 [0]),
        .I1(q_reg[0]),
        .O(\q[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_2 
       (.I0(\q_reg[23]_0 [15]),
        .I1(q_reg[15]),
        .O(\q[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_3 
       (.I0(\q_reg[23]_0 [14]),
        .I1(q_reg[14]),
        .O(\q[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_4 
       (.I0(\q_reg[23]_0 [13]),
        .I1(q_reg[13]),
        .O(\q[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[12]_i_5 
       (.I0(\q_reg[23]_0 [12]),
        .I1(q_reg[12]),
        .O(\q[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_2 
       (.I0(\q_reg[23]_0 [19]),
        .I1(q_reg[19]),
        .O(\q[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_3 
       (.I0(\q_reg[23]_0 [18]),
        .I1(q_reg[18]),
        .O(\q[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_4 
       (.I0(\q_reg[23]_0 [17]),
        .I1(q_reg[17]),
        .O(\q[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[16]_i_5 
       (.I0(\q_reg[23]_0 [16]),
        .I1(q_reg[16]),
        .O(\q[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_2 
       (.I0(\q_reg[23]_0 [3]),
        .I1(q_reg[3]),
        .O(\q[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_3 
       (.I0(\q_reg[23]_0 [2]),
        .I1(q_reg[2]),
        .O(\q[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_4 
       (.I0(\q_reg[23]_0 [1]),
        .I1(q_reg[1]),
        .O(\q[1]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_5 
       (.I0(\q_reg[23]_0 [0]),
        .I1(q_reg[0]),
        .O(\q[1]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_2 
       (.I0(\q_reg[23]_0 [23]),
        .I1(q_reg[23]),
        .O(\q[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_3 
       (.I0(\q_reg[23]_0 [22]),
        .I1(q_reg[22]),
        .O(\q[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_4 
       (.I0(\q_reg[23]_0 [21]),
        .I1(q_reg[21]),
        .O(\q[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[20]_i_5 
       (.I0(\q_reg[23]_0 [20]),
        .I1(q_reg[20]),
        .O(\q[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_2 
       (.I0(\q_reg[23]_0 [7]),
        .I1(q_reg[7]),
        .O(\q[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_3 
       (.I0(\q_reg[23]_0 [6]),
        .I1(q_reg[6]),
        .O(\q[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_4 
       (.I0(\q_reg[23]_0 [5]),
        .I1(q_reg[5]),
        .O(\q[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[4]_i_5 
       (.I0(\q_reg[23]_0 [4]),
        .I1(q_reg[4]),
        .O(\q[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_2 
       (.I0(\q_reg[23]_0 [11]),
        .I1(q_reg[11]),
        .O(\q[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_3 
       (.I0(\q_reg[23]_0 [10]),
        .I1(q_reg[10]),
        .O(\q[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_4 
       (.I0(\q_reg[23]_0 [9]),
        .I1(q_reg[9]),
        .O(\q[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \q[8]_i_5 
       (.I0(\q_reg[23]_0 [8]),
        .I1(q_reg[8]),
        .O(\q[8]_i_5_n_0 ));
  FDCE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q[0]_i_1__0_n_0 ),
        .Q(q_reg[0]));
  FDCE \q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1_n_5 ),
        .Q(q_reg[10]));
  FDCE \q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1_n_4 ),
        .Q(q_reg[11]));
  FDCE \q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1_n_7 ),
        .Q(q_reg[12]));
  CARRY4 \q_reg[12]_i_1 
       (.CI(\q_reg[8]_i_1_n_0 ),
        .CO({\q_reg[12]_i_1_n_0 ,\q_reg[12]_i_1_n_1 ,\q_reg[12]_i_1_n_2 ,\q_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [15:12]),
        .O({\q_reg[12]_i_1_n_4 ,\q_reg[12]_i_1_n_5 ,\q_reg[12]_i_1_n_6 ,\q_reg[12]_i_1_n_7 }),
        .S({\q[12]_i_2_n_0 ,\q[12]_i_3_n_0 ,\q[12]_i_4_n_0 ,\q[12]_i_5_n_0 }));
  FDCE \q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1_n_6 ),
        .Q(q_reg[13]));
  FDCE \q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1_n_5 ),
        .Q(q_reg[14]));
  FDCE \q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[12]_i_1_n_4 ),
        .Q(q_reg[15]));
  FDCE \q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1_n_7 ),
        .Q(q_reg[16]));
  CARRY4 \q_reg[16]_i_1 
       (.CI(\q_reg[12]_i_1_n_0 ),
        .CO({\q_reg[16]_i_1_n_0 ,\q_reg[16]_i_1_n_1 ,\q_reg[16]_i_1_n_2 ,\q_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [19:16]),
        .O({\q_reg[16]_i_1_n_4 ,\q_reg[16]_i_1_n_5 ,\q_reg[16]_i_1_n_6 ,\q_reg[16]_i_1_n_7 }),
        .S({\q[16]_i_2_n_0 ,\q[16]_i_3_n_0 ,\q[16]_i_4_n_0 ,\q[16]_i_5_n_0 }));
  FDCE \q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1_n_6 ),
        .Q(q_reg[17]));
  FDCE \q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1_n_5 ),
        .Q(q_reg[18]));
  FDCE \q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[16]_i_1_n_4 ),
        .Q(q_reg[19]));
  FDCE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[1]_i_1_n_6 ),
        .Q(q_reg[1]));
  CARRY4 \q_reg[1]_i_1 
       (.CI(1'b0),
        .CO({\q_reg[1]_i_1_n_0 ,\q_reg[1]_i_1_n_1 ,\q_reg[1]_i_1_n_2 ,\q_reg[1]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [3:0]),
        .O({\q_reg[1]_i_1_n_4 ,\q_reg[1]_i_1_n_5 ,\q_reg[1]_i_1_n_6 ,multi_2_s__0[0]}),
        .S({\q[1]_i_2_n_0 ,\q[1]_i_3_n_0 ,\q[1]_i_4_n_0 ,\q[1]_i_5_n_0 }));
  FDCE \q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1_n_7 ),
        .Q(q_reg[20]));
  CARRY4 \q_reg[20]_i_1 
       (.CI(\q_reg[16]_i_1_n_0 ),
        .CO({\NLW_q_reg[20]_i_1_CO_UNCONNECTED [3],\q_reg[20]_i_1_n_1 ,\q_reg[20]_i_1_n_2 ,\q_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\q_reg[23]_0 [22:20]}),
        .O({\q_reg[20]_i_1_n_4 ,\q_reg[20]_i_1_n_5 ,\q_reg[20]_i_1_n_6 ,\q_reg[20]_i_1_n_7 }),
        .S({\q[20]_i_2_n_0 ,\q[20]_i_3_n_0 ,\q[20]_i_4_n_0 ,\q[20]_i_5_n_0 }));
  FDCE \q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1_n_6 ),
        .Q(q_reg[21]));
  FDCE \q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1_n_5 ),
        .Q(q_reg[22]));
  FDCE \q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[20]_i_1_n_4 ),
        .Q(q_reg[23]));
  FDCE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[1]_i_1_n_5 ),
        .Q(q_reg[2]));
  FDCE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[1]_i_1_n_4 ),
        .Q(q_reg[3]));
  FDCE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1_n_7 ),
        .Q(q_reg[4]));
  CARRY4 \q_reg[4]_i_1 
       (.CI(\q_reg[1]_i_1_n_0 ),
        .CO({\q_reg[4]_i_1_n_0 ,\q_reg[4]_i_1_n_1 ,\q_reg[4]_i_1_n_2 ,\q_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [7:4]),
        .O({\q_reg[4]_i_1_n_4 ,\q_reg[4]_i_1_n_5 ,\q_reg[4]_i_1_n_6 ,\q_reg[4]_i_1_n_7 }),
        .S({\q[4]_i_2_n_0 ,\q[4]_i_3_n_0 ,\q[4]_i_4_n_0 ,\q[4]_i_5_n_0 }));
  FDCE \q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1_n_6 ),
        .Q(q_reg[5]));
  FDCE \q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1_n_5 ),
        .Q(q_reg[6]));
  FDCE \q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[4]_i_1_n_4 ),
        .Q(q_reg[7]));
  FDCE \q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1_n_7 ),
        .Q(q_reg[8]));
  CARRY4 \q_reg[8]_i_1 
       (.CI(\q_reg[4]_i_1_n_0 ),
        .CO({\q_reg[8]_i_1_n_0 ,\q_reg[8]_i_1_n_1 ,\q_reg[8]_i_1_n_2 ,\q_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_0 [11:8]),
        .O({\q_reg[8]_i_1_n_4 ,\q_reg[8]_i_1_n_5 ,\q_reg[8]_i_1_n_6 ,\q_reg[8]_i_1_n_7 }),
        .S({\q[8]_i_2_n_0 ,\q[8]_i_3_n_0 ,\q[8]_i_4_n_0 ,\q[8]_i_5_n_0 }));
  FDCE \q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(s00_axi_aresetn),
        .D(\q_reg[8]_i_1_n_6 ),
        .Q(q_reg[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
