----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/11/2020 19:02:23 PM
-- Design Name: 
-- Module Name: parametric_eq - beh
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity parametric_eq is
    generic(    
        data_width  : natural := 24
    );
                
    Port ( 
        clk_i           : in    std_logic;
        rst_i           : in    std_logic;
     --   start_i         : in    std_logic;
     --   ready_o         : out   std_logic;    
        q_i             : in    std_logic_vector (data_width-1 downto 0);
        f_i             : in    std_logic_vector (data_width-1 downto 0);
        data_i          : in    std_logic_vector (data_width-1 downto 0);
        data_o          : out   std_logic_vector (data_width-1 downto 0)
    );
    
end parametric_eq;

architecture beh of parametric_eq is
 
    signal data_o_s     : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_1_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_2_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal adder_3_s    : std_logic_vector (data_width-1 downto 0) := (others=>'0');
    signal multi_1_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal multi_2_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal multi_3_s    : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal reg_1_s      : std_logic_vector (data_width-1 downto 0) := (others=>'0');   
    signal reg_2_s      : std_logic_vector (data_width-1 downto 0) := (others=>'0');
        
begin

    adder_1_s <= std_logic_vector(signed(data_i) - signed(multi_3_s(43 downto 20)) - signed(adder_3_s));

    adder_2_s <= std_logic_vector(signed(multi_1_s(43 downto 20)) + signed(reg_1_s));
    
    adder_3_s <= std_logic_vector(signed(multi_2_s(43 downto 20)) + signed(reg_2_s));
    
    multi_1_s <= std_logic_vector(signed(f_i) * signed(adder_1_s));
    
    multi_2_s <= std_logic_vector(signed(f_i) * signed(reg_1_s));
    
    multi_3_s <= std_logic_vector(signed(q_i) * signed(reg_1_s));
    
    data_o <= adder_2_s;
    
    first_reg:
    entity work.reg(beh)
    generic map(
        N_bit_reg => data_width
    )
    port map(
        clk =>  clk_i,
        rst =>  rst_i,    
        d   =>  adder_2_s,
        q   =>  reg_1_s
    );
    
    second_reg:
    entity work.reg(beh)
    generic map(
        N_bit_reg => data_width
    )
    port map(
        clk =>  clk_i,
        rst =>  rst_i,    
        d   =>  adder_3_s,
        q   =>  reg_2_s
    );

end beh;
